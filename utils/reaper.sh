#!/bin/bash
#
#  Look through jobs/ directory to find old archive files
#  If old archive job succeeded, remove the corresponding AWS file
#  Remove the old archive job directory and files
#

function fileAgeDays {
    file="$1";
    AGE=$(stat -c %Z "$file");
    NOW=$(date +%s);
    (( DIFF = (NOW - AGE)/60/60/24 )); 
    echo $DIFF;
}

#
#  Don't delete eFile jobs, submitjob should delete them
#
function handleEfileJob {
    dir="$1"
    if [ -f "$dir/NEEDS_RESTART" ]; then
        echo "Efile job $dir is not done, needs restart!"
    else
        echo "Bypassing efile job $dir"
    fi
}

#
#  Completed stamp jobs may be deleted after 30 days
#
function handleStampJob {
    max_age_days=30
    dir="$1"

    # Check to see if job was successful
    if [ ! -z "$(cat "$dir/log.txt" | grep 'Final archive')" ]; then
        # Check age of completed job
        age=$(fileAgeDays "$dir")
        if [ "$age" -gt "$max_age_days" ]; then
            # Delete job directory
            echo "Stamp job $dir complete, $age days old, removing files"
            fqdir="$jobdir/$dir"
            rm -rf "$fqdir"
        else
            echo "Stamp job $dir is done, $age days old"
        fi
    else
        if [ -f "$dir/NEEDS_RESTART" ]; then
            echo "Stamp job $dir is not done, needs restart!"
        else
            echo "Stamp job $dir possibly still active"
        fi
    fi
}

#
#  Completed download jobs may be deleted after 30 days
#
function handleDownloaderJob {
    max_age_days=30
    dir="$1"

    # Check to see if job was successful
    statusfile="$dir/log.txt"
    if [ ! -f "$statusfile" ]; then
        statusfile="$dir/status.html"
    fi
    if [ ! -f "$statusfile" ]; then
        echo "Unable to determine status of job $dir, please inspect"
        return
    fi
    if [ ! -z "$(cat "$statusfile" | grep 'Archive complete!')" ]; then
        # Check job age
        age=$(fileAgeDays "$dir")
        # If job is $max_age_days old, remove archived file and job directory
        if [ "$age" -gt "$max_age_days" ]; then
            echo "Download job $dir is done, $age days old, removing files"
            # Remove AWS archive file
            # Doesn't work for batch-style downloads
            # URL=$(cat "$dir/status.html" | sed -e "s|^.*<a href='\(.*\.zip\).*$|\1|")
            # filename=$(expr "$URL" : '.*/\([^/]*.zip\)');
            # $delete_aws "$filename"

            # Delete job directory
            fqdir="$jobdir/$dir"
            rm -rf "$fqdir"
        else
            echo "Download job $dir is done, $age days old"
        fi
    else
        if [ -f "$dir/NEEDS_RESTART" ]; then
            echo "Downloader job $dir is not done, needs restart!"
        elif [ -f "$dir/CANCELLED" ]; then
            # Delete job directory
            echo "Download job $dir cancelled, removing job files"
            fqdir="$jobdir/$dir"
            rm -rf "$fqdir"
        else
            echo "Downloader job $dir possibly still active"
        fi
    fi
}


appdir="/var/www/html/downloader"
jobdir="/var/www/html/tmp/downloader/jobs"
delete_aws="$appdir/utils/delete_aws"

# Find all job directories > 24 hours old
cd $jobdir;
archive_dirs=$(find [0-9]* -maxdepth 0 -mtime +1)

for dir in $archive_dirs; do
    if [ -f "$dir/EFILE_JOB" ]; then
        handleEfileJob "$dir"
    elif [ -f "$dir/STAMP_JOB" ]; then
        handleStampJob "$dir"
    else
        handleDownloaderJob "$dir"
    fi
done

