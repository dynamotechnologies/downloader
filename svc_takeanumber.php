<?php
error_reporting(0);
/*
 *	The downloader client calls this script to request a job
 *
 *	Creates a temporary working directory to hold job files
 *	Returns a job ID and a status URL in JSON format
 *	{ "jobid": <id>, "statusurl": <url> }
 *
 *	Errors are returned with following format:
 *	{ "errormsg": <msg> }
 */

require_once('config.php');

function quit($msg) {
  $response = array('errormsg' => $msg);
  echo json_encode($response);
  exit(1);
}

# Generate job ID and working directory
$retries = 0;
while (true) {
  $jobid=rand(1000000, 9999999);
  # $jobid=1000000;	# DEBUG: Cause error
  $jobdir="$JOB_DIR/$jobid";
  if (mkdir($jobdir)) {
    break;
  } else {
    $retries++;
  }
  if ($retries > 10) {
    quit("Service unavailable: Couldn't create working directory");
  }
} 

# Create status file
$fp=fopen("$jobdir/$STATUSFILE", "w");
fwrite($fp, "Waiting for job to start");
fclose($fp);

$statusURL="$JOB_URLBASE/$jobid/$STATUSFILE";
$cancelURL="svc_cancelarchive.php?jobid=$jobid";

$response = array('jobid' => $jobid, 'statusurl' => $statusURL, 'cancelurl' => $cancelURL);
echo json_encode($response);

?>
