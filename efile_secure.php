<?php

require 'rb.php';
R::setup('sqlite:/tmp/efile_ipbans.db','user','password');

/*
 *  This is where we keep all the ridiculous gimmicks being used
 *  to obfuscate behavior until the downloader gets moved inside of PHE
 *
 *  If you want to read the contents of the time-sensitive key which is used
 *  as a request parameter, use the decodeargs() function like so:
 *
 *  <?php
 *  require "efile_secure.php";
 *  print_r(decodeargs("ppzDa26p4KHNzMmmyZ*irZ6jsGg5lq0"));
 *  ?>
 *
 *  Which will print the following:
 *
 *  Array
 *  (
 *      [time] => 1360962354
 *      [author] => lhierholzer
 *      [projectid] => 0
 *  )
 *
 *  Note that in this case the projectid has been zeroed for some reason.
 */
$DECODED_PROJECTID = "";
$DECODED_AUTHOR = "";

/*
 *  Encode string using Vigenere cipher
 */
function enc($str, $key)
{
    $slen = strlen($str);
    $klen = strlen($key);
    for ($i = 0; $i < $slen; ++$i)
    {
        $str[$i] = chr(ord($str[$i]) + ord($key[$i % $klen]) % 255);
    }
    return $str;
}

/*
 *  Decode string using Vigenere cipher
 */
function dec($str, $key)
{
    $slen = strlen($str);
    $klen = strlen($key);
    for ($i = 0; $i < $slen; ++$i)
    {
        $tmp = (ord($str[$i]) - ord($key[$i % $klen])) % 255;
        $str[$i] = ($tmp < 0) ? chr(255 + $tmp) : chr($tmp);
    }
    return $str;
} 

/*
 *  Repad base64 string with equals (=) signs (not really necessary)
 */
function pad($str) {
    $rem = strlen($str) % 4;
    if ($rem > 0) {
        $str = $str . str_repeat("=", 4-$rem);
    }
   return $str;
}

/*
 *  Remove trailing equals (=) signs to disguise base64 encoding
 */
function unpad($str) {
    return str_replace("=", "", $str);
}

/*
 *  Build obfuscated argument string
 */
function encodeargs($args) {
    $author = $args['author'];
    $projectid = $args['projectid'];
    $time = $args['time'];
    $plaintext = base_convert($time,10,36) ."|". $author ."|". base_convert($projectid,10,36);
    $usec = intval(array_shift(preg_split("/ /", microtime())) * 1000000);
    $key = sha1($usec);
    $ciphertext = unpad(base64_encode(enc($plaintext, $key)));
    $enc_key = str_pad(base_convert($usec,10,36), 4, "0", STR_PAD_LEFT);
    $ciphertext = $ciphertext . $enc_key;
    $map = array('+' => '*', '/' => '_');
    $ciphertext = strtr($ciphertext, $map);
    return $ciphertext;
}

/*
 *  Decode obfuscated argument string
 */
function decodeargs($ciphertext) {
    $map = array('*' => '+', '_' => '/');
    $ciphertext = strtr($ciphertext, $map);
    $usec = base_convert(substr($ciphertext, -4),36,10);
    $ciphertext = base64_decode(pad(substr($ciphertext, 0, -4)));
    $key = sha1($usec);
    $plaintext = dec($ciphertext, $key);
    $fields = preg_split("/\|/", $plaintext);
    if (count($fields) != 3) {
        throw new Exception("Ciphertext $ciphertext decodes to " . count($fields) . " fields");
    }
    $time_enc = $fields[0];
    $user_enc = $fields[1];
    $proj_enc = $fields[2];
    $args = array();
    $args['time'] = base_convert($time_enc,36,10);
    $args['author'] = $user_enc;
    $args['projectid'] = base_convert($proj_enc,36,10);
    return $args;
}

/*
 *  Record an IP ban
 */
function banip($ip) {
    $oldbans = R::find('ipban', 'ipaddr = ? ORDER BY time DESC', array($ip));
    $ipban = null;
    if (count($oldbans) == 0) {
        $ipban = R::dispense('ipban');
    } else {
        $ipban = array_shift($oldbans);
    }
    $ipban->ipaddr = $ip;
    $ipban->time = R::isoDateTime();	// now
    if (isset($ipban->bancount)) {
        $ipban->bancount = $ipban->bancount + 1;
    } else {
        $ipban->bancount = 1;
    }
    $id = R::store($ipban);
}

/*
 *  Check to see if the given IP addr is still banned
 *  If banned, return the ban timestamp (time of most recent ban)
 */
function checkban($ip) {
    global $IPBAN_TIMEOUT;	// Length of IP ban, in seconds (1 hr)
    global $IPBAN_THRESHOLD;	// # of infractions allowed before lockout (3)

    $now = time();		// seconds since gmtime epoch
    $cutoff = $now - $IPBAN_TIMEOUT;
    $cutoff = strftime('%F %T', $cutoff);

    // Expire old bans
    $where_clause = "time < '$cutoff' ORDER BY time DESC";
    $oldbans = R::find('ipban', $where_clause, array());
    R::trashAll($oldbans);

    // See if there are any remaining bans for this IP addr
    $where_clause = "ipaddr = ? AND bancount >= $IPBAN_THRESHOLD ORDER BY time DESC";
    $oldbans = R::find('ipban', $where_clause, array($ip));
    $ipban = null;
    if (count($oldbans) == 0) {
        return False;
    } else {
        $ipban = array_shift($oldbans);
        return $ipban->time;
    }
}

/*
 *  Unban the given IP addr
 */
function unbanip($ip) {
    $oldbans = R::find('ipban', 'ipaddr = ? ORDER BY time DESC', array($ip));
    if (count($oldbans) == 0) {
        R::trashAll($oldbans);
    }
}

/*
 *  For debugging, dump all IP ban records
 */
function printbans() {
    $oldbans = R::find('ipban', "1=1 ORDER BY time DESC", array());
    foreach ($oldbans as $ban) {
        echo "ipaddr: " . $ban->ipaddr . ", time: " . $ban->time . "\n";
    }
}

/*
 *  Send alert to mailing list
 */
function emailalert($msg) {
    global $EMAIL_ALERT;

    $body = "<html><body>\n";
    $body .= "<p>" . htmlentities($msg) . "</p>\n";
    $body .= "<pre>\n";
    $body .= '$_SERVER[] =' . "\n";
    $body .= print_r($_SERVER, True);
    $body .= "\n";
    $body .= '$_GET[] =' . "\n";
    $body .= print_r($_GET, True);
    $body .= "</pre>\n";
    $body .= "</body></html\n";
    $headers = "From: eFile Manager<noreply@epa.gov>\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    $hostname = gethostname();
    $subject = "$hostname: EFILE ALERT";

    mail($EMAIL_ALERT, $subject, $body, $headers);
}
