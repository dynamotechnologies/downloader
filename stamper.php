<?php
/*
 *  Downloader variant for selecting files to be Bates-stamped
 *  and distributed as a zip archive
 */
if (!file_exists('config.php')) {
	// config.php file missing
	echo "<H1>System error, can't continue (code 01)</H1>";
	exit(0);
}
require_once('config.php');

/* Make sure required definitions were set in the config file */
if (!isset($STELLENT_IPADDR)) {
	// STELLENT_IPADDR not set
	echo "<H1>System error, can't continue (code 02)</H1>";
	exit(0);
}

if (!isset($DATAMART_PREFIX)) {
	// DATAMART_PREFIX not set
	echo "<H1>System error, can't continue (code 03)</H1>";
	exit(0);
}

if (!isset($DATAMART_AUTH)) {
	// DATAMART_AUTH not set
	echo "<H1>System error, can't continue (code 04)</H1>";
	exit(0);
}

require_once('datamart_utils.php');
require_once('dmd_utils.php');

$DATAMART = new datamart_utils();

// Make sure page was invoked with required parameters
$projectid = $_GET['projectid'];
if (!$projectid) {
	// No project ID given
        echo "<H1>System error, can't continue (code 09)</H1>";
	exit(0);
}

$author = $_GET['author'];
if (!$author) {
	// No author name given
        echo "<H1>System error, can't continue (code 10)</H1>";
	exit(0);
}

$columnspec = "a,d,f,h,j";
if (isset($_GET['columnspec'])) {
    $columnspec = htmlentities($_GET["columnspec"]);
}
if (strlen(trim($columnspec)) == 0) {
    $columnspec = "a,d,f,h,j";
}
/* Validate columnspec */
$columnspec = trim($columnspec);
// Codes currently range from a-j
if (!preg_match("/^([a-j],)*[a-j]$/", $columnspec)) {
        echo "<H1>Bad format for columnspec, can't continue</H1>";
        exit(0);
}

/* Create archive title */
$today = date('Y-m-d');
$project_title = "$projectid Stamped Archive $today";

/* Make sure systems are available */
if ($DATAMART->datamart_status() != 200) {
	echo "<H1>Error, system not available</H1>";
	echo "<div>Please try again later (code 12)</div>";
	exit(0);
}

if (dmd_status() == False) {
	echo "<H1>Error, document repository server not responding</H1>";
	echo "<div>Please try again later (code 13)</div>";
	exit(0);
}

/* Load user data */
try {
        $USERDATA = $DATAMART->datamart_getUserData($author);
} catch (Exception $e) {
        echo "<H1>Error loading data for user ${author}</H1>";
        echo "<H2>" . $e->getMessage() . "</H2>";
        exit(0);
}

/* Load project data */
try {
        $PROJECTDATA = $DATAMART->datamart_getProjectData($projectid);
} catch (Exception $e) {
        echo "<H1>Error loading data for project ${projectid}</H1>";
        echo "<H2>" . $e->getMessage() . "</H2>";
        exit(0);
}

/* Make sure user is allowed to access the given project */
$adminunitid = $PROJECTDATA['unitcode'];
// 1 = PALS_ADMIN
// 3 = SOPA_MANAGER
// 4 = PALS_LITIGATION_COORDINATOR
// 5 = PALS_APPEAL_COORDINATOR
$authorized_roles = array(1, 3, 4, 5);
$authorized = False;
foreach ($USERDATA['roles'] as $roleline) {
    $roledata = explode("|", $roleline);
    $unit = $roledata[0];
    $role = $roledata[1];
    $unit = preg_replace("/(00)*$/", "", $unit);
    if (preg_match("/^$unit/", $adminunitid)) {
        if (in_array($role, $authorized_roles)) {
            $authorized = True;
            break;
        }
    }
}
if (! $authorized) {
    echo "<H1>System error, can't continue (code 14)</H1>";
    exit(0);
}

/* Load project containers in Dynatree format */
$DATAMART->datamart_setModeBates($adminunitid, $USERDATA);
try {
        // Don't prune folders, make Public Involvement an all-or-nothing
        // "album" folder.
        $dynatree = $DATAMART->datamart_getContainersAsDynatree($projectid, "", "/Original Letters and Submissions/i");
} catch (Exception $e) {
        echo "<H1>Error loading containers for project ${projectid}</H1>";
        echo "<H2>" . $e->getMessage() . "</H2>";
        exit(0);
}

/* Stop IE8 from modifying the page for XSS */
header('X-XSS-Protection: 0');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Project <?php echo $project_title; ?></title>

<link type="text/css" href="jquery/css/custom-theme/jquery-ui-1.8.13.custom.css" rel="Stylesheet" />
<link rel='stylesheet' type='text/css' href='dynatree/src/skin/ui.dynatree.css'>
<link rel="stylesheet" type="text/css" href="stamper.css">
<?php
/*
 * After combining, replace the above with
<link rel="stylesheet" type="text/css" href="combined-min.css">
 */
?>

<script type="text/javascript">

  var _gaq = _gaq || [];

<?php if (isset($GA_TRACKER_ID)) { ?>

  _gaq.push(['_setAccount', '<?php echo $GA_TRACKER_ID; ?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

<?php } ?>

</script>

<!-- Include the required JavaScript libraries: -->
<script src='jquery/js/jquery-1.8.2.min.js' type="text/javascript"></script>
<script src='jquery/js/jquery-ui-1.8.24.custom.min.js' type="text/javascript"></script>
<script src='jquery/js/jquery.dateFormat-1.0.js' type="text/javascript"></script>
<script src='dynatree/jquery/jquery.cookie.js' type="text/javascript"></script>
<script src='dynatree/src/jquery.dynatree.js' type="text/javascript"></script>

</head>
<body>
<div id="titleBanner">
  <img id="clipboardImg" src="assets/clipboard.gif"/>
  <div id="titleStripe">
    <div id="title">Project <?php echo $project_title; ?></div>
  </div>
</div>
<br/>
<div id="content">
<div class="sectionTitle">File Selection</div>
<div class="elementBox">
  <div id="selectAllBtn" class="btn select-button selectall">Select All</div>
  <div id="expandAllBtn" class="btn expand-button expandall">Expand All</div>
</div>
<div class="elementBox">
  <div id="totalSelected"></div>
  <div id="fileSelector"></div>
</div>

<?php include("stamper/stamper_options.php"); ?>

<div class="nofloat">
  <div id="submitBtn" class="btn submit-button">Stamp Selected Files</div><br/>
</div>
</div>

<script type="text/javascript">

  var selected_filecnt = 0;
  var selected_filesize = 0;
  var project_filecnt = 0;

  $(function () {

    // Discourage caching
    $.ajaxSetup({ cache: false });

    // Make button text unselectable
    $.fn.disableSelection = function() {
        return this
                 .attr('unselectable', 'on')
                 .css('user-select', 'none')
                 .on('selectstart', false);
    };

    // Init button UIs and disable text selection
    $(".btn").button().disableSelection();

    // Get dynatree data
    json_data = $.parseJSON('<?php echo $dynatree; ?>');
    project_filecnt = '<?php echo $DATAMART->G_DOC_COUNT; ?>';
    projectid = '<?php echo $projectid; ?>';
    project_title = '<?php echo $project_title; ?>';

    // Define download button action (create archive)
    $("#submitBtn").click(
      function() {
        var checkedlist = [];

        // Get list of doc IDs in "checkedlist"
        selectedlist = $("#fileSelector").dynatree("getSelectedNodes");
        $.each(selectedlist,
          function() {
            node = this;
            if (node.data.isFolder == false) {
              checkedlist.push(node.data.key);
            }
          }
        );

        // Validate form
        formdata = validateStamperForm();

	if ($(".warning").length > 0) {
          var msg = '<div class="confirm-title">Submission Error</div>Your form has invalid fields, please check the highlighted fields and try again!';
          showWarnings(msg);
        } else if (checkedlist.length == 0) {
          var msg = '<div class="confirm-title">Submission Error</div>You have not selected any files, please check your file selections and try again!';
          showWarnings(msg);
        } else {
          showConfirmation(checkedlist, formdata);
        }
      }
    );

    function showWarnings(msg) {
      // Show warning dialog
      $("<div><br/></div>")
      .find("br").after(msg).end()
      .css('text-align', 'center')
      .dialog({
        resizable: false,
        modal: true,
        buttons: {
          "OK": function() {
            $( this ).dialog( "destroy" );
          }
        }
      })
      .dialog('widget')
      .addClass('modalDialog');       // hide titlebar via css

      // adjust button placement
      $('.modalDialog .ui-dialog-buttonset').position({
        my: 'center bottom',
        at: 'center bottom',
        of: $('.modalDialog')
      });
    }

    /*
     *  Display confirmation screen and prompt user for action
     *
     *  checkedlist is a Javascript array of selected files
     *  formdata is a wrapped set of form elements (usually <div>)
     */
    function showConfirmation(checkedlist, formdata) {

      var title = "<div>Stamp job confirmation</div>"

      var myprompt = "<div>You selected " + selected_filecnt + " of " +
                project_filecnt + " files";
      myprompt += "<br/>(total " + selected_filesize + "kb)";
      myprompt += "<br/>Do you want to stamp these items?</div>";

      $("<div>")
      .append($(title).addClass('confirm-title'))
      .append($(myprompt).addClass('confirm-prompt'))
      .dialog({
        resizable: false,
        width: 'auto',	// broken for IE7?
        modal: true,
        buttons: {
          "Submit": function() {
            $( this ).dialog( "destroy" );
            disableUI();
            publishFiles();
            $('#stamperForm').submit();
          },
          "Cancel": function() {
            $( this ).dialog( "destroy" );
          }
        }
      })
      .dialog('widget')
      .addClass('modalDialog');       // hide titlebar via css

      // adjust button placement
      $('.modalDialog .ui-dialog-buttonset').position({
        my: 'center bottom',
        at: 'center bottom',
        of: $('.modalDialog')
      });
    }


    // Define other button actions
    // Select/deselect all documents
    $("#selectAllBtn").click(
        function() {
            if ($(this).hasClass("selectall")) {
                // Select all nodes
                $("#fileSelector").dynatree('getRoot').visit(function(node) {
                  node.select(true);
                });
                // Change button text
                $(this).button("option", "label", "Deselect All");
            } else {
                // Deselect all nodes
                $("#fileSelector").dynatree('getRoot').visit(function(node) {
                  node.select(false);
                });
                // Change button text
                $(this).button("option", "label", "Select All");
            }
            $(this).toggleClass("selectall");
            return false;
        }
    );



    // Expand all folders
    $("#expandAllBtn").click(
        function() {
            if ($(this).hasClass("expandall")) {
                // Expand all nodes
                $("#fileSelector").dynatree("getRoot").visit(function(node){
                  node.expand(true);
                });
                // Change button text
                $(this).button("option", "label", "Collapse All");
            } else {
                // Collapse all nodes
                $("#fileSelector").dynatree("getRoot").visit(function(node){
                  node.expand(false);
                });
                // Change button text
                $(this).button("option", "label", "Expand All");
            }
            $(this).toggleClass("expandall");
            return false;
        }
    );



    // Display running total of selected files and filesize (from globals)
    // See countSelectedFiles()
    function displayTotal() {
      $("#totalSelected").html("Selected " + selected_filecnt + " of " + project_filecnt + " files, estimated file size " + selected_filesize + "kb");
    }



    // Disable all buttons and tree controls on main page
    function disableUI() {
      $(".btn").button("disable");
      $("#fileSelector").dynatree("disable");
    }



    // Given a Dynatree node (representing a document),
    // create a "file spec" string in the list of documents to be archived
    // A file spec consists of the comma-separated fields:
    //     1) path
    //     2) filename
    //     3) docid
    // All fields are URI-encoded
    function addPubFile(publist, node) {
      var path="";
      node.visitParents(function(node) {
        path = encodeURIComponent(node.data.filename) + "/" + path;
      }, false);
      var filename = node.data.filename;
      var docid = node.data.key;
      publist.push(encodeURIComponent(path) +','+ encodeURIComponent(filename) +','+ encodeURIComponent(docid));
    }



    // Given a Dynatree node (representing a folder),
    // create a "file spec" string in the list of documents to be archived
    // A file spec consists of the comma-separated fields:
    //     1) path 
    //     2) album name (the category name)
    //     3) the reserved word ALBUM (instead of a docid)
    //     4) the PALS unique container ID
    // All fields are URI-encoded
    function addPubAlbum(publist, node) {
      var path="";
      node.visitParents(function(node) {
        path = encodeURIComponent(node.data.filename) + "/" + path;
      }, false);
      var filename = node.data.filename;
      var docid = "ALBUM";
      var contid = node.data.contid;
      publist.push(encodeURIComponent(path) +','+ encodeURIComponent(filename) +','+ encodeURIComponent(docid) +','+ encodeURIComponent(contid));
    }



    // Recompute count/size of selected files
    // See displayTotal()
    function countSelectedFiles() {
      selected_filecnt = 0;
      selected_filesize = 0;
      selectedlist = $("#fileSelector").dynatree("getSelectedNodes");
      $.each(selectedlist,
        function() {
          var node = this;
          if (node.data.isFolder == false) {
            if (node.data.isAlbum == true) {
              selected_filecnt += node.data.filecnt;
              selected_filesize += node.data.size;
            } else {
              selected_filecnt += 1;
              selected_filesize += node.data.size;
            }
          }
        }
      );
    }



    // Publish the selected files to a stamper job spec
    //
    // Collect list of selected files
    // Reserve a job ID for the archive job (call svc_takeanumber.php)
    function publishFiles() {

      $('#projectID').val('<?php echo $projectid; ?>');
      $('#projectName').val(<?php echo json_encode($PROJECTDATA['name']); ?>);
      $('#userID').val('<?php echo $author; ?>');
      $('#userEmail').val('<?php echo $USERDATA['email']; ?>');
      $('#columnSpec').val(<?php echo json_encode($columnspec); ?>);
      $('#archiveName').val(<?php echo json_encode("$project_title $author"); ?>);
      $('#archiveRoot').val($("#fileSelector").dynatree('getRoot').data.filename);

      // Get list of documents to publish in "publist"
      publist = new Array();
      selectedlist = $("#fileSelector").dynatree("getSelectedNodes");
      $.each(selectedlist,
        function() {
          var node = this;
          if (node.data.isAlbum == true) {
            addPubAlbum(publist, node);
          } else if (node.data.isFolder == false) {
            addPubFile(publist, node);
          }
        }
      );

      // Assemble job request
<?php
/*
 * jobspec: vbar-separated fields
 * Each field describes a file to be submitted
 * (see addPubFile() and addPubAlbum())
 * All subfields are URI-encoded
 *
 * Unlike downloader.php, the Bates Stamper doesn't have an
 * interactive mode where we have to pass multiple parameters
 * via JSON, so we don't need to store other data fields in the
 * job spec.  Other fields are sent in form variables.
 */
?>
      jobspec = publist.join("|");
      $('#selectedFiles').val(jobspec);

    } // End of function publishFiles()

    // Create dynatree
    // Attach the dynatree widget to an existing <div> element
    // and pass the tree options as an argument to the dynatree() function:
    $("#fileSelector").dynatree({
      cookieId: "stamper",
      checkbox: true,
      selectMode: 3,
      persist: true,
      children: json_data,
      onSelect: function(flag, node) {
<?php
 /*
  * This seems terribly inefficient but appears to work well in practice
  */
 ?>
        countSelectedFiles();
        $("#TotalFiles").val(selected_filecnt);
        displayTotal();
      },
      cookie: {
        expires: 90
      }
    });

    // Set root title
    $("#fileSelector").dynatree('getRoot').data.title = project_title;
    // Set name of root for stamped files in archive
    $("#fileSelector").dynatree('getRoot').data.filename = "files";

    // Count selected files
    countSelectedFiles();
    $("#TotalFiles").val(selected_filecnt);
    displayTotal();

    // Prevalidate form
    validateStamperForm();
    updateStampPreview();
  });
</script>
</body>
</html>
