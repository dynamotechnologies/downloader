<?php
/*
 *  Cancel an archive job partway through
 *  This is a "best effort" attempt and no status is returned
 */
require_once('aws_utils.php');
require_once('config.php');

error_reporting(0);

$jobid = $_GET['jobid'];
if (!isset($jobid)) { exit(1); }

$cancelfile = "$JOB_DIR/$jobid/$CANCELFILE";
$fp = fopen($cancelfile, "w");
fwrite($fp, "Job cancelled");
fclose($fp);
?>
