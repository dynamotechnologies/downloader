<?php
/*
 *	Common app definitions
 */

$PHP_CLI = "/opt/rh/rh-php56/root/usr/bin/php";

$PROJECT_DETAIL_URL = "https://www.fs.fed.us/nepa/nepa_project_exp.php";
$STATUSFILE="status.html";
$CANCELFILE="CANCELLED"; // Touch this file in working dir to halt archiving
$STATUS_WAITING = "WAITING";
$STATUS_WORKING = "WORKING";
$STATUS_SUCCESS = "SUCCESS";
$STATUS_ERROR = "ERROR";
$MAX_MEMBER_LENGTH = 50;        // Max length of a path member
$MAX_PATH_LENGTH = 250;         // Max length of a complete path

$ARCHIVE_BUFFER	= 200;		// Extract/process N documents before zipping
$INTERACTIVE_FILE_LIMIT	= 1000;	// Max docs allowed to download interactively
// $MAX_FILES_PER_ARCHIVE = 50000;	// Max docs allowed per zipfile
$MAX_FILES_PER_ARCHIVE = 50000;	// Max docs allowed per zipfile

$IPBAN_TIMEOUT = 60 * 60;	// IP bans last 1 hour
$IPBAN_THRESHOLD = 3;		// 3 strikes permitted before enforcing ban

/*
 *	App commands
 */
$STAMPER_CMD = "java -jar /var/www/common/stamper/Stamper.jar";

/*
 *	Organization Point of Contact (POC) for EPA e-File
 */
$POC_FIRSTNAME = "Adam";
$POC_LASTNAME = "Shaw";
$POC_CDXUSERID = "USFSCDX1";
$POC_AGENCY = "US Forest Service";
$POC_EMAIL = "ashaw@fs.fed.us";
$POC_PHONE = "(801) 975-3364";
$POC_EXT = "";

$BATCH_SIZE_LIMIT = 50 * 1024 * 1024;   // 50 Mb

// Filetypes that we expect to be able to convert to PDF
// Extensions are case-insensitive
$PDF_CONVERSION_TYPES = array(
'doc',
'bmp',
'docx',
'dot',
'gif',
'htm',
'html',
'jpeg',
'jpg',
'png',
'ppt',
'rtf',
'txt',
'wpd',
'pdf'
);

// Define metadata field names for use with manifest_utils.php
// NOTE: these are NOT necessarily field names in the DMD metadata struct,
// and when locating metadata values for the Excel manifest, these may
// indicate that data should be pulled from somewhere else
define ("FL_CATEGORY",	'category');
define ("FL_GROUP",	'group');
define ("FL_SUBGROUP", 'subgroup');
define ("FL_DOCTITLE", 'doctitle');
define ("FL_FILENAME", 'filename');
define ("FL_DOCDATE",	'docdate');
define ("FL_PUBDATE",	'pubdate');
define ("FL_AUTHOR",	'author');
define ("FL_RECIPIENT", 'recipient');
define ("FL_DESCRIPTION", 'description');

// Additional non-metadata field names
define ("FL_LINK",	'link');	// relative path to archived file
define ("FL_PDFPAGECOUNT", 'pdfpagecount');	// number of PDF pages
define ("FL_SENSITIVEFLAG", 'sensitiveflag');	// boolean true/false

// Mapping from metadata code letters to field names
$METADATA_FIELDS = array(
"a" => FL_CATEGORY,	// appdoctype
"b" => FL_GROUP,	// mm/level2name
"c" => FL_SUBGROUP,	// mm/level3name
"d" => FL_DOCTITLE,	// title
"e" => FL_FILENAME,	// filename
"f" => FL_DOCDATE,	// mm/docdate
"g" => FL_PUBDATE,	// pubdate
"h" => FL_AUTHOR,	// author
"i" => FL_RECIPIENT,	// mm/recipient
"j" => FL_DESCRIPTION,	// mm/docsummary

// FIXME: "z" is provisional to support a PDF page count column,
// may be supported in future version of PALS as an orderable column
// If this letter changes, make sure to change stamper/processjob.php to match

"z" => FL_PDFPAGECOUNT	// not in metadata, number of PDF pages
);

// Mapping from metadata field names to column titles
$METADATA_TITLES = array(
FL_CATEGORY	=> "Category",
FL_GROUP	=> "Group",
FL_SUBGROUP	=> "Subgroup",
FL_DOCTITLE	=> "Doc Title",
FL_FILENAME	=> "File Name",
FL_DOCDATE	=> "Date",
FL_PUBDATE	=> "Upload Date",
FL_AUTHOR	=> "Author",
FL_RECIPIENT	=> "Recipient",
FL_DESCRIPTION	=> "Description",
FL_PDFPAGECOUNT	=> "# Pages"
);

?>
