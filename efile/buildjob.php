<?php

require_once "../config.php";

/*
 *  Build the job description files (submit.csv, submitdata.php)
 *  and begin processing (call submitjob.php)
 *
 *  - Build submit.csv
 *  - Build submitdata.php (other params, list of doc IDs and names)
 */

/*
 *  Implode an array of subscriber fields into a CSV row,
 *  taking into account CSV quoting rules
 */
function implodeCSV($fields) {
    $csvrow = "";
    // escape double-quotes
    $copyfields = str_replace(',','~', preg_replace('/"/', '""', $fields));

    // quote any fields with embedded commas or newlines or double-quotes
    foreach ($copyfields as $idx => $field) {
        if (preg_match('/[,"\n]/', $field)) {
            $copyfields[$idx] = "\"$field\"";
        }
    }
    $csvrow = implode(',', $copyfields);
    return $csvrow;
}

/*
 *  Build submit.csv
 *
 *  Title
 *  EIS Type
 *  Lead Agency
 *  Lead Contact Name
 *  Lead Contact Phone
 *  Lead Contact Email
 *  Other Lead Agencies
 *  Cooperating Federal Agency
 *  Cooperating Other Agency
 *  Proposed Comment Days
 *  Key State
 *  Other State
 *  Action Category
 *  Action Subcategory
 *  Registration First Name
 *  Registration Last Name
 *  CDX UserID
 *  Registration Organization
 *  Registration Email
 *  Registration Phone
 *  Registration Phone Extension
 *  Project Website Link
 */
function buildSubmitCSV($path_to_file) {
    $fields = array(
        "Title"				=> $_POST['Title'],
        "EIS Type"			=> $_POST['EISType'],
        "Lead Agency"			=> $_POST['LeadAgency'],
        "Lead Contact Name"		=> $_POST['LeadContactName'],
        "Lead Contact Phone"		=> $_POST['LeadContactPhone'],
        "Lead Contact Email"		=> $_POST['LeadContactEmail'],
        "Other Lead Agencies"		=> $_POST['OtherLeadAgencies_multi'],
        "Cooperating Federal Agency"	=> $_POST['CoopFedAgencies_multi'],
        "Cooperating Other Agency"	=> $_POST['OtherCoopAgencies_multi'],
        "Proposed Comment Days"		=> $_POST['ProposedCommentDays'],
        "Key State"			=> $_POST['KeyState'],
        "Other State"			=> $_POST['OtherStates_multi'],
        "Action Category"		=> $_POST['eisSubjectCategory'],
        "Action Subcategory"		=> $_POST['eisSubjectSubcategory'],
        "Registration First Name"	=> $_POST['FirstName'],
        "Registration Last Name"	=> $_POST['LastName'],
        "CDX UserID"			=> $_POST['CDXUserId'],
        "Registration Organization"	=> $_POST['RegOrg'],
        "Registration Email"		=> $_POST['RegEmail'],
        "Registration Phone"		=> $_POST['RegPhone'],
        "Registration Phone Extension"	=> $_POST['RegExt'],
        "Project Website Link"		=> $_POST['ProjectWebsiteLink']
    );

    $fp = fopen($path_to_file, "w");
    fwrite($fp, implodeCSV(array_keys($fields)) . "\n");
    fwrite($fp, implodeCSV(array_values($fields)));
    fclose($fp);
}

/*
 *  Store other parameters so job can restart
 */
function buildSubmitData($path_to_file) {
    $fp = fopen($path_to_file, "w");
    $postvars = serialize($_POST);
    fwrite($fp, $postvars);
    fclose($fp);
}

/*
 *	MAIN
 */
try {
    if (! $JOB_DIR) {
        throw new Exception("JOB_DIR not provided, cannot continue!");
    }
    // Get job ID
    $mypath = $_SERVER['SERVER_NAME'] . dirname($_SERVER['SCRIPT_NAME']);
    $jsonstr = file_get_contents("http://$mypath/../svc_takeanumber.php");
    $jobdata = json_decode($jsonstr, True);
    if (! isset($jobdata['jobid'])) {
        $msg = "Couldn't get new job ID, cannot continue!  Reason: " .
            $jobdata['errormsg'];
        throw new Exception($msg);
    }
    $jobid = strval($jobdata['jobid']);

    // Create working directories
    $workingdir = "$JOB_DIR/$jobid";
    if (! is_dir("$workingdir")) {
        throw new Exception("Job dir $jobid not created, cannot continue!");
    }
    touch("$workingdir/EFILE_JOB");
    $submitcsv = "$workingdir/submit.csv";
    $submitdata = "$workingdir/submitdata.php";
    $restart = "$workingdir/restart.sh";

    // Generate secret key to prevent external users from running job
    $key = rand();
    $_POST['secretKey'] = $key;

    // Build submit.csv
    buildSubmitCSV($submitcsv);

    // Save job data (submitdata.php)
    buildSubmitData($submitdata);

    // Create the restart script
    $fp = fopen($restart, "w");
    $submitjoburl = "$mypath/submitjob.php";
    fwrite($fp, "curl 'http://$submitjoburl?jobid=$jobid&key=$key&start=0'");
    fclose($fp);
    chmod($restart, 0755);

    $email = "";
    if (isset($_POST['userEmail'])) {
        $email = $_POST['userEmail'];
    } else {
        throw new Exception("User record has no email address!  Can't continue");
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Job Submitted</title>
<link type="text/css" href="../jquery/css/custom-theme/jquery-ui-1.8.13.custom.css" rel="Stylesheet" />
<link rel="stylesheet" type="text/css" href="../efile.css">
</head>
<body>
<div id="content">
<p>Your job has been submitted.</p>
<p>A confirmation email will be sent to <?php echo $email; ?> when your job is complete.</p>
<p>You may now close this window.</p>
</div>
</body>
</html>
<?php
    // Submit job
    system("$restart >/dev/null 2>&1 &");

} catch (Exception $e) {

    echo "<PRE>";
    echo "Exception: " . $e->getMessage();
    // print_r($_POST);	// DEBUG
    echo "</PRE>";

}
?>
