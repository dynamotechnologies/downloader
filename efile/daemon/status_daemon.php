#!/opt/rh/rh-php56/root/usr/bin/php
<?php
//TODO add logging
  //Daemon used to get the status of a transaction and send the user an update
  //Dies when there are no items left in the queue and is restarted by
  //submitjob if it is not already running.
  //have to use absolute paths, since it's being called by another php file
  require_once("/var/www/html/downloader/config.php");
  require_once("utils.php");
  require_once("/var/www/html/downloader/mailer/Mailer.php");

  $pidfile = '/var/www/efile/status_daemon.pid';
  $FAILED = 'FAILED';
  $SUCCESS = 'COMPLETED';
  $STATUS_TABLE = 'status_jobs';


  umask(0); //there will only be one of these running; don't need chmod
  $pid = pcntl_fork();
  if ($pid < 0) {
    print('fork failed');
    exit(1);
  }
  if ($pid > 0) { //this is the parent process
    echo "daemon process started\n";
    exit;
  }
  //otherwise we're the child process and can continue
  $sid = posix_setsid();
  if ($sid < 0) {
    exit(2);
  }

  file_put_contents($pidfile, getmypid());
  pcntl_signal(SIGTERM, "sig_handler");
  pcntl_signal(SIGHUP, "sig_handler");
  ob_start();
  $db = get_db_handle();
  loop($db);
  $output = ob_get_clean();
  unlink($pidfile);

  function sig_handler($signo) {
    global $pidfile;
    switch($signo) {
      case SIGTERM:
        //If a job is interrupted in any phase, now the worst case scenario
        //is that it will repeat that job. User gets two emails, not the biggest
        //deal. Can later add another table if necessary, but this seems like
        //and unlikely problem, so just killing the program.
        unlink($pidfile);
        exit;
        break;
      case SIGHUP:
        //TODO add restart of daemon
        exit;
        break;
      default:
        //TODO determine what the default error handling should be
        exit;
    }
  }

  function loop($db) {
    global $STATUS_TABLE;
    global $pidfile;
    global $FAILED;
    global $SUCCESS;
    //read from the database. If there are entries in the table, process them
    $repeat = false;
    $result = $db->query("SELECT * FROM " . $STATUS_TABLE);
    if ($result == false) {
      //TODO add error handling
      return;
    }
    $resultSet = $result->fetchAll();
    if ($resultSet == false) {
      //TODO add error handling
      return;
    }
    if (count($resultSet)> 0) {
      foreach ($resultSet as $job) {
        $txid = $job['txid'];
        $userEmail = $job['userEmail'];
        $shortname = $job['userId'];
        $eisTitle = $job['title'];
        $projName = $job['projectName'];
        $projId = $job['projectId'];
        $status = getStatus($txid);
        if ($status[0] == $SUCCESS || $status[0] == $FAILED) {
          sendEmail($txid, $userEmail, $shortname, $eisTitle,
                    $projName, $projId, $status);
          //remove it from the database
          $query = "DELETE FROM $STATUS_TABLE WHERE txid=:txid";
          $delQuery = $db->prepare($query);
          if (!$delQuery->execute(array(':txid' => $txid))) {
            //TODO add error handling here
          }

        } else {
          $repeat = true; //there's at least 1 project still in progress
        }
      }
    }
    if ($repeat) {
      sleep(30);
      loop($db);
    }
  }

  function getStatus($txid) {
    global $EFILE_STATUS;
    $status = system("$EFILE_STATUS $txid 2>&1", $rc);
    //status is "status;detailed status"
    if ($rc != 0) {
      //TODO add error handling here
    }
    $statusAndDetails = explode(";", $status);
    return $statusAndDetails;
  }

  function sendEmail($txid, $userEmail, $shortname, $eisTitle,
                     $projName, $projId, $status) {
    global $SUCCESS;
    global $FAILED;
    $msg = "<html><body>";
    $msg .= "<p>Dear $shortname,</p>";
    $date = htmlentities(strftime('%F'));
    $time = htmlentities(strftime('%T'));
    if ($status[0] == $SUCCESS) {
      $msg .= "<p>This email confirms that your submission of the $projName " .
              "($projId) EIS has been successfully processed";

    } else if ($status[0] == $FAILED) {
      $msg .= "<p> This email has been sent to inform you that your " .
              "submission of the $projName ($projId) EIS has failed processing";
    } else {
      //TODO status isn't as expected, develop error handling here
    }

    $msg .= " by the EPA on $date at $time. Please keep this email " .
            "for your records.</p>";

    //TODO add helpful message for if it failed

    $msg .= "<p> Thank you for using PALS to submit your EIS!</p>";
    $subject = "EPA EIS filing submission ";
    $subject .= $status[0] == $SUCCESS ? "confirmation" : "alert";
    $subject .= " - $eisTitle";
    $mailer = new Mailer();
    $sent = $mailer->sendEmail($subject, $msg . "</body></html>", $userEmail);
    if(!$sent) {
      //TODO once I get logging set up here, log this error
    }

    //Send copy of email to Dynamo
    if (isset($EFILE_EMAIL_NOTIFY)) {
      $msg .= "\n";
      $msg .= "<br/>\n";
      $msg .= "<pre>\n";
      $msg .= "=================\n";
      $msg .= "Logfile: $LOGFILE\n";
      $msg .= "=================\n";
      $msg .= file_get_contents($LOGFILE);
      $msg .= "</pre>\n";
      $hostname = gethostname();
      $subject = "$hostname: $subject";
      $msg .= "</body></html>";
      $sent = $mailer->sendEmail($subject, $body, $EFILE_EMAIL_NOTIFY);
      if (!$sent) {
        //TODO once I get logging set up here, log this error
      }
    }


  }

?>
