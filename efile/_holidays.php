<?php
  function getHolidays($year) {
    $listOfDays = [];
    //d-m-Y works, m-d-Y does not
    $listOfDays[] = "01-01-$year";
    $listOfDays[] = "04-07-$year";
    $listOfDays[] = "11-11-$year";
    $listOfDays[] = "25-12-$year";

    //turn list of days into unix time

    $toUnix = function($value) {
      return strtotime($value);
    };

    $holidays = array_map($toUnix, $listOfDays);

    //have to calculate the rest
    //MLK = 3rd Mon in Jan
    //Pres Day = 3rd Mon in Feb
    //Mem Day = last Mon in May
    //Labor Day = 1st Mon in Sep
    //Col Day = 2nd Mon in Oct
    //Thanks = 4th Thurs in Nov

    $holidays[] = strtotime("third monday $year-01");
    $holidays[] = strtotime("third monday $year-02");
    $holidays[] = strtotime("last monday $year-05");
    $holidays[] = strtotime("first monday $year-09");
    $holidays[] = strtotime("second monday $year-10");
    $holidays[] = strtotime("fourth thursday $year-11");

    //move these to the closest weekday if on weekend
    $move_func = function($prevalue) {
      $dayOfWeek = date('w', $prevalue);
      $value = date('d-m-Y', $prevalue);
      if ($dayOfWeek == 0) {
        return strtotime("$value +1 days");
      } else if ($dayOfWeek == 6) {
        return strtotime("$value -1 days");
      } else {
        return strtotime($value);
      }
    };

    $correctedHolidays = array_map($move_func, $holidays);
    return $correctedHolidays;
  }
?>
