<script language="javascript">
$(function() {
    // Initialize options
    $('<option value="0">&nbsp;</option>').prependTo("#EISType");
    $("#EISType").val("0");
    $('<option value="0">&nbsp;</option>').prependTo("#KeyState");
    initMultiselect("#OtherLeadAgencies");
    initMultiselect("#CoopFedAgencies");
    initMultiselect("#OtherStates");

<?php
    $URL = "";
    if ($PROJECTDATA['wwwpub'] == 1) {
        if ($PROJECTDATA['wwwlink'] === "") {
            $URL = "${PROJECT_DETAIL_URL}?project=$projectid";
        } else {
            $URL = $PROJECTDATA['wwwlink'];
        }
    }
?>
    $("#ProjectWebsiteLinkLabel").text("<?php echo $URL; ?>");
    $("#ProjectWebsiteLink").val("<?php echo $URL; ?>");

    $('#ProposedCommentDays').datepicker({ minDate: 1});
    //Ensure that the minimum date for the datepicker follows business rules
    $('#EISType').on('change', function(){
      var minDays = calculateMinDays();
      $('#ProposedCommentDays').datepicker('destroy');
      $('#ProposedCommentDays').datepicker({ beforeShowDay: noHolidayOrWeekend,
                                             minDate: minDays});
    });
<?php
    // Set default KeyState if available
    $key_state = "0";
    if (count($PROJECTDATA['stateids']) == 1) {
      $key_state = $PROJECTDATA['stateids'][0];
      if ($key_state == "US") {	// Convert PALS	"US|All States"
        $key_state = "00";	// to EPA	"00|Nationwide"
      }
    }
?>
    $("#KeyState").val("<?php echo $key_state; ?>");

    // Initialize EIS Subject Category/Subcategory
    $("#subjectSelector").val("0");
    updateSubcategories(0);
    $("#subCatSelector").val("0");

    // Automatically erase UI warnings and errors
    $('select').change(function() {
        $(this).removeClass('warning');
        $(this).parent().find("div.error").empty();
    });
    $('input').change(function() {
        $(this).removeClass('warning');
        $(this).parent().find("div.error").empty();
    });

    /*
     * Init multiselect actions
     * "selector" is the jQuery select expr for the <div> element surrounding the <select>s
     * Example: initMultiselect("#leadAgency");
     */
    function initMultiselect(selector) {

        sortSelect(selector+" .choices");	// Sort options in left-side box

        // When right-arrow is clicked, move options to right-side box
        $(selector+" .choose").click(
            function() {
                $(selector+" .choices option:selected").remove().appendTo(selector+" .chosen");
                sortSelect(selector+" .chosen");	// Sort right-side box
            }
        );
        // When left-arrow is clicked, move options to left-side box
        $(selector+" .unchoose").click(
            function() {
                $(selector+" .chosen option:selected").remove().appendTo(selector+" .choices");
                sortSelect(selector+" .choices");	// Sort left-side box
            }
        );
    }

    /*
     * Sort an option list
     * "selector" is the jQuery select expr for the <select> element
     * Example: sortSelect("#sampleSelect .chosen");
     */
    function sortSelect(selector) {
        var my_options = $(selector + " option");
        my_options.sort(function(a,b) {
            if (a.text > b.text) return 1;
            else if (a.text < b.text) return -1;
            else return 0
        });
        $(selector).empty().append(my_options);
    }


});

/*
 * Calculates if day is national holiday or weekend
 */
function noHolidayOrWeekend(date) {
  var isWeekday = $.datepicker.noWeekends(date);
  if (isWeekday[0]) {
    //check if holiday
    return isHoliday(date);
  } else {
    return isWeekday;
  }
}

/*
 * Calculates if day is national holiday
 */
function isHoliday(date) {
  var year = date.getFullYear();
  //months are 0 indexed while days are not
  var dateHolidays = [
    new Date(year, 0, 1), new Date(year, 6, 4), new Date(year, 10, 11),
    new Date(year, 11, 25)
  ];

  //now for the other holidays
  dateHolidays.push(getDate(year, 3, 1, 0));
  dateHolidays.push(getDate(year, 3, 1, 1));
  dateHolidays.push(getDate(year, 5, 1, 4));
  dateHolidays.push(getDate(year, 1, 1, 8));
  dateHolidays.push(getDate(year, 2, 1, 9));
  dateHolidays.push(getDate(year, 4, 4, 10));

  holidays = dateHolidays.map(function(date) {
    var dayOfWeek = date.getDay();
    if (dayOfWeek == 6) {
      var newDate = new Date(date)
      newDate.setDate(newDate.getDate() - 1);
      return newDate.getTime();
    } else if (dayOfWeek == 0) {
      var newDate = new Date(date);
      newDate.setDate(newDate.getDate() + 1);
      return newDate.getTime();
    } else {
      return date.getTime();
    }
  });

  if ($.inArray(date.getTime(), holidays) >= 0) {
    return [false, ''];
  } else {
    return [true, ''];
  }
}

function getDate(year, qualifier, dayOfWeek, month) {
  //qualifier = 1 || 2 || 3 || 4 || 5
  //5 = last of month
  var date = new Date(year, month);
  if (qualifier !== 5) {
    date.setDate(1);
    while (date.getDay() !== dayOfWeek) {
      date.setDate(date.getDate() + 1);
    }
    var addDays = 7 * (qualifier - 1);
    date.setDate(date.getDate() + addDays);
  } else {
    date.setDate(32);
    while (date.getDay() !== dayOfWeek) {
      date.setDate(date.getDate() - 1);
    }
  }
  return date;
}

/*
 * Calculate the minimum number of days allowed by EPA regulations
 * for the comment period to end
 */
function calculateMinDays() {
  var eisType = $('#EISType').val().toLowerCase();
  var minDays = eisType.search('final') >= 0 ? 30 :
                eisType.search('draft') >= 0 ? 45 : 1;
  var date = new Date();
  var saturday = 6
  var dayOfWeek = date.getDay();
  minDays += 7;
  //what we want to do is to count the days of the week in reverse
  //starting at Friday as 0, i.e. the number of days until Friday from today
  //in general, the way to reverse an ordered set is below:
  //let n = number of numbers in ordered set
  //    z = largest number in the set
  //    t = target origin point (where you want the counting in reverse to start)
  //    x = the number in the set you're trying to reverse, in our case, day of week
  //f(x) = z - ((x + z - t) % n))
  //for us, that's 6 - ((x + 1) % 7)
  //x + 1 because Friday is 1 day away from the largest day, Saturday
  minDays += saturday - ((dayOfWeek + 1) % (saturday + 1));
  return minDays;
}

function validateEfileForm() {
    // Clear old warnings
    $('#efileControls input').each(function(i, e)
    {
        $(e).removeClass('warning');
    });
    $('#efileControls select').each(function(i, e)
    {
        $(e).removeClass('warning');
    });
    $('#efileControls .error').empty();

    // Validate fields
    $('#efileControls input.required').each(function(i, e)
    {
        if( !$(e).val() ) {
            $(e).addClass('warning');
            $(e).parent().find('div.error').html('please enter a value');
        }
    });
    $('#efileControls select.required').each(function(i, e)
    {
        if( $(e).val() == "0" ) {
            $(e).addClass('warning');
            $(e).parent().find('div.error').html('please make a selection');
        }
    });

    MAX_TITLE_LENGTH = 300;
    var title = $('#Title').val();
    if (title.length > MAX_TITLE_LENGTH) {
        // title = title.substr(0,MAX_TITLE_LENGTH);
        // $('#Title').val(title);
        $('#Title').addClass('warning');
        $('#Title').parent().find('div.error').html('Title too long, please reduce to ' + MAX_TITLE_LENGTH + ' characters');
    }

    MAX_CONTACTNAME_LENGTH = 50;
    name = $('#LeadContactName').val();
    if (name.length > MAX_CONTACTNAME_LENGTH) {
        // name = name.substr(0,MAX_CONTACTNAME_LENGTH);
        // $('#LeadContactName').val(name);
        $('#LeadContactName').addClass('warning');
        $('#LeadContactName').parent().find('div.error').html('Contact name too long, please reduce to ' + MAX_CONTACTNAME_LENGTH + ' characters');
    }

    //check if the date is even a date
    var dateString = $('#ProposedCommentDays').val();
    var mdy = dateString.split('/');
    if (mdy.length !== 3 && dateString !== '') {
      $('#ProposedCommentDays').addClass('warning');
      $('#ProposedCommentDays').parent().find('div.error')
                               .html('The value entered is not a valid date. Correct format is month/day/year.');
    }
    var month = parseInt(mdy[0]);
    var day = parseInt(mdy[1]);
    var year = parseInt(mdy[2]);
    if (month <= 0 || month > 12) {
      $('#ProposedCommentDays').addClass('warning');
      $('#ProposedCommentDays').parent().find('div.error')
                               .html('The value entered is not a valid date. Correct format is month/day/year.');
    } else {
      var maxDays = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
      if (year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0)) {
        maxDays[2] = 29;
      }
      if (day <= 0 || day > maxDays[month]) {
        $('#ProposedCommentDays').addClass('warning');
        $('#ProposedCommentDays').parent().find('div.error')
                                 .html('The value entered is not a valid date. Correct format is month/day/year.');
      }
    }
    var minDays = calculateMinDays();
    var eisTypeFull = $('#EISType').val().toLowerCase();
    var eisType = eisTypeFull.search('final') >= 0 ? 'Final' :
                  eisTypeFull.search('draft') >= 0 ? 'Draft' :
                  '';
    var selectedDate = $('#ProposedCommentDays').datepicker('getDate');
    //technically used for datepicker, so need to just grab boolean portion
    //and need to reverse it, since datepicker false = do not show = is holiday
    var isOffLimits = !noHolidayOrWeekend(selectedDate)[0];
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    var timeDiff = selectedDate.getTime() - today.getTime();
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    if (diffDays < minDays) {
      $('#ProposedCommentDays').addClass('warning');
      $('#ProposedCommentDays').parent().find('div.error')
                               .html('The selected date violates EPA regulations of a minimum of: ' +
                                      minDays + ' days for all ' + eisType + ' EIS filings');
    }
    if (isOffLimits) {
      $('#ProposedCommentDays').addClass('warning');
      $('#ProposedCommentDays').parent().find('div.error')
                               .html('The selected date cannot fall on a weekend or holiday.');
    }

    // Consolidate multivalued selects into tilde-delimited CSV strings
    $('#efileControls div.multiselect').each(function(i, e) {
        multivalues = "";
        $(e).find("select.chosen option").each(function(i,e) {
            if (multivalues != "") {
                multivalues += "~";
            }
            multivalues += $(e).val();
        });
        // Assign multivalued select to a CSV string
        $("#" + $(e).attr('id') + "_multi").val(multivalues);
    });
    var otherCoopAgency_multi_list = '';
    var otherCoopAgencyDict = {};
    $('#otherCoopAgencies').find('input').each(function(i,e) {
      if (otherCoopAgency_multi_list != '') {
        otherCoopAgency_multi_list += '~';
      }
      var agency = $(e).val();
      if (agency !== '' && !(agency in otherCoopAgencyDict) &&
          agency.replace(/\s/g, '').length > 0) {
        otherCoopAgency_multi_list += agency;
        otherCoopAgencyDict[agency] = 1;
      } else {
        if (agency !== '') {
          $(e).addClass('warning');
          $(e).parent().find('div.error').html('The agency must contain characters other than spaces and cannot be a repeat of a previously entered agency');
        }
      }
    });
    $('#OtherCoopAgencies_multi').val(otherCoopAgency_multi_list)
    fields = $('#Title')
          .add('#EISType')
          .add('#LeadAgency')
          .add('#LeadContactName')
          .add('#LeadContactPhone')
          .add('#LeadContactEmail')
          .add('#OtherLeadAgencies')
          .add('#CoopFedAgencies')
          .add('#OtherCoopAgencies_multi')
          .add('#ProposedCommentDays')
          .add('#KeyState')
          .add('#OtherStates')
          .add('#subjectSelector')
          .add('#subCatSelector')
          .add('#FirstName')
          .add('#LastName')
          .add('#CDXUserID')
          .add('#RegOrg')
          .add('#RegEmail')
          .add('#RegPhone')
          .add('#RegExt')
          .add('#ProjectWebsiteLink');
<?php
// Note jQuery returns wrapped set in the order
// that elements appear on the web page
?>
    return fields;
}
</script>

<div id="efileControls">
<form id="efileForm" method="POST" action="efile/buildjob.php">
  <input id="projectID" name="projectID" style="display:none"/>
  <input id="projectName" name="projectName" style="display:none"/>
  <input id="userID" name="userID" style="display:none"/>
  <input id="userEmail" name="userEmail" style="display:none"/>
  <input id="userFirstname" name="userFirstname" style="display:none"/>
  <input id="userLastname" name="userLastname" style="display:none"/>
  <input id="selectedFiles" name="selectedFiles" style="display:none"/>
  <input id="jobID" name="jobID" style="display:none"/>

  <input id="FirstName" name="FirstName" style="display:none"
  value="<?php echo htmlentities($POC_FIRSTNAME); ?>"/>

  <input id="LastName" name="LastName" style="display:none"
   value="<?php echo htmlentities($POC_LASTNAME); ?>"/>

  <input id="CDXUserID" name="CDXUserId" style="display:none"
   value="<?php echo htmlentities($POC_CDXUSERID); ?>"/>

  <input id="RegOrg" name="RegOrg" style="display:none"
   value="<?php echo htmlentities($POC_AGENCY); ?>"/>

  <input id="RegEmail" name="RegEmail" style="display:none"
   value="<?php echo htmlentities($POC_EMAIL); ?>"/>

  <input id="RegPhone" name="RegPhone" style="display:none"
   value="<?php echo htmlentities($POC_PHONE); ?>"/>

  <input id="RegExt" name="RegExt" style="display:none"
   value="<?php echo htmlentities($POC_EXT); ?>"/>

  <div>
    <span class="instruction">* indicates a required field</span>
  </div>

  <div>
    <span class="star">*</span>
    <span class="label required">EIS Title</span>
    <div>
      <input id="Title" name="Title" type="text" class="required"
        value="<?php echo htmlentities($PROJECTDATA['name']); ?>"/>
      <div class="error"></div>
    </div>
  </div>

  <div>
    <span class="star">*</span>
    <span class="label required">EIS Type</span>
    <div>
      <select id="EISType" name="EISType" class="choices required">
        <?php include("_eistypes.html"); ?>
      </select>
      <div class="error"></div>
    </div>
  </div>

  <div>
    <span class="star">*</span>
    <span class="label required">Lead Agency</span>
    <div>
      <span class="plabel">USDA Forest Service</span>
      <input id="LeadAgency" name="LeadAgency" class="hidden" value="USFS"/>
      <div class="error"></div>
    </div>
  </div>

  <div>
    <span class="star hidden">*</span>
    <span class="label">Project Website Link</span>
    <div>
      <span class="plabel" id="ProjectWebsiteLinkLabel"></span>
      <input id="ProjectWebsiteLink" name="ProjectWebsiteLink" class="hidden" type="text"/>
      <div class="error"></div>
    </div>
  </div>

  <div>
    <span class="star">*</span>
    <span class="label required">Contact Name</span>
    <div>
      <input id="LeadContactName" name="LeadContactName" type="text" class="required"
        value="<?php echo htmlentities($PROJECTDATA['contactname']); ?>"/>
      <div class="error"></div>
    </div>
  </div>

  <div>
    <span class="star">*</span>
    <span class="label required">Contact Phone</span>
    <div>
      <input id="LeadContactPhone" name="LeadContactPhone" type="text" class="required"
        value="<?php echo htmlentities($PROJECTDATA['contactphone']); ?>"/>
      <div class="error"></div>
    </div>
  </div>

  <div>
    <span class="star">*</span>
    <span class="label required">Contact Email</span>
    <div>
      <input id="LeadContactEmail" name="LeadContactEmail" type="text" class="required"
        value="<?php echo htmlentities($PROJECTDATA['contactemail']); ?>"/>
      <div class="error"></div>
    </div>
  </div>

  <div>
    <span class="star hidden">*</span>
    <span class="label">Other Lead Agencies</span>
    <div id="OtherLeadAgencies" class="multiselect">
      <table><tr>
	       <td>
	          <select size="6" class="choices" multiple="multiple">
		            <?php include("_agencies.html"); ?>
	          </select>
	       </td>
	      <td class="controls">
	         <input class="choose" type="button" value=">"/><br>
	          <input class="unchoose" type="button" value="<"/>
	      </td>
	      <td>
	         <select size="6" class="chosen" multiple="multiple">
	         </select>
	      </td>
      </tr></table>
      <div class="error"></div>
    </div>
    <input id="OtherLeadAgencies_multi" name="OtherLeadAgencies_multi" style="display:none"/>
  </div>

  <div>
    <span class="star hidden">*</span>
    <span class="label">Cooperating Agencies</span>
    <div id="CoopFedAgencies" class="multiselect">
      <table><tr>
	       <td>
	          <select size="6" class="choices" multiple="multiple">
		            <?php include("_agencies.html"); ?>
	          </select>
	       </td>
	       <td class="controls">
	          <input class="choose" type="button" value=">"/><br>
	           <input class="unchoose" type="button" value="<"/>
	       </td>
	       <td>
	          <select size="6" class="chosen" multiple="multiple">
	          </select>
	       </td>
       </tr></table>
       <div class="error"></div>
     </div>
     <input id="CoopFedAgencies_multi" name="CoopFedAgencies_multi" style="display:none"/>
   </div>

   <div>
     <span class="star hidden">*</span>
     <span class="label">Other Cooperating Agencies</span>
     <div id="otherCoopAgencies">
       <div class="otherCoopAgDiv">
         <span class="otherCoopAgInstructions nofloat">
           Enter first Other Cooperating Agency here
         </span>
         <input class="otherCoopAgency" id="otherCoopAgency0" type="text"/>
         <div class="error"></div>
       </div>
     </div>
     <input id='OtherCoopAgencies_multi' name="OtherCoopAgencies_multi" type='text' style="display:none"/>
   </div>

   <div>
     <span class="star">*</span>
     <span class="label required">Comment Period<br/>End Date</span>
     <div>
       <input id="ProposedCommentDays" name="ProposedCommentDays" type="text" class="required"/>
       <div class="error"></div>
     </div>
   </div>

   <div>
     <span class="star">*</span>
     <span class="label required">Key State</span>
     <div>
       <select id="KeyState" name="KeyState" class="choices required">
         <option value="NAT">Nationwide</option>
          <?php include("_states.html"); ?>
       </select>
       <div class="error"></div>
     </div>
   </div>

   <div>
     <span class="star hidden">*</span>
     <span class="label">Other States</span>
     <div id="OtherStates" class="multiselect">
       <table><tr>
	        <td>
	           <select size="6" class="choices" multiple="multiple">
		             <?php include("_states.html"); ?>
	           </select>
	        </td>
	        <td class="controls">
	           <input class="choose" type="button" value=">"/><br>
	           <input class="unchoose" type="button" value="<"/>
	        </td>
	        <td>
	           <select size="6" class="chosen" multiple="multiple">
	           </select>
	        </td>
        </tr></table>
        <div class="error"></div>
      </div>
      <input id="OtherStates_multi" name="OtherStates_multi" style="display:none"/>
    </div>

    <?php include("_actions.html"); ?>
  </form>
</div>
