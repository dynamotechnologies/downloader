<?php
//TODO add shared variables between submitjob and status_daemon
//to the config.php file
require_once("../config.php");
require_once("../dmd_utils.php");
require_once("../aws_utils.php");
require_once('../mailer/Mailer.php');
require_once("daemon/utils.php");
require_once("_holidays.php");

// Logfile tags
define("L_DEBUG", "DEBUG");
define("L_INFO", "INFO");	// Generally data to be sent back to user
define("L_ERROR", "ERROR");
/*
 *  usage: submitjob.php?jobid=<jobid>&start=<startidx>
 *
 *  Download PDFs, generate file CSVs, package and submit through Node2
 *  Takes a submission CSV, an ordered list of IDs and file names, and an index
 *  indicating at which file to (re)start processing.
 *
 *  jobid    = The ID of the user job, identifies the work directory
 *  startidx = The 0-based index of the first file that requires processing
 *
 *  Expects the following files to exist locally:
 *      submit.csv     - the submission CSV
 *      submitdata.php - a serialized PHP array of form data including
 *      restart.sh     - a script used to initiate processing and restarts
 *
 *  If the set of files exceeds 50 Mb, process the job as multiple
 *  submissions.
 */

/*
 *  Implode an array of subscriber fields into a CSV row,
 *  taking into account CSV quoting rules
 */
function implodeCSV($fields) {
    $csvrow = "";
    // escape double-quotes
    $copyfields = preg_replace('/"/', '""', $fields);
    // quote any fields with embedded commas or newlines or double-quotes
    foreach ($copyfields as $idx => $field) {
        if (preg_match('/[,"\n]/', $field)) {
            $copyfields[$idx] = "\"$field\"";
        }
    }
    $csvrow = implode(',', $copyfields);
    return $csvrow;
}

/*
 *  Explode a subscriber CSV line on commas,
 *  taking into account CSV quoting rules
 */
function explodeCSV($line) {
    // s - PCRE_DOTALL - dot matches newlines
    // U - PCRE_UNGREEDY - match shortest pattern
    $escapedline = preg_replace_callback('/(^|,)"(.+)"(?=,|$)/sU',
        // remove surrounding quotes, replace escaped double-quotes,
        // and escape commas in any quoted CSV fields
        function ($match) {
            return $match[1] . preg_replace("/,/", "&#44;",
                                   preg_replace('/""/', '"', $match[2])
                               );
        }, trim($line));

    $boom = explode(',', $escapedline);

    // restore escaped commas in exploded array
    $boom = preg_replace("/&#44;/", ",", $boom);
    return $boom;
}

/*
 *  Log message and exit
 */
function quit($msg) {
    global $NEEDS_RESTART;
    global $EMAIL_ALERT;

    logERROR($msg);
    if (isset($NEEDS_RESTART)) {
        touch($NEEDS_RESTART);	// signal for help
    }
    sendErrorEmail($EMAIL_ALERT);
    sendUserErrorEmail($msg);
    exit(1);
}

function getPDF($docid, $filename) {
  $fp = fopen($filename, "w");
  if ($fp == null) {
      quit("Could not open file $filename");
  }
  try {
      fwrite($fp, dmd_getPDF($docid));
  } catch (Exception $e) {
      quit("Error retrieving file: " . $e->getMessage());
  }
  fclose($fp);
}

function addToFileCSV($fname, $name, $size, $pages) {
    $sizeinmb = $size / (1024 * 1024);
    $sizeinmb = round($sizeinmb, 2);
    $csvdata = array($name, strval($sizeinmb), strval($pages));
    try {
        $fp = fopen($fname, "a");
        if ($fp == null) {
            quit("Could not open file $fname");
        }
        fwrite($fp, "\n" . implodeCSV($csvdata));
        fclose($fp);
    } catch (Exception $e) {
        quit("Could not open file $fname: " . $e->getMessage());
    }
}

function addToEfileData($fname, $name, $path, $mimetype) {
    $spec = urlencode($name) ."|". urlencode($path) ."|". urlencode($mimetype);
    try {
        $fp = fopen($fname, "a");
        if ($fp == null) {
            quit("Could not open file $fname");
        }
        fwrite($fp, $spec . "\n");
        fclose($fp);
    } catch (Exception $e) {
        quit("Could not open file $fname: " . $e->getMessage());
    }
}

function countPages($docid, $file) {
    global $PAGECOUNTER;

    $status = 0;
    ob_start();
    $pagecount = system("$PAGECOUNTER $file", $status);
    ob_end_clean();
    if ($status != 0) {
        quit("Error running page counter for $docid, please notify admin");
    }
    if (! is_numeric($pagecount)) {
        quit("Unable to count pages for PDF $docid, check file format!");
    }
    return $pagecount;
}

function submitFiles($efiledata) {
    global $EFILE_SUBMIT;

    logDEBUG("Starting submission to eFile");
    $status = 0;
    ob_start();
    $output = system("$EFILE_SUBMIT $efiledata 2>&1", $status);
    ob_end_clean();
    if ($status != 0) {
        quit("Error submitting files: $EFILE_SUBMIT $efiledata $output");
    }

    // write transaction ID to logfile
    logINFO("Batch submission complete, tx = $output");
    return $output;
}

/*
 *  Prepend a timestamp to a log message
 */
function timestamp($msg) {
    $tstamp = strftime('%F %T');
    return "$tstamp $msg";
}

function logDEBUG($msg) { writeLogMsg(L_DEBUG, timestamp($msg)); }
function logINFO($msg) { writeLogMsg(L_INFO, timestamp($msg)); }
function logERROR($msg) { writeLogMsg(L_ERROR, timestamp($msg)); }

function writeLogMsg($prefix, $data) {
    global $LOGFILE;

    if (!isset($LOGFILE)) {
        exit(1);	// log file undefined
    }
    try {
        $fp = fopen($LOGFILE, "a");
        if ($fp == null) {
            quit("Could not open file $LOGFILE");
        }
        fwrite($fp, "$prefix: $data\n");
        fclose($fp);
    } catch (Exception $e) {
        // Fatal condition, quit immediately
        echo "Could not open log file $LOGFILE: " . $e->getMessage();
        exit(1);
    }
}

/*
 *  Store a whole (user-readable) data file in the log
 */
function logFile($prefix, $file, $title) {
    $lines = array();
    $lines[] = "Filename: $title";
    try {
        $lines = array_merge($lines, file($file));
    } catch (Exception $e) {
        quit("Could not open file $file: " . $e->getMessage());
    }
    foreach ($lines as $line) {
        writelogMsg($prefix, rtrim($line));	// remove newline
    }
}

function emptyDirectory($dir) {
    if ($dirhandle = opendir($dir)) {
        while (false !== ($file = readdir($dirhandle))) {
            if ($file != "." && $file != "..") {
                unlink("$dir/$file");
            }
        }
        closedir($dirhandle);
    }
}

/*
 *  Reset file list CSV
 */
function resetFilesCSV($filescsv) {
    unlink($filescsv);
    try {
        $fp = fopen($filescsv, "w");
        if ($fp == null) {
            quit("Could not open file $filescsv");
        }
        fwrite($fp, "File Name,File Size (MB),Pages");
        fclose($fp);
    } catch (Exception $e) {
        quit("Could not open file $filescsv: " . $e->getMessage());
    }
}

function updateRestartScript($fname, $docidx) {
    $cmd = file_get_contents($fname);
    $cmd = preg_replace("/(start=\d+)/", "start=$docidx", $cmd);
    try {
        $fp = fopen($fname, "w");
        if ($fp == null) {
            quit("Could not open file $fname");
        }
        fwrite($fp, "$cmd");
        fclose($fp);
    } catch (Exception $e) {
        quit("Could not open restart script $fname: " . $e->getMessage());
    }
}

/*
 * Check to see if date meets requirements of EPA
 */

function checkProposedCommentDay($dateSubmitted, $eisType) {
  $listOfHolidays = getHolidays(date('Y'));
  $proposedCommentTime = strtotime($dateSubmitted);
  $proposedDayOfWeek = date('w', $proposedCommentTime);
  $today = strtotime('today midnight');
  $dif = $proposedCommentTime - $today;
  $final = 'final';
  $draft = 'draft';
  $numDays = 6; // base 0 counting
  $eisType = strtolower($eisType);
  $dayOfWeek = date("w");
  $addDays = 7;
  //look at efile_controls calculateMinDays for explanation of why this works
  //and how to make it happen for any ordered set, not just days of week
  $addDays = $numDays - (($dayOfWeek + 1) % ($numDays + 1));
  if (strpos($eisType, $final) >= 0) {
    //needs 30 days + 2 fridays (addDays)
    $addDays += 30;
  } else if (strpos($eisType, $final) >= 0) {
    //needs 45 days + 2 fridays (addDays)
    $addDays += 45;
  } else {
    quit("EIS Type must either be Final or Draft: $eisType");
  }
  $holiday_func = function($value) {
    global $proposedCommentTime;
    return $value == $proposedCommentTime ? 1 : 0;
  };

  $holiday_array = array_map($holiday_func, $listOfHolidays);
  $isaHoliday = array_reduce($holiday_array, function($carry, $value) {
    $carry += $value;
    return $carry;
  });

  $linuxDate = date(strtotime("+ $addDays days")) - $today;
  if ($dif < $linuxDate || $proposedDayOfWeek == 6 ||
      $proposedDayOfWeek == 0 || $isaHoliday > 0) {
    quit('The provided Comment Period End Date is not far enough away from the
          date on which this EIS will be filed to the Federal Registry.
          Draft EIS need 45 days and Final EIS need 30 days after being
          filed to the Federal Registry. Note that EIS are filed to the
          Federal Registry on the second Friday from their submission date
          to the EPA (including today if today is Friday). Additionally, the
          selected date cannot fall on a federal holiday nor a weekend.');
  }
}

/*
 *	Build and send HTML confirmation email
 */
function sendConfirmationEmail($email) {
    global $LOGFILE;
    global $EFILE_EMAIL_NOTIFY;
    global $POSTVARS;

    $shortname = htmlentities($POSTVARS['userID']);
    $eistitle = htmlentities($POSTVARS['Title']);
    $projname = htmlentities($POSTVARS['projectName']);
    $projid = htmlentities($POSTVARS['projectID']);
    $date = htmlentities(strftime('%F'));
    $time = htmlentities(strftime('%T'));

    $msg = "<html><body>";
    $msg .= "<p>Dear $shortname,</p>";
    $msg .= "<p>This e-mail confirms that your submission of the $projname " .
            "($projid) EIS was received by the EPA on $date at $time. Another ".
            "email will be sent with the status of your submission once " .
            "it has been processed by the EPA. Please keep this confirmation for your records.</p>";
    $msg .= "<p>For your information, the following documents were sent to the EPA:</p>\n";

    $fileinfo = preg_grep("/^FILESCSV: /", file($LOGFILE));
    $fileinfo = preg_replace("/^FILESCSV: /", "", $fileinfo);
    $msg .= '<table border-spacing="0" border="1" style="border-spacing: 0;border-width: 1px">' . "\n";
    $td = '<td style="border-width: 1px;padding: 3px">';

    $msg .= "<tr>";
    $msg .= "$td" . "Document Name" . "</td>" .
            "$td" . "Document Size (M)" . "</td>" .
            "$td" . "Pages" . "</td>";
    $msg .= "</tr>\n";
    foreach ($fileinfo as $file) {
        $parts = explodeCSV($file);
        if ((count($parts) == 1) && (preg_match("/^Filename: /", $file) == 1)) {
            continue;	// Skip CSV filename
        } else if (count($parts) != 3) {
            $msg .= "<tr>";
            $msg .= "<td colspan=3>$file</td>";
            $msg .= "</tr>\n";
            continue;	// Something messed up
        }
        if ($parts[2] == "Pages") {
            continue;	// Skip title line
        }
        $msg .= "<tr>";
        $msg .= "$td" . htmlentities($parts[0]) . "</td>" .
                "$td" . htmlentities($parts[1]) . "</td>" .
                "$td" . htmlentities($parts[2]) . "</td>";
        $msg .= "</tr>\n";
    }
    $msg .= "</table>\n";

    $msg .= "<p>Should you need to contact the EPA regarding this submission, please e-mail: <a href='mailto:EISFiling@epa.gov'>EISFiling@epa.gov</a></p>";
    $msg .= "<p>Thank you for using PALS to submit your EIS!</p>";

    /*$headers = "From: eFile Manager<noreply@epa.gov>\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    $to = $email;*/
    $subject = "EPA EIS filing submission confirmation - $eistitle";
    $mailer = new Mailer();
    $sent = $mailer->sendEmail($subject, $msg . "</body></html>", $email);
    if (!$sent) {
      logERROR('Could not send confirmation email');
    }

    /*
     *  Send a copy of confirmation email to Dynamo
     *  For debugging, use localhost if ISP blocks port 25
     */
    $msg .= "\n";
    $msg .= "<br/>\n";
    $msg .= "<pre>\n";
    $msg .= "Original recipient: $email\n";
    $msg .= "=================\n";
    $msg .= "Logfile: $LOGFILE\n";
    $msg .= "=================\n";
    $msg .= file_get_contents($LOGFILE);
    $msg .= "</pre>\n";
    $hostname = gethostname();
    $subject = "$hostname: $subject";
    if (isset($EFILE_EMAIL_NOTIFY)) {
      $msg .= "</body></html>";
      $sent = $mailer->sendEmail($subject, $msg, $EFILE_EMAIL_NOTIFY);
      if (!$sent) {
        logERROR('Could not send email to notification address');
      }
    }
}


/*
 *	Build and send error report
 */
function sendErrorEmail($email) {
    global $LOGFILE;
    global $JOBID;
    global $RESTART;
    global $POSTVARS;

    $eistitle = htmlentities($POSTVARS['Title']);
    $msg = "<html><body>";

    $date = htmlentities(strftime('%F'));
    $time = htmlentities(strftime('%T'));

    if (isset($JOBID)) {
        $msg .= "<p>Error encountered while processing job $JOBID on $date at $time.</p>";
    }
    if (isset($POSTVARS)) {
        $projname = htmlentities($POSTVARS['projectName']);
        $projid = htmlentities($POSTVARS['projectID']);
        $msg .= "<p>Project $projname ($projid)</p>";
    }
    if (isset($RESTART)) {
        $msg .= "<p>Job restart script at: <PRE>$RESTART</PRE>.</p>";
    } else {
        $msg .= "<p>No job restart script defined</p>";
    }

    /*$headers = "From: eFile Manager<noreply@epa.gov>\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";*/
    $hostname = gethostname();
    $subject = "$hostname: ERROR: EPA EIS filing submission - $eistitle";

    $msg .= "\n";
    $msg .= "<br/>\n";
    $msg .= "<pre>\n";
    $msg .= "=================\n";
    $msg .= "Logfile: $LOGFILE\n";
    $msg .= "=================\n";
    $msg .= file_get_contents($LOGFILE);
    $msg .= "</pre>\n";
    $msg .= "</body></html>";
    $mailer = new Mailer();
    $sent = $mailer->sendEmail($subject, $msg, $email);
    if (!$sent) {
      logERROR('Could not send error email');
    }
}

/*
 * Build User Error Email Notification
 */

function sendUserErrorEmail($msg_body) {
  global $POSTVARS;

  $eistitle = htmlentities($POSTVARS['Title']);
  $msg = "<html><body>";

  $date = htmlentities(strftime('%F'));
  $time = htmlentities(strftime('%T'));
  $email = '';
  if (isset($POSTVARS)) {
      $projname = htmlentities($POSTVARS['projectName']);
      $projid = htmlentities($POSTVARS['projectID']);
      $msg .= "<p>Project $projname ($projid)</p>";
      $email = $POSTVARS['userEmail']; //still goes to admin email for some reason
      logERROR("$email");
  } else {
    logERROR('No POSTVARS, cannot access user email');
    return null;
  }
  $msg .= '<p>A problem was encountered during the processing of your EIS eFile'.
          ' request:</p>';
  $msg .= '<p>' . $msg_body . '</p>';
  /*$headers = "From: eFile Manager<noreply@epa.gov>\n";
  $headers .= "MIME-Version: 1.0\n";
  $headers .= "Content-type: text/html; charset=iso-8859-1\n";*/
  $hostname = gethostname();
  $subject = "$hostname: ERROR: EPA EIS filing submission - $eistitle";
  $msg .= "</body></html>";
  $mailer = new Mailer();
  $sent = $mailer->sendEmail($subject, $msg, $email);
  if (!$sent) {
    logERROR('Could not send error email');
  }
}

//***MAIN***
//**********

if (! isset($PAGECOUNTER)) {
    quit("PAGECOUNTER utility not set, check config!");
}

if (! isset($EFILE_SUBMIT)) {
    quit("EFILE_SUBMIT utility not set, check config!");
}

if (! isset($_GET['jobid'])) {
    quit("No job ID!  Can't continue");
}
$JOBID = $_GET['jobid'];

if (! isset($_GET['start'])) {
    quit("No start index!  Can't continue");
}
$start = $_GET['start'];

$workingdir = "$JOB_DIR/$JOBID";
if (! is_dir("$workingdir")) {
    quit("Job directory for job $JOBID not found!  Can't continue");
}

$pdfsdir = "$workingdir/pdfs";			// downloaded PDFs
$submitcsv = "$workingdir/submit.csv";		// EIS submission data
$filescsv = "$workingdir/files.csv";		// List of files in EIS
$efiledata = "$workingdir/efile.data";		// Input to EPA-eFile program
$submitdata = "$workingdir/submitdata.php";	// Job spec incl list of files
$RESTART = "$workingdir/restart.sh";

$LOGFILE = "$workingdir/log.txt";
$NEEDS_RESTART = "$workingdir/NEEDS_RESTART";

unlink($NEEDS_RESTART);				// Clear restart flag, if any

$POSTVARS = unserialize(file_get_contents($submitdata));

// Only permit this script to be called from localhost
if (! isset($_GET['key'])) {
    quit("No key!  Can't continue");
}
$key = $_GET['key'];
if ($POSTVARS['secretKey'] != $key) {
    quit("Script can only be called from localhost!  Can't continue");
}

$selectedfiles = "";
if (isset($POSTVARS['selectedFiles'])) {
    $selectedfiles = $POSTVARS['selectedFiles'];
} else {
    quit("Missing selectedFiles in job request!  Can't continue");
}

$cdxuserid = "";
if (isset($POSTVARS['CDXUserId'])) {
    $cdxuserid = $POSTVARS['CDXUserId'];
} else {
    quit("Missing CDXUserId in job request!  Can't continue");
}

$email = "";
if (isset($POSTVARS['userEmail'])) {
    $email = $POSTVARS['userEmail'];
} else {
    quit("Missing user email in job request!  Can't continue");
}

$proposedCommentDays = '';
if (isset($POSTVARS['ProposedCommentDays'])) {
  $proposedCommentDays = $POSTVARS['ProposedCommentDays'];
} else {
  quit("Missing end date for comment period");
}

$submittedType = '';
if (isset($POSTVARS['EISType'])) {
  $submittedType = $POSTVARS['EISType'];
} else {
  quit('Must submit a type of EIS');
}

checkProposedCommentDay($proposedCommentDays, $POSTVARS['EISType']);


// Make sure $pdfsdir exists
if (! is_dir($pdfsdir)) {
    if (! mkdir($pdfsdir)) {
        quit("Could not create PDF directory $pdfsdir, can't continue");
    }
}

$filelist = preg_split("/\|/", $selectedfiles);
$timeparts = preg_split("/ /", microtime());	// [ decimalusec seconds ]
$timestamp = ($timeparts[1] * 1000) + round($timeparts[0] * 1000);

// Create CSV file titles
$submitcsvtitle = "$cdxuserid Submission Metadata $timestamp.csv";
$filescsvtitle = "$cdxuserid File Metadata $timestamp.csv";

$batchsize = 0;
$batch = array();
unlink($efiledata);		// Reset efile request for next batch
resetFilesCSV($filescsv);	// Reset file csv for next batch

$total = count($filelist);
logINFO("Starting job ($total files, numbered 0-" . strval($total-1) . ")");
logINFO("Processing files $start to " . strval($total-1));

for ($i = $start; $i < $total; $i++) {
    $filedata = preg_split("/,/", $filelist[$i]);
    if (count($filedata) != 2) {
        quit("Bad file index at entry $i [" . $filelist[$i] . "]!  Can't continue");
    }
    $docname = urldecode($filedata[0]);
    $docid   = urldecode($filedata[1]);

    // If document is sensitive, skip to next doc
    try {
        $metadata = dmd_getMetadata($docid);
    } catch (Exception $e) {
        quit("Error retrieving metadata for $docid: " . $e->getMessage());
    }
    $sensitiveflag = dmd_issensitive($metadata);
    if ($sensitiveflag) {
        logINFO("Skipping sensitive doc [$i/" . strval($total-1) . "]: $docid");
        continue;
    }

    logINFO("Packaging doc [$i/" . strval($total-1) . "]: $docid");
    $tmpfile = "$pdfsdir/$docid.pdf";
    getPDF($docid, $tmpfile);
    $filesize = filesize($tmpfile);
    $batchsize += $filesize;

    $pdfpages = countPages($docid, $tmpfile);

    // add data to file CSV
    addToFileCSV($filescsv, "$docname.pdf", $filesize, $pdfpages);

    // add data to efile.data
    // Each line includes file name, path, and mime-type
    addToEfileData($efiledata, "$docname.pdf", $tmpfile, "application/pdf");

    $batch[$i] = $docname;
}

$txid = 0;
// Submit final batch
if ($batchsize > 0) {
    // add CSVs to efile.data
    addToEfileData($efiledata, $submitcsvtitle, $submitcsv, "text/csv");
    addToEfileData($efiledata, $filescsvtitle, $filescsv, "text/csv");

    logDEBUG("====================");
    logDEBUG("Submitting batch, " . strval(count($batch)) . " files, " . strval(round($batchsize / 1024, 2)) . "kb");
    $txid = submitFiles($efiledata);
    logINFO("Contents:");
    foreach ($batch as $idx => $filename) {
        logINFO("[$idx] $filename");
    }
    logINFO("Batch size: " . round($batchsize / 1024, 2) . "kb");
    logFile("SUBMITCSV", $submitcsv, $submitcsvtitle);
    logFile("FILESCSV", $filescsv, $filescsvtitle);
    logDEBUG("====================");
    emptyDirectory($pdfsdir);
    updateRestartScript($RESTART, $total);
}
logINFO("Job complete");


// When everything's done, send the confirmation mail to the user email
sendConfirmationEmail($email);



// Save job log on server
// Archived log file name is datetime + jobid + projectid + username
$tstamp = strftime('%Y%m%d_%H%M%S');
$projid = $POSTVARS['projectID'];
$shortname = $POSTVARS['userID'];
$logfilename = "${tstamp}_${JOBID}_${projid}_${shortname}.log";
try {
  // Can't use AWS to store data because of PHE
  // aws_putfile_private("efile/logs/$logfilename", $LOGFILE);
  copy($LOGFILE, "$EFILE_ARCHIVE_DIR/$logfilename");
} catch (Exception $e) {
  quit("Error writing logfile: " . $e->getMessage());
}

$title = $POSTVARS['Title'];
$projName = $POSTVARS['projectName'];
$db = get_db_handle();
$stmt = $db->prepare("INSERT INTO status_jobs(txid, userEmail, userId, title, projectName, projectId) ".
                    "VALUES (:txid, :email, :shortname, :title, :projName, :projid)");

$vals = array(':txid' => $txid,
              ':email' => $email,
              ':shortname' => $shortname,
              ':title' => $title,
              ':projName' => $projName,
              ':projid' => $projid);

if ($stmt->execute($vals)) {
  //TODO See if this is actually needed...
} else {
  //TODO add error handling. This is a very necessary step, so probably have to
  //figure out some errors, handle them, and then email the user to contact
  //emnepa support if it fails
}

//check for pid file to see if daemon is running
$pidExists = file_exists('/var/www/efile/status_daemon.pid');
if (!$pidExists) {
  exec("/var/www/html/downloader/efile/daemon/status_daemon.php > " .
       "/dev/null 2>&1 &");
}

// Clean up, empty workingdir
emptyDirectory($pdfsdir);
rmdir($pdfsdir);
emptyDirectory($workingdir);
rmdir($workingdir);

?>
