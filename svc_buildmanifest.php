<?php
/*
 *  Manifest-building service for downloader Index UI
 *
 *  Request consists of POST with list of files in body, and a jobid
 *  parameter in the URL.
 *
 *  Body consists of
 *  urlencoded archive title, vbar,
 *  urlencoded metadata column spec, vbar,
 *  followed by array of vbar-separated strings
 *  Strings consist of comma-separated urlencoded properties in the order:
 *    path,filename,docid
 *
 *  The filename is the document's title.  Actual document filenames are
 *  generally not very descriptive so the title is used as the new filename
 *  in the archive.
 *
 *  Filenames need to be unique within a single category, so if multiple
 *  documents have the same title, the filename will be disambiguated with
 *  a two-digit extension.  If more than 99 documents have the same title
 *  in the same category, the archive will fail.
 *
 *  The title does not normally indicate the type of the file, and in any
 *  case there is some logic to determine which rendition of the file will
 *  be included in the archive.  If the filetype (based on the original
 *  filename) is of a type that we expect to be able to convert to PDF, we
 *  include the PDF version.  Otherwise the original file is included in
 *  the archive.  The list of convertible filetypes is (case insensitive):
 *  doc,bmp,docx,dot,gif,htm,html,jpeg,jpg,png,ppt,rtf,txt,wpd,pdf
 *
 *  During the archiving process, files are written to a working directory
 *  at /var/www/html/tmp/downloader/jobs/[JOBID].  This lets the user see
 *  the current job status by polling a web-viewable file.
 *
 *  Status messages are periodically written to status.html in the working
 *  directory for this job (jobs/[JOBID]/status.html)
 *
 *  When this program exits, the printed output is returned to the client
 *  as a final status message.
 *
 *  For the most part, we don't expect the project file set to change and/or
 *  to be rearranged while the user is working on it, although this is
 *  possible.  Rearranging files is probably benign, but removing files may
 *  cause the archive process to fail.
 *
 *  Possible job status values:
 *  - WAITING
 *  - WORKING
 *  - SUCCESS or ERROR
 *
 *  Canceling a job results in an ERROR status
 *
 *  Possible errors:
 *  - Error getting document (may have been deleted while user was editing
 *    the package?)
 *  - Error creating hierarchy (bad file/dir name?)
 *  - Out of space?
 *  - Permissions?
 *  - Error uploading to edge server?
 */
require_once('aws_utils.php');
require_once('dmd_utils.php');
require_once('datamart_utils.php');
require_once('manifest_utils.php');
require_once('config.php');

error_reporting(0);

$DATAMART = new datamart_utils();

/*
 *  Define globals
 */
$JOBID = $_GET['jobid'];
if (!isset($JOBID)) { echo "$STATUS_ERROR: No jobid specified"; exit(1); }

$ORIGONLY_FLAG = $_GET['origonly'];
if ($ORIGONLY_FLAG === "1") {
    $PDF_CONVERSION_TYPES = array();	// Only download originals
}

$FOLDERSTRATEGY = $_GET['folderstrategy'];

$ARCHIVE_METADATA = array();	// Metadata for all archived files

$UNIQUE_FILELIST = array();	// List of unique filenames (lowercased)

/*
 *  Update job status HTML file
 */
function updateStatus($status, $msg) {
  global $JOB_DIR, $JOBID, $STATUSFILE;

  $statusfile = "$JOB_DIR/$JOBID/$STATUSFILE";
  $fp = fopen($statusfile, "w");
  fwrite($fp, "<div id='status-code'>Status: ${status}</div>$msg");
  fclose($fp);
}

function checkCancel() {
  global $JOB_DIR, $JOBID, $CANCELFILE;

  $cancelfile = "$JOB_DIR/$JOBID/$CANCELFILE";
  return file_exists($cancelfile);
}

/*
 *	Replace nonprintable characters and reserved Windows metachars
 *	\/:*?"<>|
 */
function sanitizename($str) {
  // Remove nonprinting chars
  $str = preg_replace('/[[:cntrl:]]+/', '', $str);

  // Remove illegal filename chars
  $search = array('\\', '/', ':', '*', '?', '"', '<', '>', '|');
  $replace = "_";
  $str = str_replace($search, $replace, $str);

  // Remove non-ASCII chars
  $str = preg_replace('/[^(\x20-\x7F)]+/','_', $str);

  return $str;
}

function getFile($docid, $filename, $pdf_flag) {
  $fp = fopen($filename, "w");
  if ($fp == null) {
    quit("Could not open file $filename");
  }
  try {
    if ($pdf_flag) {
      fwrite($fp, dmd_getPDF($docid));
    } else {
      fwrite($fp, dmd_getFile($docid));
    }
  } catch (Exception $e) {
    quit("Error retrieving file: " . $e->getMessage());
  }
  fclose($fp);
}

/*
 *  Mark error status and quit
 */
function quit($msg) {
  global $STATUS_ERROR;
  updateStatus($STATUS_ERROR, $msg);
 // echo $msg;
 // exit(1);
}

/*
 *	Archive a single document, keep metadata in list for manifest
 *	$filespec is a comma-separated list of urlencoded fields:
 *
 *	field 1: Path to file (using / as directory seperators, and
 *		each directory name is urlencoded separately
 *	field 2: File name
 *	field 3: Docid
 *
 *	NOTE: Any albums in the job spec need to be expanded into a list of
 *	individual filespec entries prior to calling this function.
 *
 *	$folderstrategy can be either "nested" (default) or "flat"
 *	If $folderstrategy == "flat" then store all documents in the same
 *	folder.  This will increase the possibility of name collisions, but
 *	the final document paths will be shorter.
 */
function processFilespec($filespec, $folderstrategy) {

    global $ARCHIVE_METADATA;
    global $UNIQUE_FILELIST;
    global $MAX_MEMBER_LENGTH, $MAX_PATH_LENGTH;
    global $STATUS_ERROR;

    // split filespec into properties
    $props = explode(",", $filespec);
    $path = urldecode($props[0]);
    $filename = urldecode($props[1]);
    $docid = urldecode($props[2]);

    // urldecode and sanitize path elements
    // Note incoming path ends with a slash
    // mkdir -p path (in working_dir)
    if (substr($path, -1) == "/") {
        $path = substr($path, 0, -1);	// remove trailing slash
    }
    $pathparts = explode("/", $path);
    $spath = "";

    if ($folderstrategy == "flat") {
        // Store all files in same folder (named after root of file tree)
        $pathelem = sanitizename(urldecode($pathparts[0]));
        if (strlen($pathelem) > $MAX_MEMBER_LENGTH) {
            $pathelem = substr(0, $MAX_MEMBER_LENGTH, $pathelem);
        }
        $spath = $pathelem;
    } else {
        // Default folderstrategy is "nested"
        foreach ($pathparts as $part) {
            $pathelem = sanitizename(urldecode($part));
            if (strlen($pathelem) > $MAX_MEMBER_LENGTH) {
              if (substr($pathelem,0,32) == "Original Letters and Submissions")
              $pathelem = substr($pathelem, 0, $MAX_MEMBER_LENGTH);
               //  quit("Path element too long, $pathelem &gt; $MAX_MEMBER_LENGTH");
            }
            $spath = "$spath/$pathelem";
        }
        $spath = substr($spath, 1);	// remove leading slash
    }

    if (is_dir("$spath") == False) {
        $rc = mkdir("$spath", 0777, True);
        if (!$rc) {
            quit("Could not create temp dir $spath");
        }
    }

    // Get unique target filename
    $sfilename = sanitizename($filename);
    $targetfname = "${spath}/${sfilename}";

    // Maximum path length for windows is 260, including null terminator,
    // drive letter, and colon.  There is a Unicode API for NTFS that permits
    // longer paths but we are probably not allowed to assume them, for backward
    // compatibility.

    // Check the length of the full path and filename.  MAX_PATH_LENGTH is
    // currently set at 250 characters.
    // This check is made without knowing the file extension type but we do
    // not allow extensions of longer than 4 characters.

    // If the dotted extension is longer than 4 characters long, assume it is
    // just part of the file name.

    // Windows file names are case-insensitive, so we need to make all of the
    // uniqueness checks case-insensitive too.

    if (strlen($targetfname) > $MAX_PATH_LENGTH) {
        if (strlen("${spath}/") > $MAX_PATH_LENGTH) {
            quit("Path too long, $targetfname &gt; $MAX_PATH_LENGTH");
        } else {
            // Desperation move: truncate filename
            $targetfname = substr(0, $MAX_PATH_LENGTH, $targetfname);
        }
    }

    // Make filename unique with a numeric suffix, if needed
    $orig_targetfname = $targetfname;
    $ctr = 0;
    while (in_array(strtolower($targetfname), $UNIQUE_FILELIST)) {
        $ctr += 1;
        if ($ctr > 99) {
            quit("Too many name conflicts for $orig_targetfname");
        }
        $targetfname = "${orig_targetfname}_" . sprintf("%02d", $ctr);
    }

    $UNIQUE_FILELIST[] = strtolower($targetfname);
    $sfilename = basename($targetfname);

    // Get original file metadata
    $metadata = array();
    try {
        $metadata = dmd_getMetadata($docid);
    } catch (Exception $e) {
        quit("Error retrieving metadata: " . $e->getMessage());
    }
    $metadata['sfilename'] = $sfilename;	// Sanitized filename

    // Set $metadata['sensitiveflag'] to true/false
    $metadata['sensitiveflag'] = dmd_issensitive($metadata);

    $orig_fname = trim($metadata['filename']);
    $orig_ftype = "";
    $dot_ext = "";

    // Get original file type
    if (preg_match('/\.[[:alnum:]+$\-]{1,10}$/', $orig_fname) > 0) {
        $orig_ftype = preg_replace('/^.*\.([[:alnum:]+$\-]{1,10})$/', '${1}', $orig_fname);
        $orig_ftype = strtolower($orig_ftype);
        $dot_ext = '.' . $orig_ftype;
    }

    // Determine file rendition to download
    if (in_array($orig_ftype, $PDF_CONVERSION_TYPES)) {
        $dot_ext = '.pdf';
        getFile($docid, "${targetfname}${dot_ext}", True);	// get PDF
    } else {
        getFile($docid, "${targetfname}${dot_ext}", False);	// get original
    }

    $metadata['path'] = "${targetfname}${dot_ext}";	// Path to file

    $metadata['docid'] = $docid;
    $ARCHIVE_METADATA[] = $metadata;

    // zip -r zipfile directory
    //// Do not need to create archive file for manifest building
    // $rc = 0;
    // $output = array();
    // $zipcmd = "zip -r archive \"${targetfname}${dot_ext}\"";
    // exec($zipcmd, $output, $rc);
    // if ($rc !== 0) {
    //     quit("Zip utility quit with error [$rc]");
    // }

    // Remove working file
    unlink("${targetfname}${dot_ext}");

}

function calculate_directories_from_path($path, &$file_category, &$file_group, &$file_subgroup)
{
	$directories = explode("/", $path);
	$directories_count = count($directories) - 2; 

	//$directories[1] = Category 
	//$directories[2] = Group 
	//$directories[3] = Subgroup 

	$file_category = $directories[1];
	$file_group = "";
	$file_subgroup = "";

	if ($directories_count >= 2)
	{
		$file_group = $directories[2];
		
		if ($directories_count >= 3)
		{
			$file_subgroup = $directories[3];
		}
	}
}

function buildManifest($archive_name, $column_spec) {
    global $ARCHIVE_METADATA;
    global $STATUS_ERROR;

    // Build manifest
    // Format provided by column_spec
    // (Old default: Category, Doc Name, Link, Date, Author, Description)
    $manifest_data = array();

    foreach ($ARCHIVE_METADATA as $metadata) {
		
		$file_category = "";
		$file_group = "";
		$file_subgroup = ""; 
		
		calculate_directories_from_path($metadata['path'], $file_category, $file_group, $file_subgroup);
		
        $category = $file_category;
        $doctitle = $metadata['sfilename'];
        $link = $metadata['path'];
        $docdate = trim($metadata['moreMetadata']['docdate']);
        
        // 3/4/2014 - Changed to use author specified in metadata, rather than uploader
  	
        if((isset($metadata['moreMetadata']['level3id']) && !is_null($metadata['moreMetadata']['level3id']) && $metadata['moreMetadata']['level3id'] == 57) || (isset($metadata['moreMetadata']['caraid']) && !is_null($metadata['moreMetadata']['caraid'])) || (isset($metadata['moreMetadata']['caraletterid']) && !is_null($metadata['moreMetadata']['caraletterid'])) || (isset($metadata['moreMetadata']['caraphaseid']) && !is_null($metadata['moreMetadata']['caraphaseid']))) {
        $author = trim($metadata['author']);
         } else {
        $author = trim($metadata['moreMetadata']['docauthor']);
        }
     
        $description = trim($metadata['moreMetadata']['docsummary']);

        $group = $file_group;
        $subgroup = $file_subgroup; 
        $filename = trim($metadata['filename']);
        $pubdate = trim($metadata['pubdate']);
        // NOTE: "recipient" comes from the "addressee" moreMetadata
        $recipient = trim($metadata['moreMetadata']['addressee']);
        $sensitiveflag = $metadata['sensitiveflag'];

        $row = array();
        $row[FL_CATEGORY] = $category;
        $row[FL_DOCTITLE] = $doctitle;
        $row[FL_DOCDATE] = $docdate;
        $row[FL_AUTHOR] = $author;
        $row[FL_DESCRIPTION] = $description;

        $row[FL_LINK] = $link;

        $row[FL_GROUP] = $group;
        $row[FL_SUBGROUP] = $subgroup;
        $row[FL_FILENAME] = $filename;
        $row[FL_PUBDATE] = $pubdate;
        $row[FL_RECIPIENT] = $recipient;

        $row[FL_SENSITIVEFLAG] = $sensitiveflag;

        $manifest_data[] = $row;
    }

    try {
/*
        // Build CSV manifest
        $manifest_fname="${archive_name}.csv";
        manifest_build_csv($archive_name, $column_spec, $manifest_data, $manifest_fname);
*/

        // Build Excel manifest
        $manifest_fname="${archive_name}.xlsx";	// Build Excel2007 file
        manifest_build_xls($archive_name, $column_spec, $manifest_data, $manifest_fname);
  
   } catch (Exception $e) {
        quit("Error building manifest: " . $e->getMessage());
    }

    //// Do not need to create archive file for manifest building
    // $rc = 0;
    // $output = array();
    // $zipcmd = "zip -r archive \"$manifest_fname\"";
    // exec($zipcmd, $output, $rc);
    // if ($rc !== 0) {
    //     quit("Zip utility quit with error [$rc]");
    // }
}

/*
 *	Process job request
 */
function main() {
    global $JOB_DIR, $JOBID;
    global $STATUS_WORKING, $STATUS_ERROR;
    global $ARCHIVE_URLBASE;
    global $FOLDERSTRATEGY;
    global $DATAMART;

    // Change current dir to working dir
    $working_dir = "$JOB_DIR/$JOBID";
    $dir = opendir($working_dir);
    if (!$dir) { echo "$STATUS_ERROR: Working dir not available?"; exit(1); }
    closedir($dir);
    chdir($working_dir);

    touch("$working_dir/DOWNLOAD_JOB");

    updateStatus($STATUS_WORKING, "Started processing");

    $request_data = file_get_contents('php://input');

    // parse request data into title, column spec, project ID, and file list
    $fileAr = explode("|", $request_data);

    $archive_name = sanitizename(urldecode($fileAr[0]));
    $column_spec = urldecode($fileAr[1]);
    $projectid = urldecode($fileAr[2]);
    $fileAr = array_slice($fileAr, 3);
    if (strlen($fileAr[0]) == 0) {
        quit("No files specified");
    }

    updateStatus($STATUS_WORKING, "Expanding albums");
    $newFileAr = array();
    foreach ($fileAr as $line) {
        $fields = explode(",", $line);
        $docid = $fields[2];
        if ($docid == "ALBUM") {
            $path = urldecode($fields[0]);
            $foldername = urldecode($fields[1]);
            $contid = trim($fields[3]);
            $phaseid = trim($fields[4]);
            $album_files = $DATAMART->datamart_expandAlbum($projectid, $contid, $phaseid);
            foreach ($album_files as $newfile) {
                $newfilespec = urlencode($path . $newfile['path'])
                . "," . urlencode($newfile['filename'])
                . "," . urlencode($newfile['docid']);
                $newFileAr[] = $newfilespec;
            }
        } else {
            $newFileAr[] = $line;
        }
    }
    $fileAr = $newFileAr;

    $numfiles = count($fileAr);
    updateStatus($STATUS_WORKING, "Processing $numfiles files");

    $filenum = 0;

    $folderstrategy = "nested";
    if ($FOLDERSTRATEGY == "flat") {
        $folderstrategy = "flat";
    }

    foreach ($fileAr as $file) {
        if (checkCancel()) {
            quit("Manifest job canceled by user");
        }
        $filenum += 1;
        updateStatus($STATUS_WORKING, "Processing file $filenum of $numfiles");
        processFilespec($file, $folderstrategy);
    }

    updateStatus($STATUS_WORKING, "Building manifest");
    buildManifest($archive_name, $column_spec);

    // Upload manifest into edge server, compute URL
    $manifest_fname="${archive_name}.xlsx";
    $filesize_b = filesize($manifest_fname);
    $filesize_k = strval(intval($filesize_b / 1024));
    ////$archive_zipfile_name = "${archive_name}.zip";

    updateStatus($STATUS_WORKING, "Uploading ${filesize_k}k manifest to file server");
    try {
        aws_putfile($manifest_fname, $manifest_fname);
    } catch (Exception $e) {
        quit("Error uploading file to AWS: " . $e->getMessage());
    }

    $link="$ARCHIVE_URLBASE/$manifest_fname";
    $finalmsg = "Manifest complete!<br/>Processed " . count($fileAr) . " files<br/><br/><div id='archive-link'><a href='$link'>Download link (${filesize_k}k)</a></div>";
    updateStatus($STATUS_SUCCESS, $finalmsg);
    echo $finalmsg;
}

/*
 *  Kick off all processing here
 */
main();
?>

