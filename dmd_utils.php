<?php
error_reporting(0);

require_once('config.php');

// Utility function to issue HTTP GET requests
function dmd_get($url, $timeout=0) {
  global $httpcode;
  global $STELLENT_AUTH;
  $httpcode = null;

  $ch=curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_USERPWD, $STELLENT_AUTH);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  if ($timeout > 0) {
    // Tried using CURLOPT_CONNECTTIMEOUT but this does not work
    // when the host cannot be contacted at all
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
  }
  $resp = curl_exec($ch);
  $info = curl_getinfo($ch);
  $httpcode = $info["http_code"];
  curl_close($ch);
  return $resp;
}

// Get test file
function dmd_status() {
  global $httpcode;
  global $STELLENT_TESTFILE;
  try {
    $resp = dmd_getFile($STELLENT_TESTFILE, 30);	// use timeout
    return True;
  } catch (Exception $e) {
    return False;
  }
}

// Get regular file
function dmd_getFile($fileID, $timeout=0) {
  global $httpcode;
  global $STELLENT_IPADDR;

  $url = "http://$STELLENT_IPADDR/web/idcplg?IdcService=GET_FILE&RevisionSelectionMethod=Latest&dDocName=$fileID&Rendition=Primary";
  $resp = dmd_get($url, $timeout);
  if ($httpcode == 200) {
      return $resp;
  } else {
      throw new Exception("Error $httpcode while retrieving file $fileID");
  }
}

// Get PDF rendition of a file
function dmd_getPDF($fileID, $timeout=0) {
  global $STELLENT_IPADDR;

  $url = "http://$STELLENT_IPADDR/web/idcplg?IdcService=GET_FILE&RevisionSelectionMethod=Latest&dDocName=$fileID&Rendition=Web";
  return dmd_get($url, $timeout);
}

// Get file metadata
function dmd_getMetadata($fileID) {
  global $STELLENT_IPADDR;

  $url = "http://$STELLENT_IPADDR/web/idcplg?IdcService=DOC_INFO_BY_NAME&dDocName=$fileID&IsJava=1";
  $hda_response = dmd_get($url);
  $hda = explode("\n", $hda_response);
  // skip to @ResultSet DOC_INFO
  $idx = array_search("@ResultSet DOC_INFO", $hda);
  if ($idx === False) {
    throw new Exception("Could not get metadata for $fileID");
  }
  $idx = intval($idx);
  // get number of lines
  $idx += 1;
  $numLines = intval($hda[$idx]);
  // read key names
  $idx += 1;
  $keys = array();
  foreach (range(0, $numLines - 1) as $delta) {
    $keyspec = explode(" ", $hda[$idx + $delta]);
    $keys[$delta] = $keyspec[0];
  }
  // read values
  $idx += $numLines;
  $values = array();
  foreach (range(0, $numLines - 1) as $delta) {
    $values[$keys[$delta]] = $hda[$idx + $delta];
  }
  // read @end
  $idx += $numLines;
  if ($hda[$idx] !== "@end") {
    throw new Exception("Invalid metadata for $fileID");
  }

  // Metadata consists of:
  // author (xOriginalAuthor)
  // title (dDocTitle)
  // pubdate (xPubDate)
  // appsysid (xAppSystemID)
  // appdoctype (xAppDocumentType)
  // moreMetadata (xMoreMetadata)
  // filename (dOriginalName)
  // filesize (dFileSize)
  $metadata = array();
  $metadata['author'] = $values['xOriginalAuthor'];
  $metadata['title'] = $values['dDocTitle'];
  $metadata['pubdate'] = $values['xPubDate'];
  $metadata['appsysid'] = $values['xAppSystemID'];
  $metadata['appdoctype'] = $values['xAppDocumentType'];
  $metadata['filename'] = $values['dOriginalName'];
  $metadata['filesize'] = $values['dFileSize'];
  $jsondata = strval($values['xMoreMetadata']);
  $jsondata = preg_replace("/\\\\\\\\/", '\\', $jsondata);
  $moreMetadata = json_decode($jsondata, $assoc=True);
  $metadata['moreMetadata'] = $moreMetadata;
  return $metadata;
}

// Get normalized sensitiveflag value (true/false)
function dmd_issensitive($metadata) {
    $sensitiveflag = false;
    if (isset($metadata['moreMetadata'])) {
        $moreMetadata = $metadata['moreMetadata'];
        if (isset($moreMetadata['sensitiveflag'])) {
            $flagstring = strtolower(trim($moreMetadata['sensitiveflag']));
            switch ($flagstring) {
                case 'true':
                case '1':
                    $sensitiveflag = true;
                    break;
                case 'false':
                case '0':
                    $sensitiveflag = false;
                    break;
            }
        }
    }
    return $sensitiveflag;
}

?>
