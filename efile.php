<?php
/*
 *  Downloader variant for selecting EIS files to be distributed via
 *  EPA eNEPA e-File.
 */
if (!file_exists('config.php')) {
	// config.php file missing
	echo "<H1>System error, can't continue (code 01)</H1>";
	exit(0);
}
require_once('config.php');

/* Make sure required definitions were set in the config file */
if (!isset($STELLENT_IPADDR)) {
	// STELLENT_IPADDR not set
	echo "<H1>System error, can't continue (code 02)</H1>";
	exit(0);
}

if (!isset($DATAMART_PREFIX)) {
	// DATAMART_PREFIX not set
	echo "<H1>System error, can't continue (code 03)</H1>";
	exit(0);
}

if (!isset($DATAMART_AUTH)) {
	// DATAMART_AUTH not set
	echo "<H1>System error, can't continue (code 04)</H1>";
	exit(0);
}

if (!isset($EMAIL_ALERT)) {
	// EMAIL_ALERT not set
	echo "<H1>System error, can't continue (code 05)</H1>";
	exit(0);
}

if (!isset($EFILE_EMAIL_NOTIFY)) {
	// EFILE_EMAIL_NOTIFY not set
	echo "<H1>System error, can't continue (code 06)</H1>";
	exit(0);
}

require_once("efile_secure.php");
/*
 *  Perform security checks (IP bans, bad request parameters)
 *  * If the request fails for any reason that might indicate a manually
 *    constructed URL, ban the IP address
 *  * Always submit an alert email before an IP ban
 */
$DECODED_PROJECTID = "";
$DECODED_AUTHOR = "";
$source_ip = $_SERVER['REMOTE_ADDR'];

if (checkban($source_ip) !== False) {
    emailalert("Attempt to access eFile through blocked IP addr $source_ip");
    header("Location: restricted.php");
    exit(0);	// Silent exit
}

// On initial load, resubmit the request a 2nd time with obfuscated parameters
if (isset($_GET['author']) || isset($_GET['projectid'])) {
    $author = "";
    if (isset($_GET['author'])) {
        $author = $_GET['author'];
    }
    $projectid = "";
    if (isset($_GET['projectid'])) {
        $projectid = $_GET['projectid'];
    }
    $time = time();
    $args['author'] = $author;
    $args['projectid'] = $projectid;
    $args['time'] = $time;
    $key = encodeargs($args);
    header("Location: efile.php?key=$key");
    exit(0);
}

// On the 2nd request, decode obfuscated parameters
$key = "";
if (isset($_GET['key'])) {
    $key = $_GET['key'];
} else {
    emailalert("eFile panel request without key parameter, banning ip $source_ip");
    banip($source_ip);
    echo "<H1>System error, can't continue (code 07)</H1>";
    exit(0);
}
$args = array();
try {
    $args = decodeargs($key);
} catch (Exception $e) {
    emailalert("Error decoding arguments: " . $e->getMessage());
    banip($source_ip);
    echo "<H1>System error, can't continue (code 08)</H1>";
    exit(0);
}
$DECODED_AUTHOR = $args['author'];
$DECODED_PROJECTID = $args['projectid'];
$request_time = $args['time'];

// If request is > 5 min old, fail request
// (don't ipban because this could block legitimate requests)
$gmtime = time();
if ($gmtime - $request_time > 300) {
    echo "<H1>Request expired, please restart from PALS window</H1>";
    exit(0);
}

require_once('datamart_utils.php');
require_once('dmd_utils.php');

$DATAMART = new datamart_utils();

/* Make sure page was invoked with required parameters */
$projectid = htmlentities($DECODED_PROJECTID);
if (!$projectid) {
	// No project ID given
        emailalert("No project ID");
        banip($source_ip);
        echo "<H1>System error, can't continue (code 09)</H1>";
	exit(0);
}

$author = htmlentities($DECODED_AUTHOR);
if (!$author) {
	// No author name given
        emailalert("No author shortname");
        banip($source_ip);
        echo "<H1>System error, can't continue (code 10)</H1>";
	exit(0);
}

/* Create archive title */
$today = date('Y-m-d');
$project_title = "$projectid EIS Filing Form $today";

/* Make sure EPA-eFile.properties file was configured */
if (count(preg_grep("/^endpoint/", file("/var/www/efile/EPA-eFile.properties"))) != 1) {
	// eFile utility /var/www/efile/EPA-eFile.properties misconfigured
        emailalert("/var/www/efile/EPA-eFile.properties misconfigured");
        echo "<H1>System error, can't continue (code 11)</H1>";
}

/* Make sure systems are available */
if ($DATAMART->datamart_status() != 200) {
        emailalert("Datamart not responding");
	echo "<H1>Error, system not available</H1>";
	echo "<div>Please try again later (code 12)</div>";
	exit(0);
}

if (dmd_status() == False) {
	echo "<H1>Error, document repository server not responding</H1>";
	echo "<div>Please try again later (code 13)</div>";
	exit(0);
}

/* Load user data */
try {
        $USERDATA = $DATAMART->datamart_getUserData($author);
} catch (Exception $e) {
        emailalert("Author record ${author} not found");
        banip($source_ip);
        echo "<H1>Error loading data for user ${author}</H1>";
        echo "<H2>" . $e->getMessage() . "</H2>";
        exit(0);
}

/* Load project data */
try {
        $PROJECTDATA = $DATAMART->datamart_getProjectData($projectid);
} catch (Exception $e) {
        emailalert("Project record ${projectid} not found");
        banip($source_ip);
        echo "<H1>Error loading data for project ${projectid}</H1>";
        echo "<H2>" . $e->getMessage() . "</H2>";
        exit(0);
}

/* Make sure user is allowed to access the given project */
/* Authorized roles are PALS_ADMIN, PALS_MANAGER, or SOPA_MANAGER */
$adminunitid = $PROJECTDATA['unitcode'];
//$authorized_roles = array(1, 2, 3);		//SRC 4/12/2013 ER-73 - Allow any user with a unit role on the LMU for a project to Efile
$authorized = False;
foreach ($USERDATA['roles'] as $roleline) {
    $roledata = explode("|", $roleline);
    $unit = $roledata[0];
    $role = $roledata[1];
    $unit = preg_replace("/(00)*$/", "", $unit);
    if (preg_match("/^$unit/", $adminunitid)) {
        //if (in_array($role, $authorized_roles)) {	//SRC 4/12/2013 ER-73
            $authorized = True;
            break;
        //}	//SRC 4/12/2013 ER-73
    }
}
if (! $authorized) {
    emailalert("User $author not authorized to submit EIS for project ${projectid}, user must have a role of PALS Administrator, PALS Program Manager, or SOPA Manager to use this service.");
    banip($source_ip);
    echo "<H1>System error, can't continue (code 14)</H1>";
    echo "<H2>There was a problem validating your access to the EIS submission tool</H2>";
    echo "<H2> You must have a role of PALS Administrator, PALS Program Manager, or SOPA Manager to use this service</H2>";
    echo "<H2>We regret the inconvenience</H2>";
    $PALSHELP = "pals-help@fs.fed.us";
    echo "<H2>Please contact <A HREF='mailto:$PALSHELP'>$PALSHELP</A></H2>";
    echo "<H2>Thanks!</H2>";
    exit(0);
}

/* Load project containers in Dynatree format */
$DATAMART->datamart_setModeEfile();
try {
        $dynatree = $DATAMART->datamart_getContainersAsDynatree($projectid, "/Public Involvement/i");
} catch (Exception $e) {
        echo "<H1>Error loading containers for project ${projectid}</H1>";
        echo "<H2>" . $e->getMessage() . "</H2>";
        exit(0);
}

/* Stop IE8 from modifying the page for XSS */
header('X-XSS-Protection: 0');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Project <?php echo $project_title; ?></title>

<link type="text/css" href="jquery/css/custom-theme/jquery-ui-1.8.13.custom.css" rel="Stylesheet" />
<link rel='stylesheet' type='text/css' href='dynatree/src/skin/ui.dynatree.css'>
<link rel="stylesheet" type="text/css" href="efile.css">
<?php
/*
 * After combining, replace the above with
<link rel="stylesheet" type="text/css" href="combined-min.css">
 */
?>

<script type="text/javascript">

  var _gaq = _gaq || [];

<?php if (isset($GA_TRACKER_ID)) { ?>

  _gaq.push(['_setAccount', '<?php echo $GA_TRACKER_ID; ?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

<?php } ?>

</script>

<!-- Include the required JavaScript libraries: -->
<script src='jquery/js/jquery-1.8.2.min.js' type="text/javascript"></script>
<script src='jquery/js/jquery-ui-1.8.24.custom.min.js' type="text/javascript"></script>
<script src='dynatree/jquery/jquery.cookie.js' type="text/javascript"></script>
<script src='dynatree/src/jquery.dynatree.js' type="text/javascript"></script>

</head>
<body>
<div id="titleBanner">
  <img id="clipboardImg" src="assets/clipboard.gif"/>
  <div id="titleStripe">
    <div id="title">Project <?php echo $project_title; ?></div>
  </div>
</div>
<br/>
<div id="content">
<div id="moreInfoBox">
  <a class="popup" href="http://www.epa.gov/compliance/nepa/submiteis/index.html">
  <img src="assets/external-link-ltr-icon.png"/> Submitting EIS document(s) guidance</a>
</div>
<div class="sectionTitle">File Selection</div>
<div class="elementBox">
  <div id="selectAllBtn" class="btn select-button selectall">Select All</div>
  <div id="expandAllBtn" class="btn expand-button expandall">Expand All</div>
</div>
<div class="elementBox">
  <div id="totalSelected"></div>
  <div id="fileSelector"></div>
</div>
<div class="sectionTitle">EIS Form</div>
<div id="efile">
  <?php include("efile/efile_controls.php"); ?>
</div>
<div class="nofloat">
  <div id="submitBtn" class="btn submit-button">Submit to eFile</div><br/>
</div>
</div>

<script type="text/javascript">

  var selected_filecnt = 0;
  var selected_filesize = 0;
  var project_filecnt = 0;

  $(function () {

    // Discourage caching
    $.ajaxSetup({ cache: false });

    // Make button text unselectable
    $.fn.disableSelection = function() {
        return this
                 .attr('unselectable', 'on')
                 .css('user-select', 'none')
                 .on('selectstart', false);
    };

    // Init button UIs and disable text selection
    $(".btn").button().disableSelection();

    // Init moreInfoBox links
    $('.popup').click(function() {
        var win = window.open($(this).prop('href'), '',
            'height=600,width=800,scrollbars=yes');
        if (window.focus) {
            win.focus();
        }
        return false;
    });

    // Get dynatree data
    json_data = $.parseJSON('<?php echo $dynatree; ?>');
    project_filecnt = '<?php echo $DATAMART->G_DOC_COUNT; ?>';
    projectid = '<?php echo $projectid; ?>';
    project_title = '<?php echo $project_title; ?>';

    // Define download button action (create archive)
    $("#submitBtn").click(
      function() {
        var checkedlist = [];

        // Get list of doc IDs in "checkedlist"
        selectedlist = $("#fileSelector").dynatree("getSelectedNodes");
        $.each(selectedlist,
          function() {
            node = this;
            if (node.data.isFolder == false) {
              checkedlist.push(node.data.key);
            }
          }
        );

        // Validate form
        formdata = validateEfileForm();

	if ($(".warning").length > 0) {
          var msg = '<div class="confirm-title">Submission Error</div>There are invalid fields in your EIS Form, please check the highlighted fields and try again!';
          showWarnings(msg);
        } else if (checkedlist.length == 0) {
          var msg = '<div class="confirm-title">Submission Error</div>You have not selected any files, please check your file selections and try again!';
          showWarnings(msg);
        } else {
          showConfirmation(checkedlist, formdata);
        }
      }
    );

    function showWarnings(msg) {
      // Show warning dialog
      $("<div><br/></div>")
      .find("br").after(msg).end()
      .css('text-align', 'center')
      .dialog({
        resizable: false,
        modal: true,
        buttons: {
          "OK": function() {
            $( this ).dialog( "destroy" );
          }
        }
      })
      .dialog('widget')
      .addClass('modalDialog');       // hide titlebar via css

      // adjust button placement
      $('.modalDialog .ui-dialog-buttonset').position({
        my: 'center bottom',
        at: 'center bottom',
        of: $('.modalDialog')
      });
    }

    /*
     *  Get displayable field data for confirmation panel
     *
     *  formdata - wrapped set of form fields
     *  id       - id of field value to retrieve
     */
    function getEISField(formdata, id) {
        displayval = "";
        e = formdata.filter("#" + id);
        if (e.is('input')) {
            displayval = e.val();
        } else if ($(e).is('select')) {
            if (id == "subjectSelector") {
                displayval = $(e).find("option:selected").text();
            } else if (id == "subCatSelector") {
                displayval = $(e).find("option:selected").text();
            } else if (id == "KeyState") {
                displayval = $(e).find("option:selected").text();
            } else
                displayval = $(e).find("option:selected").val();
        } else if ($(e).is('div.multiselect')) {
            multivalues_display = "";
            $(e).find("select.chosen option").each(function(i,e) {
                if (multivalues_display != "") {
                    multivalues_display += ", ";
                }
                multivalues_display += $(e).val();
            });
            displayval = multivalues_display;
        }
        return displayval.replace(/&/g, '&amp;')	// Escape HTML
                         .replace(/>/g, '&gt;')
                         .replace(/</g, '&lt;')
                         .replace(/"/g, '&quot;');
    }

    /*
     *  Display confirmation screen and prompt user for action
     *
     *  checkedlist is a Javascript array of selected files
     *  formdata is a wrapped set of form elements (usually <div>)
     */
    function showConfirmation(checkedlist, formdata) {

      var title = "<div>Submission confirmation</div>"

      var table = "<div><table>";

      table += '<tr><td>EIS Title</td>';
      table += '<td>' + getEISField(formdata, 'Title') + '</td></tr>';

      table += '<tr><td>EIS Type</td>';
      table += '<td>' + getEISField(formdata, 'EISType') + '</td></tr>';

      table += '<tr><td>Lead Agency</td>';
      table += '<td>USDA Forest Service</td></tr>';

      table += '<tr><td>Contact Name</td>';
      table += '<td>' + getEISField(formdata, 'LeadContactName') + '</td></tr>';

      table += '<tr><td>Contact Phone</td>';
      table += '<td>' + getEISField(formdata, 'LeadContactPhone') + '</td></tr>';

      table += '<tr><td>Contact Email</td>';
      table += '<td>' + getEISField(formdata, 'LeadContactEmail') + '</td></tr>';

      table += '<tr><td>Other Lead Agencies</td>';
      table += '<td>' + getEISField(formdata, 'OtherLeadAgencies') + '</td></tr>';

      table += '<tr><td>Cooperating Aencies</td>';
      table += '<td>' + getEISField(formdata, 'CoopFedAgencies') + '</td></tr>';

      table += '<tr><td>Other</td>';
      table += '<td>' + getEISField(formdata, 'OtherCoopAgencies_multi') + '</td></tr>';

      table += '<tr><td>EIS Comment Period Date</td>';
      table += '<td>' + getEISField(formdata, 'ProposedCommentDays') + '</td></tr>';

      table += '<tr><td>Key State</td>';
      table += '<td>' + getEISField(formdata, 'KeyState') + '</td></tr>';

      table += '<tr><td>Other States</td>';
      table += '<td>' + getEISField(formdata, 'OtherStates') + '</td></tr>';

      table += '<tr><td>EIS Subject - Category</td>';
      table += '<td>' + getEISField(formdata, 'subjectSelector') + '</td></tr>';

      table += '<tr><td>EIS Subject - Subcategory</td>';
      table += '<td>' + getEISField(formdata, 'subCatSelector') + '</td></tr>';

      table += '<tr><td>Project Website Link</td>';
      table += '<td>' + getEISField(formdata, 'ProjectWebsiteLink') + '</td></tr>';

      table += "</table></div>";

      var myprompt = "<div>You selected " + checkedlist.length + " of " +
                project_filecnt + " files";
      myprompt += "<br/>(total " + selected_filesize + "kb)";
      myprompt += "<br/>Do you want to submit these items?</div>";

      $("<div>")
      .append($(title).addClass('confirm-title'))
      .append($(table).addClass('confirm-table'))
      .append($(myprompt).addClass('confirm-prompt'))
      .dialog({
        resizable: false,
        width: 'auto',	// broken for IE7?
        modal: true,
        buttons: {
          "Submit": function() {
            $( this ).dialog( "destroy" );
            disableUI();
            publishFiles();
            $('#efileForm').submit();
          },
          "Cancel": function() {
            $( this ).dialog( "destroy" );
          }
        }
      })
      .dialog('widget')
      .addClass('modalDialog');       // hide titlebar via css

      // adjust button placement
      $('.modalDialog .ui-dialog-buttonset').position({
        my: 'center bottom',
        at: 'center bottom',
        of: $('.modalDialog')
      });
    }


    // Define other button actions
    // Select/deselect all documents
    $("#selectAllBtn").click(
        function() {
            if ($(this).hasClass("selectall")) {
                // Select all nodes
                $("#fileSelector").dynatree('getRoot').visit(function(node) {
                  node.select(true);
                });
                // Change button text
                $(this).button("option", "label", "Deselect All");
            } else {
                // Deselect all nodes
                $("#fileSelector").dynatree('getRoot').visit(function(node) {
                  node.select(false);
                });
                // Change button text
                $(this).button("option", "label", "Select All");
            }
            $(this).toggleClass("selectall");
            return false;
        }
    );



    // Expand all folders
    $("#expandAllBtn").click(
        function() {
            if ($(this).hasClass("expandall")) {
                // Expand all nodes
                $("#fileSelector").dynatree("getRoot").visit(function(node){
                  node.expand(true);
                });
                // Change button text
                $(this).button("option", "label", "Collapse All");
            } else {
                // Collapse all nodes
                $("#fileSelector").dynatree("getRoot").visit(function(node){
                  node.expand(false);
                });
                // Change button text
                $(this).button("option", "label", "Expand All");
            }
            $(this).toggleClass("expandall");
            return false;
        }
    );



    // Display running total of selected files and filesize (from globals)
    // See countSelectedFiles()
    function displayTotal() {
      $("#totalSelected").html("Selected " + selected_filecnt + " of " + project_filecnt + " files, estimated file size " + selected_filesize + "kb");
    }



    // Disable all buttons and tree controls on main page
    function disableUI() {
      $(".btn").button("disable");
      $("#fileSelector").dynatree("disable");
    }



    // Given a Dynatree node (representing a document),
    // create a "file spec" string in the list of documents to be archived
    // A file spec consists of the filename and docid (path not needed)
    // The filename, and docid are URI-encoded
    function addPubFile(publist, node) {
      var filename = node.data.filename;
      var docid = node.data.key;
      publist.push(encodeURIComponent(filename) +','+ encodeURIComponent(docid));
    }



    // Recompute count/size of selected files
    // See displayTotal()
    function countSelectedFiles() {
      selected_filecnt = 0;
      selected_filesize = 0;
      selectedlist = $("#fileSelector").dynatree("getSelectedNodes");
      $.each(selectedlist,
        function() {
          var node = this;
          if (node.data.isFolder == false) {
            if (node.data.isAlbum == true) {
              selected_filecnt += node.data.filecnt;
              selected_filesize += node.data.size;
            } else {
              selected_filecnt += 1;
              selected_filesize += node.data.size;
            }
          }
        }
      );
    }



    // Publish the selected files to an efile job spec
    //
    // Collect list of selected files
    // Reserve a job ID for the archive job (call svc_takeanumber.php)
    function publishFiles() {

      $('#projectID').val('<?php echo $projectid; ?>');
      $('#projectName').val('<?php echo $PROJECTDATA['name']; ?>');
      $('#userID').val('<?php echo $author; ?>');
      $('#userEmail').val('<?php echo $USERDATA['email']; ?>');
      $('#userFirstname').val('<?php echo $USERDATA['firstname']; ?>');
      $('#userLastname').val('<?php echo $USERDATA['lastname']; ?>');

      // Get list of documents to publish in "publist"
      publist = new Array();
      selectedlist = $("#fileSelector").dynatree("getSelectedNodes");
      $.each(selectedlist,
        function() {
          var node = this;
          if (node.data.isFolder == false) {
            addPubFile(publist, node);
          }
        }
      );

      // Assemble job request
      //
      // selectedfiles is a string of vbar-separated fields
      // Each field describes a file to be submitted (see addPubFile())
      // All fields are URI-encoded
      selectedfiles = publist.join("|");
      $('#selectedFiles').val(selectedfiles);

    } // End of function publishFiles()



    // Create dynatree
    // Attach the dynatree widget to an existing <div> element
    // and pass the tree options as an argument to the dynatree() function:
    $("#fileSelector").dynatree({
      cookieId: "efile",
      checkbox: true,
      selectMode: 3,
      persist: true,
      children: json_data,
      onSelect: function(flag, node) {
<?php
 /*
  * This seems terribly inefficient but appears to work well in practice
  */
 ?>
        countSelectedFiles();
        displayTotal();
      },
      cookie: {
        expires: 90
      }
    });

		//setup OtherCoopAgency list
		var numberOfAddedAgencies = 1;
		$('body').on('keyup', '.otherCoopAgency', function() {
			var childNum = $(this).parent().index('.otherCoopAgDiv');
			if (!this.value) {
				$(this).parent().children('.otherCoopAgDiv').eq(childNum + 1).remove();
				if (childNum > 0) {
					$(this).addClass('translucent');
				}
			} else {
				if ($(this).hasClass('translucent')) {
					$(this).removeClass('translucent');
				}
				var totalChildren = $(this).parent().parent().children('.otherCoopAgDiv').length;
				if (childNum + 1 >= totalChildren) {
					//add another text box
					addAnotherTextBox($(this).parent().parent());
				}
			}
		});

		$('body').on('click', '.removeAg', function() {
			$(this).parent().parent().remove();
		});

		function addAnotherTextBox(parent) {
			parent.append('<div class="otherCoopAgDiv">' +
			                '<span class="otherCoopAgInstructions">' +
											  'Enter next Other Cooperating Agency here' +
											'</span>' +
											'<input class="otherCoopAgency" ' +
											  'id="otherCoopAgency' + numberOfAddedAgencies + '" type="text"/>' +
											'<a class="removeOtherAgDiv">' +
											  '<span class="ui-icon ui-icon-circle-close removeAg"/>' +
											'</a>' +
											'<div class="error"></div>' +
										'</div>');

		  var elId = '#otherCoopAgency' + numberOfAddedAgencies;
			$(elId).addClass("translucent");
			numberOfAddedAgencies += 1;
		}

    // Set root title
    $("#fileSelector").dynatree('getRoot').data.title = project_title;

    // Count selected files
    countSelectedFiles();
    displayTotal();

    // Prevalidate form
    validateEfileForm();

  });
</script>
</body>
</html>
