<?php
error_reporting(-1);
require_once('sdk-1.4.4/sdk-1.4.4/sdk.class.php');
require_once('config.php');

    function aws_putfile($fileid, $localpath) {
        global $AWS_BUCKET;
        $s3 = new AmazonS3();
        $response = $s3->create_object($AWS_BUCKET, $fileid, array(
            'fileUpload' => $localpath,
            'acl' => AmazonS3::ACL_PUBLIC
        ));
        return $response;
    }

    function aws_putfile_private($name, $localpath) {
        global $AWS_BUCKET;
        $s3 = new AmazonS3();
        $response = $s3->create_object($AWS_BUCKET, $name, array(
            'fileUpload' => $localpath,
            'acl' => AmazonS3::ACL_PRIVATE
        ));
        return $response;
    }

    function aws_getfile($fileid, $localpath) {
        global $AWS_BUCKET;
        $s3 = new AmazonS3();
        $response = $s3->get_object($AWS_BUCKET, $fileid, array(
            'fileDownload' => $localpath
        ));
        return $response;
    }

    function aws_deletefile($fileid) {
        global $AWS_BUCKET;
        $s3 = new AmazonS3();
        $response = $s3->delete_object($AWS_BUCKET, $fileid);
        return $response;
    }

    function aws_listfiles($prefix, $pcre_filename_pattern) {
        global $AWS_BUCKET;
        $s3 = new AmazonS3();
        $response = $s3->get_object_list($AWS_BUCKET, array(
            'pcre' => $pcre_filename_pattern,
            'prefix' => $prefix
        ));
        return $response;
    }
?>
