<?php if (!class_exists('CFRuntime')) die('No direct access allowed.');
/**
 * Stores your AWS account information. Add your account information, and then rename this file
 * to 'config.inc.php'.
 *
 * @version 2011.06.02
 * @license See the included NOTICE.md file for more information.
 * @copyright See the included NOTICE.md file for more information.
 * @link http://aws.amazon.com/php/ PHP Developer Center
 * @link http://aws.amazon.com/security-credentials AWS Security Credentials
 */


/**
 * Amazon Web Services Key. Found in the AWS Security Credentials. You can also pass this value as the first
 * parameter to a service constructor.
 */
#define('AWS_KEY', '1JTETT70V8M3H7EZ6QG2');
define('AWS_KEY', 'AKIAKP67DUWUTV42D3MA');


/**
 * Amazon Web Services Secret Key. Found in the AWS Security Credentials. You can also pass this value as
 * the second parameter to a service constructor.
 */
#define('AWS_SECRET_KEY', '9tx4kfX7wJ9WR9vJQ5btsxKshJcWNQX6M3wjv+X7');
define('AWS_SECRET_KEY', 'EcqBUgeaZrQ4C9AFs7zAiesn4GG9OdILqzyTdhLT');

/**
 * Amazon Account ID without dashes. Used for identification with Amazon EC2. Found in the AWS Security
 * Credentials.
 */
#define('AWS_ACCOUNT_ID', '844567719136');
define('AWS_ACCOUNT_ID', '401957505027');

/**
 * Your CanonicalUser ID. Used for setting access control settings in AmazonS3. Found in the AWS Security
 * Credentials.
 */
#define('AWS_CANONICAL_ID', '0deb97cc24a92c706c17078ff3562ea3e674b7af1c034b0ff3845a09244d1257');
define('AWS_CANONICAL_ID', '5ef05c2a66e077d22c7320126ea9f448e94238d65ed97181096bdaee8a15567f');


/**
 * Your CanonicalUser DisplayName. Used for setting access control settings in AmazonS3. Found in the AWS
 * Security Credentials (i.e. "Welcome, AWS_CANONICAL_NAME").
 */
#define('AWS_CANONICAL_NAME', 'Phase One FS eMNEPA');
define('AWS_CANONICAL_NAME', 'aws-usda-cara G2224.014');


/**
 * Determines which Cerificate Authority file to use.
 *
 * A value of boolean `false` will use the Certificate Authority file available on the system. A value of
 * boolean `true` will use the Certificate Authority provided by the SDK. Passing a file system path to a
 * Certificate Authority file (chmodded to `0755`) will use that.
 *
 * Leave this set to `false` if you're not sure.
 */
define('AWS_CERTIFICATE_AUTHORITY', false);

/**
 * This option allows you to configure a preferred storage type to use for caching by default. This can
 * be changed later using the set_cache_config() method.
 *
 * Valid values are: `apc`, `xcache`, a DSN-style string such as `pdo.sqlite:/sqlite/cache.db`, a file
 * system path such as `./cache` or `/tmp/cache/`, or a serialized array for memcached configuration.
 *
 * serialize(array(
 * 	array(
 * 		'host' => '127.0.0.1',
 * 		'port' => '11211'
 * 	),
 * 	array(
 * 		'host' => '127.0.0.2',
 * 		'port' => '11211'
 * 	)
 * ));
 */
define('AWS_DEFAULT_CACHE_CONFIG', '');

/**
 * 12-digit serial number taken from the Gemalto device used for Multi-Factor Authentication. Ignore this
 * if you're not using MFA.
 */
define('AWS_MFA_SERIAL', '');

/**
 * Amazon CloudFront key-pair to use for signing private URLs. Found in the AWS Security Credentials. This
 * can be set programmatically with <AmazonCloudFront::set_keypair_id()>.
 */
define('AWS_CLOUDFRONT_KEYPAIR_ID', '');

/**
 * The contents of the *.pem private key that matches with the CloudFront key-pair ID. Found in the AWS
 * Security Credentials. This can be set programmatically with <AmazonCloudFront::set_private_key()>.
 */
define('AWS_CLOUDFRONT_PRIVATE_KEY_PEM', '');

/**
 * Set the value to true to enable autoloading for classes not prefixed with "Amazon" or "CF". If enabled,
 * load `sdk.class.php` last to avoid clobbering any other autoloaders.
 */
define('AWS_ENABLE_EXTENSIONS', 'false');
