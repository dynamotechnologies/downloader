<?php
date_default_timezone_set('America/New_York');

/** PHPExcel */
require_once '/var/www/common/phpexcel/Classes/PHPExcel.php';
PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
define('PCLZIP_TEMPORARY_DIR', '/tmp/');	// workaround for http://phpexcel.codeplex.com/discussions/361285

/* Mapping between metadata code letters and field names */
require_once 'config.php';

/*
 *	Various methods for building manifest spreadsheets
 *
 *	Columns are specified in a CSV string as letter codes (from a-j)
 *
 *	Default output columns: Category, Doc Title, Date, Author, Description
 *
 *	Ideally Doc Title will also link to the file
 */

/*
 *	Reformat an ISO datetime into a mm/dd/yyyy date
 *	Assumes YYYY-MM-DDTHH:MM:SSZ (Lucene/docdate)
 */
function docdate2mdy($date) {
    $result = $date;
    if (preg_match("/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z.*$/", $date) > 0) {
      $result = preg_replace('/^(\d{4})-(\d{2})-(\d{2}).*$/', '${2}/${3}/${1}', $date);
    }
    return $result;
}

/*
 *	Reformat an ISO datetime into a mm/dd/yyyy date
 *	Assumes YYYYMMDDTHH:MM:SS (XMLRPC/pubdate)
 */
function pubdate2mdy($date) {
    $result = $date;
    if (preg_match("/^\d{4}\d{2}\d{2}T\d{2}:\d{2}:\d{2}.*$/", $date) > 0) {
      $result = preg_replace('/^(\d{4})(\d{2})(\d{2}).*$/', '${2}/${3}/${1}', $date);
    } else if (preg_match("/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*$/", $date) > 0) {
      $result = preg_replace('/^(\d{4})-(\d{2})-(\d{2}).*$/', '${2}/${3}/${1}', $date);

	}
    return $result;
}

/*
 *	Quote a string value for a CSV file
 */
function csv_quote($str) {
  $str = preg_replace('/"/', '""', $str);
  return '"' . $str . '"';
}

/*
 *	Build a manifest in CSV format at $filename
 *
 *	$manifest_data is an array of associative arrays with keys:
 *	  FL_CATEGORY
 *	  FL_GROUP
 *	  FL_SUBGROUP
 *	  FL_DOCTITLE
 *	  FL_FILENAME
 *	  FL_DOCDATE
 *	  FL_PUBDATE
 *	  FL_AUTHOR
 *	  FL_RECIPIENT
 *	  FL_DESCRIPTION
 *	  FL_LINK
 *	  FL_PDFPAGECOUNT
 *
 *	key names defined as constants in includes.php
 */
function manifest_build_csv($title, $column_spec, $manifest_data, $filename) {
  global $METADATA_FIELDS, $METADATA_TITLES;

  $csv_data = array();
  $columns = split(',', $column_spec);

  // Create title row
  $titles = array();
  foreach ($columns as $col) {
    $titles[] = $METADATA_TITLES[$METADATA_FIELDS[$col]];
  }

  $csv_data[] = implode(",", $titles);

  foreach ($manifest_data as $metadata) {

    // Create data row
    $csv_row = array();
    foreach ($columns as $col) {
      $fieldname = $METADATA_FIELDS[$col];
      $data = $metadata[$fieldname];
      switch($fieldname) {
        case FL_DOCTITLE:
          $data = csv_quote('=HYPERLINK("' . $metadata[FL_LINK] . '", "' . $data . '")');
          break;
        case FL_DOCDATE:
          $data = docdate2mdy($data);
          break;
        case FL_PUBDATE:
          $data = pubdate2mdy($data);
          break;
        default:
          $data = csv_quote($data);
          break;
      }
      $csv_row[] = $data;
    }
    $csv_data[] = implode(",", $csv_row);
  }

  $fp = fopen($filename, "w");
  if ($fp == null) {
    quit($STATUS_ERROR, "Could not open file $filename");
  }
  foreach ($csv_data as $row) {
    try {
      fwrite($fp, $row . "\n");
    } catch (Exception $e) {
      quit($STATUS_ERROR, "Error writing to $filename: "
      . $e->getMessage());
    }
  }
  fclose($fp);
}

/*
 * Underline cells
 * $xls is the PHPExcel handle
 * $cells should look like "A1:E1"
 */
function xls_underline($xls, $cells) {
    $xls->getActiveSheet()->getStyle($cells)->applyFromArray(
      array('borders' => array(
        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
      ))
    );
}

/*
 * Center cells
 * $xls is the PHPExcel handle
 * $cells should look like "A1:E1"
 */
function xls_center($xls, $cells) {
    $xls->getActiveSheet()->getStyle($cells)->applyFromArray(
      array('alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
      )
    );
}

/*
 *	Columns 3 and 4 (Date and Author) are center-justified
 */
function assign_cell($sheet, $celladdr, $column, $metadata) {
    $value = $metadata[$column];
    switch($column) {
        case FL_DOCTITLE:
          if ($metadata[FL_SENSITIVEFLAG]) {
              $value = "[SENSITIVE] $value";
          }
          // Excel 2007 has trouble with links that contain embedded spaces,
          // encoding spaces with %20 appears to take care of the problem.
          // Excel 2010 does not seem to have this problem and the issue did
          // not surface with files coded in Excel5 (binary) format.
          $link = str_replace(" ", "%20", $metadata[FL_LINK]);
          $sheet->setCellValue($celladdr, $value);
          $sheet->getCell($celladdr)->getHyperlink()->setUrl($link);
          break;
        case FL_DOCDATE:
          $value = docdate2mdy($value);
          $sheet->setCellValue($celladdr, $value);
          break;
        case FL_PUBDATE:
          $value = pubdate2mdy($value);
          $sheet->setCellValue($celladdr, $value);
          break;
        default:
          $sheet->setCellValue($celladdr, $value);
          break;
    }
}

/*
 *	Build spreadsheet in Excel format
 *
 *	$manifest_data is an array of associative arrays with keys:
 *	  FL_CATEGORY
 *	  FL_GROUP
 *	  FL_SUBGROUP
 *	  FL_DOCTITLE
 *	  FL_FILENAME
 *	  FL_DOCDATE
 *	  FL_PUBDATE
 *	  FL_AUTHOR
 *	  FL_RECIPIENT
 *	  FL_DESCRIPTION
 *	  FL_LINK
 *	  FL_PDFPAGECOUNT
 *
 *	key names defined as constants in includes.php
 *	Columns defined in column_spec
 *
 *	Rows 1 and 2 are title rows
 *
 *	Default spreadsheet has 5 columns, columns A-E
 *	Date and Author columns are center-justified
 */
function manifest_build_xls($title, $column_spec, $manifest_data, $filename) {
  global $METADATA_FIELDS, $METADATA_TITLES;

  // Create new PHPExcel object
  $xls = new PHPExcel();

  // Set properties
  $xls->getProperties()->setCreator("PALS Downloader")
      ->setTitle($title)
      ->setSubject("Collected NEPA project documents")
      ->setDescription("Download archive manifest");

  $xls->setActiveSheetIndex(0);

  // Get number of columns, translate to column name
  $columns = explode(',', $column_spec);
  $numcolumns = count($columns);
  $lastcolumn = PHPExcel_Cell::stringFromColumnIndex($numcolumns - 1);

  // Make all columns autowidth
  for ($idx = 0; $idx < $numcolumns; $idx++) {
    $col = PHPExcel_Cell::stringFromColumnIndex($idx);
    $xls->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
  }

  // Add main title
  $xls->getActiveSheet()->setCellValue('A1', $title);
  xls_center($xls, 'A1');
  $xls->getActiveSheet()->mergeCells("A1:${lastcolumn}1");
  xls_underline($xls, "A1:${lastcolumn}1");

  // Add column titles
  foreach ($columns as $idx => $code) {
    $coltitle = $METADATA_TITLES[$METADATA_FIELDS[$code]];
    $col = PHPExcel_Cell::stringFromColumnIndex($idx);
    $xls->getActiveSheet()->setCellValue("${col}2", $coltitle);
  }
  $xls->getActiveSheet()->getStyle("A2:${lastcolumn}2")->applyFromArray(
      array('fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('argb' => 'FFBBBBBB')
      ))
  );

  // Add data
  $numRows = count($manifest_data);
  foreach ($manifest_data as $idx => $metadata) {
    $row = $idx + 3;
    foreach ($columns as $idx => $code) {
      $col = PHPExcel_Cell::stringFromColumnIndex($idx);
      assign_cell($xls->getActiveSheet(), "$col$row", $METADATA_FIELDS[$code], $metadata);
      $xls->getActiveSheet()->getStyle("$col$row")->getAlignment()->setWrapText(true);
    }

    xls_underline($xls, "A$row:$lastcolumn$row");
  }

  $lastrow = $numRows + 2;

  // Center Date and Author columns
  foreach ($columns as $idx => $code) {
    switch ($METADATA_FIELDS[$code]) {
      case FL_DOCDATE:
      case FL_PUBDATE:
      case FL_AUTHOR:
      $col = PHPExcel_Cell::stringFromColumnIndex($idx);
      xls_center($xls, "${col}2:$col$lastrow");
        break;
      default:
        break;
    }
  }

  // Apply font styling
  $xls->getActiveSheet()->getStyle("A1:$lastcolumn$lastrow")->getFont()->setName('Arial');
  $xls->getActiveSheet()->getStyle("A1:$lastcolumn$lastrow")->getFont()->setBold(true);
  $xls->getActiveSheet()->getStyle("A1:$lastcolumn$lastrow")->getFont()->setSize(10);

  // Save Excel file
  // $objWriter = new PHPExcel_Writer_Excel5($xls); // for Excel 5 (BIFF) format
  $objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
  $objWriter->save($filename);
}

?>
