<?php

// Process job
//
// URL parameters:
//   jobid	= name of job working directory
//   key	= secret string assigned during job submission
//   startfile	= index of next file to process (0-based)
//
// Create the job.db database (RedBean) if necessary
//
// If startfile == 0
//   scan the jobfile for album data and expand as needed
//
// Store stamped file data in job.db for building the manifest Excel
// spreadsheets later
//
// Fields consist of:
//   index (0-based)
//   sfilename - sanitized filename
//   path - full path to unique file
//   docid
//   metadata - serialized metadata from DMD, this should probably be a
//     really large clob just in case the metadata ever grows

require_once "../rb.php";
require_once "../aws_utils.php";
require_once "../dmd_utils.php";
require_once "../datamart_utils.php";
require_once "../manifest_utils.php";
require_once "../config.php";

$DATAMART = new datamart_utils();

// Logfile tags
define("L_DEBUG", "DEBUG");
define("L_INFO", "INFO");         // Generally data to be sent back to user
define("L_ERROR", "ERROR");

/*
 *      Replace nonprintable characters and reserved Windows metachars
 *      \/:*?"<>|
 */
function sanitizename($str) {
  // Remove nonprinting chars
  $str = preg_replace('/[[:cntrl:]]+/', '', $str);

  // Remove illegal filename chars
  $search = array('\\', '/', ':', '*', '?', '"', '<', '>', '|');
  $replace = "_";
  $str = str_replace($search, $replace, $str);

  // Remove non-ASCII chars
  $str = preg_replace('/[^(\x20-\x7F)]+/','_', $str);

  return $str;
}

function getFile($docid, $filename, $pdf_flag) {
  $fp = fopen($filename, "w");
  if ($fp == null) {
    quit("Could not open document $filename");
  }
  try {
    if ($pdf_flag) {
      fwrite($fp, dmd_getPDF($docid));
    } else {
      fwrite($fp, dmd_getFile($docid));
    }
  } catch (Exception $e) {
    quit("Error retrieving file: " . $e->getMessage());
  }
  fclose($fp);
}

/*
 *  Log message and exit
 */
function quit($msg) {
    global $NEEDS_RESTART;
    global $EMAIL_ALERT;

    echo $msg;
    logERROR($msg);
    if (isset($NEEDS_RESTART)) {
        touch($NEEDS_RESTART);  // signal for help
    }
    sendErrorEmail($EMAIL_ALERT);
    exit(1);
}

/*
 *	Build and send user HTML confirmation email
 */
function sendConfirmationEmail($emailaddr, $htmlmsg) {
    global $POSTVARS;

    // if files were excluded due to errors, switch to the "WithErrors"
    // confirmation email
    if (errorExists()) {
        return sendConfirmationWithErrorsEmail($emailaddr, $htmlmsg);
    }

    $shortname = htmlentities($POSTVARS['userID']);
    $archive_name = htmlentities($POSTVARS['archiveName']);
    $projname = htmlentities($POSTVARS['projectName']);
    $projid = htmlentities($POSTVARS['projectID']);
    $date = htmlentities(strftime('%F'));
    $time = htmlentities(strftime('%T'));

    $title = "$projname ($projid)";

    $msg = "<html><body>";
    $msg .= "<p>Dear $shortname,</p>";
    $msg .= "<p>Your request for a stamped set of documents from the $projname ($projid) project has been processed successfully.</p>";
    $msg .= $htmlmsg;
    $msg .= "<p>Thank you for using PALS!</p>";

    $headers = "From: Project File Stamper<noreply@fs.fed.us>\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    $to = $emailaddr;
    $hostname = gethostname();
    $subject = "$hostname: Project File Stamper completion $title";
    mail($to, $subject, $msg . "</body></html>", $headers);
}

/*
 *	Build and send user HTML confirmation email
 *
 */
function sendConfirmationWithErrorsEmail($emailaddr, $htmlmsg) {
    global $POSTVARS;

    $shortname = htmlentities($POSTVARS['userID']);
    $archive_name = htmlentities($POSTVARS['archiveName']);
    $projname = htmlentities($POSTVARS['projectName']);
    $projid = htmlentities($POSTVARS['projectID']);
    $date = htmlentities(strftime('%F'));
    $time = htmlentities(strftime('%T'));

    // $failed is an array of metadata for documents that failed to stamp
    // including $failed['reason'] = message describing reason for failure
    $failed = getErrors();
    $numfailed = count($failed);

    $title = "$projname ($projid)";

    $msg = "<html><body>";
    $msg .= "<p>Dear $shortname,</p>\n";
    if ($numfailed === 1) {
        $msg .= "<p>Your request for a stamped set of documents from the $projname ($projid) project included 1 file that could not be stamped by the system.  This file is included in your stamped set of documents and listed in the table at the end of this email.</p>\n";
        $msg .= "<p>To be stamped, a file must be in PDF format.  PALS attempts to convert all files to PDF, but not all files can be converted (such as video files).  You may attempt to re-stamp your project file after making any needed adjustments.</p>\n";
    } else {
        $msg .= "<p>Your request for a stamped set of documents from the $projname ($projid) project included $numfailed files that could not be stamped by the system.  These files are included in your stamped set of documents and listed in the table at the end of this email.</p>\n";
        $msg .= "<p>To be stamped, a file must be in PDF format.  PALS attempts to convert all files to PDF, but not all files can be converted (such as video files).  You may attempt to re-stamp your project file after making any needed adjustments.</p>\n";
    }
    $msg .= $htmlmsg;
    $msg .= "<p>Thank you for using PALS!</p>";
    $msg .= "<p>Unstamped files:</p>";

    $errors_beans = getErrors();
    $msg .= "<table border='1' style='border-spacing:0;border-width:1px'><tr>";
    // $msg .= "<td>Index</td>";
    $msg .= "<td>File name</td>";
    $msg .= "<td>Document ID</td>";
/*
    // Not including error messages/codes in email output anymore
    $msg .= "<td>Error message</td>";
    $msg .= "<td>Stamper exit code</td>";
*/
    $msg .= "</tr>";
    foreach ($errors_beans as $b) {
        $idx = $b->idx;
        $metadata = unserialize($b->metadata);
        $sfilename = htmlentities($metadata['sfilename']);
        $docid = htmlentities($metadata['docid']);
        $stamper_msg_ar = unserialize($b->stamper_msg);
        foreach (array_keys($stamper_msg_ar) as $key) {
            $stamper_msg_ar[$key] = htmlentities($stamper_msg_ar[$key]);
        }
        $stamper_msg = join("<br/>", $stamper_msg_ar);
        $stamper_rc = $b->stamper_rc;
        $msg .= "<tr>";
        // $msg .= "<td>$idx</td>";
        $msg .= "<td>$sfilename</td>";
        $msg .= "<td>$docid</td>";
/*
        $msg .= "<td>$stamper_msg</td>";
        $msg .= "<td>$stamper_rc</td>";
*/
        $msg .= "</tr>";
        $msg .= "\n";
    }
    $msg .= "</tr></table>";

    $headers = "From: Project File Stamper<noreply@fs.fed.us>\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    $to = $emailaddr;
    $hostname = gethostname();
    $subject = "$hostname: Project File Stamper completion $title";
    mail($to, $subject, $msg . "</body></html>", $headers);
}

/*
 *	Build and send user alert
 */
/*
function sendUserAlertEmail($alertmsg) {
    global $POSTVARS;

    $email = $POSTVARS['userEmail'];
    $shortname = htmlentities($POSTVARS['userID']);
    $archive_name = htmlentities($POSTVARS['archiveName']);
    $projname = htmlentities($POSTVARS['projectName']);
    $projid = htmlentities($POSTVARS['projectID']);
    $date = htmlentities(strftime('%F'));
    $time = htmlentities(strftime('%T'));

    $title = "$projname ($projid)";

    $msg = "<html><body>";
    $msg .= "<p>Dear $shortname,</p>";
    $msg .= "<p>Your request for a stamped set of documents from the $projname ($projid) project could not be completed due to the following error:</p>";
    $msg .= "<p><pre>$alertmsg</pre></p>";
    $PALSHELP = "pals-help@fs.fed.us";
    $msg .= "<p>Please contact <A HREF='mailto:$PALSHELP'>$PALSHELP</A> to resolve this issue.</p>";
    $msg .= "<p>Thank you!</p>";

    $headers = "From: Project File Stamper<noreply@fs.fed.us>\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    $to = $email;
    $hostname = gethostname();
    $subject = "$hostname: Project File Stamper job error $title";
    mail($to, $subject, $msg . "</body></html>", $headers);
}
*/

/*
 *      Build and send error report to POCG Ops
 */
function sendErrorEmail($email) {
    global $LOGFILE;
    global $JOBID;
    global $RESTART;
    global $POSTVARS;

    $msg = "<html><body>";

    $projname = "-projectname-";
    $projid = "-projectid-";
    $date = htmlentities(strftime('%F'));
    $time = htmlentities(strftime('%T'));
    $title = "-title-";

    if (isset($JOBID)) {
        $msg .= "<p>Error encountered while processing job $JOBID on $date at $time.</p>";
    }
    if (isset($POSTVARS)) {
        $projname = htmlentities($POSTVARS['projectName']);
        $projid = htmlentities($POSTVARS['projectID']);
        $title = "$projname ($projid)";
        $msg .= "<p>Project $title</p>";
    }
    if (isset($RESTART)) {
        $msg .= "<p>Job restart script at: <PRE>$RESTART</PRE>.</p>";
    } else {
        $msg .= "<p>No job restart script defined.</p>";
    }

    $headers = "From: Bates Stamper Process<noreply@fs.fed.us>\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    $hostname = gethostname();
    $subject = "$hostname: ERROR: Bates stamping for $title";

    $msg .= "\n";
    $msg .= "<br/>\n";
    $msg .= "<pre>\n";
    $msg .= "=================\n";
    $msg .= "Logfile: $LOGFILE\n";
    $msg .= "=================\n";
    $logfile = file($LOGFILE);
    if (count($logfile) < 100) {
        $msg .= implode("\n", $logfile);
    } else {
        $msg .= "First 50 lines:\n";
        $msg .= "---------------\n";
        $msg .= implode("\n", array_slice($logfile, 0, 50));
        $msg .= "---------------\n";
        $msg .= "Last 50 lines:\n";
        $msg .= "--------------\n";
        $msg .= implode("\n", array_slice($logfile, -50));
    }
    $msg .= "</pre>\n";
    mail($email, $subject, $msg . "</body></html>", $headers);
}

/*
 *  Prepend a timestamp to a log message
 */
function timestamp($msg) {
    $tstamp = strftime('%F %T');
    return "$tstamp $msg";
}

function logDEBUG($msg) { writeLogMsg(L_DEBUG, timestamp($msg)); }
function logINFO($msg) { writeLogMsg(L_INFO, timestamp($msg)); }
function logERROR($msg) { writeLogMsg(L_ERROR, timestamp($msg)); }

function writeLogMsg($prefix, $data) {
    global $LOGFILE;

    if (!isset($LOGFILE)) {
        exit(1);	// log file undefined
    }
    try {
        $fp = fopen($LOGFILE, "a");
        if ($fp == null) {
            quit("Could not open log file $LOGFILE");
        }
        fwrite($fp, "$prefix: $data\n");
        fclose($fp);
    } catch (Exception $e) {
        // Fatal condition, quit immediately
        echo "Could not open log file $LOGFILE: " . $e->getMessage();
        exit(1);
    }
}

/*
 *  Store a whole (user-readable) data file in the log
 */
function logFile($prefix, $file, $title) {
    $lines = array();
    $lines[] = "Filename: $title";
    try {
        $lines = array_merge($lines, file($file));
    } catch (Exception $e) {
        quit("Could not include file $file: " . $e->getMessage());
    }
    foreach ($lines as $line) {
        writelogMsg($prefix, rtrim($line));     // remove newline
    }
}

/*
 *	Stamp a document
 *
 *	Returns null, or an array containing:
 *	{
 *		messages => array of error messages
 *		rc => return code from Java stamper
 *	}
 *
 *	$params is an array containing the following values:
 *
 *	StampLocation (location)
 *	AddMargin (addmargin)
 *	Prefix (prefix)
 *	Suffix (suffix)
 *	TotalFiles (totalfiles)
 *	StartFileSeq (fileseq)
 *	StartPageInCollection (pageincollection)
 *	StampFormatCode (stampformat)
 *	TotalFilesFormat (totalfilesformat)
 *	TotalFilePagesFormat (totalfilepagesformat)
 *	FileSeqFormat (fileseqformat)
 *	PageInFileFormat (pageinfileformat)
 *	PageInCollectionFormat (pageincollectionformat)
 *	DateFormatCode (dateformat)
 *	StaticTime (statictime)
 *
 *	There is also a StartNumber parameter but this is used by the
 *	UI to populate either StartFileSeq or StartPageInCollection so it
 *	should be ignored
 *
 *	The stamper config file is stamper.conf
 *	When the stamper finishes successfully, the output is in
 *	output/1.pdf
 *	The text of the first stamp is on the first line in
 *	output/stamplog
 *	and the prebuilt config for the next file (without any files) is in
 *	output/nextjob.conf
 */
function stampFile($infile, $params) {
    global $STAMPER_CMD;

    $nextconfig = "output/nextjob.conf";	// generated config file
    $stamplog = "output/stamplog";		// generated list of stamps
    $config = "stamper.conf";

    if (file_exists($nextconfig)) {
        if (!copy($nextconfig, $config)) {
            quit("Error preparing next $config for stamper!");
        }
    } else {
        // Write new config file
        $fp = fopen($config, "w");
        if ($fp == false) {
            quit("Couldn't open $config for writing!");
        }
        fwrite($fp, "location:" . $params['StampLocation'] . "\n");
        fwrite($fp, "stampfontsize:" . $params['StampFontSize'] . "\n");
        fwrite($fp, "addmargin:" . $params['AddMargin'] . "\n");
        fwrite($fp, "prefix:" . $params['Prefix'] . "\n");
        fwrite($fp, "suffix:" . $params['Suffix'] . "\n");
        fwrite($fp, "totalfiles:" . $params['TotalFiles'] . "\n");
        fwrite($fp, "startfileseq:" . $params['StartFileSeq'] . "\n");
        fwrite($fp, "startpageincollection:" . $params['StartPageInCollection'] . "\n");
        fwrite($fp, "stampformat:" . $params['StampFormatCode'] . "\n");
        fwrite($fp, "totalfilesformat:" . $params['TotalFilesFormat'] . "\n");
        fwrite($fp, "totalfilepagesformat:" . $params['TotalFilePagesFormat'] . "\n");
        fwrite($fp, "fileseqformat:" . $params['FileSeqFormat'] . "\n");
        fwrite($fp, "pageinfileformat:" . $params['PageInFileFormat'] . "\n");
        fwrite($fp, "pageincollectionformat:" . $params['PageInCollectionFormat'] . "\n");
        fwrite($fp, "dateformat:" . $params['DateFormatCode'] . "\n");
        fwrite($fp, "statictime:" . $params['StaticTime'] . "\n");
        fwrite($fp, "::\n");
        fclose($fp);
    }

    // Write location of file
    $fp = fopen($config, "a");
    if ($fp == false) {
        quit("Couldn't open $config to attach target file!");
    }
    fwrite($fp, "$infile");
    fclose($fp);

    $stampcmd = "$STAMPER_CMD \"${config}\" 2>&1";
    exec($stampcmd, $output, $rc);
    // rc = 0  Success
    // rc = 1  Error (message in stderr)
    // rc = 2  Invalid PDF (stderr message "Invalid PDF: [pdfname]")
    if ($rc !== 0) {
        foreach ($output as $line) {
            logERROR($line);
        }
        $response['messages'] = $output;
        $response['rc'] = $rc;
        return $response;
    }
    // output is in output/1.pdf
    return null;
}

/*
 *	Process a single document, keep metadata in list for manifest
 *	$filespec is a comma-separated list of urlencoded fields:
 *
 *	field 1: Path to file (using / as directory seperators, and
 *		each directory name is urlencoded separately
 *	field 2: File name
 *	field 3: Docid
 *
 *	$idx is the 0-based index of the document in the list, which
 *	eventually provides a sort order for the metadata in the manifest
 *
 *	NOTE: Any albums in the job spec need to be expanded into a list of
 *	individual filespec entries prior to calling this function.
 *
 *	$folderstrategy can be either "nested" (default) or "flat"
 *	If $folderstrategy == "flat" then store all documents in the same
 *	folder.  This will increase the possibility of name collisions, but
 *	the final document paths will be shorter.
 */
function processFilespec($filespec, $idx, $folderstrategy) {

    global $MAX_MEMBER_LENGTH, $MAX_PATH_LENGTH;
    global $POSTVARS;

    $includeUnstampable = true;
/*
    Original behavior was to flag unstampable documents as errors,
    new behavior is to include them in the archive file and manifest, but
    without stamps.

    Future behavior may include assigning unique per-file stamp values to
    unstampable files (e.g. as part of file name or of an enclosing folder)
*/

    // split filespec into properties
    $props = explode(",", $filespec);
    $path = urldecode($props[0]);
    $filename = urldecode($props[1]);
    $docid = urldecode($props[2]);

    // urldecode and sanitize path elements
    // Note incoming path ends with a slash
    // mkdir -p path (in working_dir)
    if (substr($path, -1) == "/") {
        $path = substr($path, 0, -1);	// remove trailing slash
    }
    $pathparts = explode("/", $path);
    $spath = "";

    if ($folderstrategy == "flat") {
        // Store all files in same folder (named after root of file tree)
        $pathelem = sanitizename(urldecode($pathparts[0]));
        if (strlen($pathelem) > $MAX_MEMBER_LENGTH) {
            $pathelem = substr(0, $MAX_MEMBER_LENGTH, $pathelem);
        }
        $spath = $pathelem;
    } else {
        // Default folderstrategy is "nested"
        foreach ($pathparts as $part) {
            $pathelem = sanitizename(urldecode($part));
            if (strlen($pathelem) > $MAX_MEMBER_LENGTH) {
                $pathelem = substr(0, $MAX_MEMBER_LENGTH, $pathelem);
            }
            $spath = "$spath/$pathelem";
        }
        $spath = substr($spath, 1);	// remove leading slash
    }

    if (is_dir("$spath") == False) {
        $rc = mkdir("$spath", 0777, True);
        if (!$rc) {
            quit("Could not create temp dir $spath");
        }
    }

    // Get unique target filename
    $sfilename = sanitizename($filename);
    $targetfname = "${spath}/${sfilename}";

    // Maximum path length for windows is 260, including null terminator,
    // drive letter, and colon.  There is a Unicode API for NTFS that permits
    // longer paths but we are probably not allowed to assume them, for backward
    // compatibility.

    // Check the length of the full path and filename.  MAX_PATH_LENGTH is
    // currently set at 250 characters.
    // This check is made without knowing the file extension type but we do
    // not allow extensions of longer than 4 characters.

    // If the dotted extension is longer than 4 characters long, assume it is
    // just part of the file name.

    // Windows file names are case-insensitive, so we need to make all of the
    // uniqueness checks case-insensitive too.

    if (strlen($targetfname) > $MAX_PATH_LENGTH) {
        if (strlen("${spath}/") > $MAX_PATH_LENGTH) {
            quit("Path too long, $targetfname &gt; $MAX_PATH_LENGTH");
        } else {
            // Desperation move: truncate filename
            $targetfname = substr(0, $MAX_PATH_LENGTH, $targetfname);
        }
    }

    // Make targetfname unique with a numeric suffix, if needed
    $origtargetfname = $targetfname;
    if (nameExists($targetfname)) {
        $targetfname = "${targetfname}_$idx";
        if (nameExists($targetfname)) {
            quit("Unable to find unique name for $origtargetfname");
        }
    }

    $sfilename = basename($targetfname);

    // Get original file metadata
    $metadata = array();
    try {
        $metadata = dmd_getMetadata($docid);
    } catch (Exception $e) {
        quit("Error retrieving metadata: " . $e->getMessage());
    }
    $metadata['sfilename'] = $sfilename;	// Sanitized filename

    // Set $metadata['sensitiveflag'] to true/false
    $metadata['sensitiveflag'] = dmd_issensitive($metadata);

    $orig_fname = trim($metadata[FL_FILENAME]);
    $orig_ftype = "";
    $dot_ext = "";

    $origpdf = "${targetfname}.pdf";
    getFile($docid, $origpdf, True);	// get PDF

    $metadata['path'] = "$origpdf";	// Path to file
    $metadata['docid'] = $docid;

    // Stamp the PDF
    // stampFile creates output/ if necessary,
    // removes any existing nextjob.conf file,
    // stamps the file and stores it as 1.pdf
    // records the stamp text in stamplog
    // and creates a nextjob.conf for the next file

    $stampedpdf = "output/1.pdf";
    $stamplog = "output/stamplog";
    $temppdf = "temp.pdf";

    $stamperrors = stampFile($origpdf, $POSTVARS, $docid);

    if ($stamperrors == null) {

        // Stamp success
        // Replace original PDF with stamped PDF
        rename($origpdf, $temppdf);
        rename($stampedpdf, $origpdf);
        unlink($temppdf);

        // Store stamp text in Description field (moreMetadata/docsummary)
        $stamps = file($stamplog);
        if (($stamps === False) || (count($stamps) == 0)) {
            quit("Didn't record any stamp value for $sfilename");
        }

        $description = trim($metadata['moreMetadata']['docsummary']);
        $firststamp = trim($stamps[0]);
        $description = "[$firststamp]\n$description";
        $metadata['moreMetadata']['docsummary'] = $description;

        // store metadata in persistent db
        // use path+fname as unique key
        $b = R::dispense('metadata');
        $b->key = strtolower($targetfname);
        $b->metadata = serialize($metadata);
        $b->pdfpagecount = countPDFPages($origpdf);
        $b->idx = $idx;
        $b->filename = $sfilename;
        $b->sensitiveflag = $metadata['sensitiveflag'];
        R::store($b);

        // Stamped files will be periodically archived by updateZipArchive()

    } else {

        // Stamping failed

        if ($includeUnstampable) {
            // original PDF (or whatever was in web rendition)
            // will be archived as-is

            // Update Description field (moreMetadata/docsummary)
            $description = trim($metadata['moreMetadata']['docsummary']);
            $description = "File not stamped\n$description";
            $metadata['moreMetadata']['docsummary'] = $description;

            // store metadata in persistent db
            // use path+fname as unique key
            $b = R::dispense('metadata');
            $b->key = strtolower($targetfname);
            $b->metadata = serialize($metadata);
            $b->pdfpagecount = "";
            $b->idx = $idx;
            R::store($b);
        } else {
            // Remove source file
            unlink($origpdf);
        }

        // Record error to DB
        $b = R::dispense('errors');
        $b->metadata = serialize($metadata);
        $b->pdfpagecount = "";
        $b->idx = $idx;
        $b->stamper_msg = serialize($stamperrors['messages']);
        $b->stamper_rc = $stamperrors['rc'];
        R::store($b);
    }
}

/* Count # of files under a directory */
function countFiles($dirname) {
    $dirname = rtrim($dirname, "/");
    $total = 0;
    $dirlist = scandir($dirname);
    foreach ($dirlist as $entry) {
        if ($entry === ".") { continue; }
        if ($entry === "..") { continue; }
        if (is_dir("$dirname/$entry")) {
            $total += countFiles("$dirname/$entry");
        } else {
            $total += 1;
        }
    }
    return $total;
}

/* Sum sizes of files under a directory */
function totalFileSizes($dirname) {
    $dirname = rtrim($dirname, "/");
    $total = 0;
    $dirlist = scandir($dirname);
    foreach ($dirlist as $entry) {
        if ($entry === ".") { continue; }
        if ($entry === "..") { continue; }
        if (is_dir("$dirname/$entry")) {
            $total += totalFileSizes("$dirname/$entry");
        } else {
            $total += filesize("$dirname/$entry");
        }
    }
    return $total;
}

/*
 *  Take all files from $POSTVARS['archiveRoot'] and move to zipfile
 *
 *  The original zipfile format has a limit of 64k files per archive
 *  We limit the number of files to 50k per zipfile, but there is still
 *  only one manifest spreadsheet, which is attached to the first zipfile.
 *
 *  If a zipfile is over 2 Gb, start a new zipfile
 *
 *  The first zipfile is archive.zip
 *  The second zipfile is archive_2.zip
 *  The third zipfile is archive_3.zip, etc.
 *
 */
function updateZipArchive() {
    global $POSTVARS;
    global $MAX_FILES_PER_ARCHIVE;

    $archive_root = $POSTVARS['archiveRoot'];
    $filecount = countFiles($archive_root);

    $archive_idx = 1;		// counter for multiple zipfiles
    $archive_filecount = 0;	// number of files in zipfile
    $archivename = "archive";	// name of zipfile (without extension)

    // Get info for current archive file
    $latest_archive = getLatestArchive();
    if ($latest_archive != null) {
        $archive_idx = $latest_archive->archiveidx;
        $archive_filecount = $latest_archive->filecount;
        $archivename = $latest_archive->archivename;
    }

    $zipfile_size_limit_mb = 2000;
    $archivesize_mb = 0;
    if (file_exists("$archivename.zip")) {
        $archivesize_mb = intval(filesize("$archivename.zip") / (1024 * 1024));
    }
    logDEBUG("File $archivename.zip is $archivesize_mb mb");

    // if # records in archive + # new files > max files, start new archive
    if (($archive_filecount + $filecount > $MAX_FILES_PER_ARCHIVE) ||
        ($archivesize_mb > $zipfile_size_limit_mb)) {
        $archive_idx += 1;
        $archive_filecount = 0;
        $archivename = "archive_${archive_idx}";
        logINFO("Starting new archive $archivename");
    }

    // zip zipfile file-to-archive
    $rc = 0;
    $output = array();
    $zipcmd = "zip -gmr $archivename \"${archive_root}\"";
    exec($zipcmd, $output, $rc);
    if ($rc == 12) {
        return;	// nothing to do
    } else if ($rc !== 0) {
        quit("Zip utility quit with error [$rc]");
    }

    // Update db record for zipfile stats
    $b = findArchive($archive_idx);
    if ($b == null) {
        $b = R::dispense('archives');
        $b->archiveidx = $archive_idx;
        $b->filecount = 0;
        $b->archivename = $archivename;
    }
    $b->filecount = $b->filecount + $filecount;
    R::store($b);
}

/*
 *  Build Excel manifest, attach to archive.zip
 */
function buildManifest($archive_name, $column_spec) {

    // Build manifest
    // Format provided by column_spec
    // (Old default: Category, Doc Name, Link, Date, Author, Description)
    $manifest_data = array();

    $metadata_beans = getMetadata();
    foreach ($metadata_beans as $b) {
        $metadata = unserialize($b->metadata);

        $category = trim($metadata['appdoctype']);
        $doctitle = $metadata['sfilename'];
        $link = $metadata['path'];
        $docdate = $metadata['moreMetadata']['docdate'];
        $author = trim($metadata['author']);
        $description = trim($metadata['moreMetadata']['docsummary']);

        $group = trim($metadata['moreMetadata']['level2name']);
        $subgroup = trim($metadata['moreMetadata']['level3name']);
        $filename = trim($metadata['filename']);
        $pubdate = trim($metadata['pubdate']);
        $recipient = trim($metadata['moreMetadata']['recipient']);
        $sensitiveflag = $metadata['sensitiveflag'];

        $pdfpagecount = trim($b->pdfpagecount);

        $row = array();
        $row[FL_CATEGORY] = $category;
        $row[FL_DOCTITLE] = $doctitle;
        $row[FL_DOCDATE] = $docdate;
        $row[FL_AUTHOR] = $author;
        $row[FL_DESCRIPTION] = $description;

        $row[FL_LINK] = $link;

        $row[FL_GROUP] = $group;
        $row[FL_SUBGROUP] = $subgroup;
        $row[FL_FILENAME] = $filename;
        $row[FL_PUBDATE] = $pubdate;
        $row[FL_RECIPIENT] = $recipient;

        $row[FL_PDFPAGECOUNT] = $pdfpagecount;

        $row[FL_SENSITIVEFLAG] = $sensitiveflag;

        $manifest_data[] = $row;
    }

    try {
/*
        // Build CSV manifest
        $manifest_fname="${archive_name}.csv";
        manifest_build_csv($archive_name, $column_spec, $manifest_data, $manifest_fname);
*/

        // Build Excel manifest
        $manifest_fname="${archive_name}.xlsx";	// Build Excel2007 file
        global $METADATA_TITLES;
        $METADATA_TITLES[FL_DESCRIPTION] = "[Stamp] / Description";
        manifest_build_xls($archive_name, $column_spec, $manifest_data, $manifest_fname);
    } catch (Exception $e) {
        quit("Error building manifest: " . $e->getMessage());
    }

    $rc = 0;
    $output = array();
    $zipcmd = "zip -r archive \"$manifest_fname\"";
    exec($zipcmd, $output, $rc);
    if ($rc !== 0) {
        quit("Zip utility quit with error [$rc]");
    }
}

/*
 *  Count PDF pages
 *  Don't run this on files that fail PDF conversion
 */
function countPDFPages($file) {
    global $PAGECOUNTER;

    $status = 0;
    ob_start();
    $pagecount = system("$PAGECOUNTER \"$file\"", $status);
    ob_end_clean();
    if ($status != 0) {
        $pagecount = "ERROR";
    }
    return $pagecount;
}

/*
 *  Return true if filename already exists in the archive
 *
 *  Compare the lowercased name against the 'key' in the metadata table
 */
function nameExists($name) {
    $found = R::findOne('metadata', ' key = ? ',
                         array(strtolower($name)));
    return ($found != null);
}

/*
 *  Return true if any files have failed to stamp properly
 */
function errorExists() {
    $found = R::findOne('errors');
    return ($found != null);
}

/*
 *  Return list of all errors
 */
function getErrors() {
    return R::findAll('errors', 'ORDER BY idx');
}

/*
 *  Return list of all metadata
 */
function getMetadata() {
    return R::findAll('metadata', 'ORDER BY idx');
}

/*
 *  Count archived documents
 */
function countMetadata() {
    return R::count('metadata');
}

/*
 *  Return list of sensitive documents
 */
function getSensitive() {
    return R::find('metadata', 'sensitiveflag = 1 ORDER BY idx');
}

/*
 *  Count sensitive documents
 */
function countSensitive() {
    return R::count('metadata', 'sensitiveflag = 1');
}

/*
 *  Find archive by index
 */
function findArchive($archive_idx) {
    return R::findOne('archives', 'archiveidx = ?', array($archive_idx));
}

/*
 *  Return list of all archives
 */
function getArchives() {
    return R::findAll('archives', 'ORDER BY archiveidx');
}

/*
 *  Count archives
 */
function countArchives() {
    return R::count('archives');
}

/*
 *  Get latest archive index
 *  Returns null if no archive files have been created
 */
function getLatestArchive() {
    return R::findOne('archives', ' 1 ORDER BY archiveidx DESC LIMIT 1 ');
}

/*
 *  Update restart.php to point to the next file to process
 *  Also save the last known copy of the stamper config
 */
function updateRestarter($jobid, $key, $startfile) {
    global $RESTART;

    $fp = fopen($RESTART, "w");
    $phpcmds = array();
    $phpcmds[] = '$_GET["jobid"]="' . $jobid . '";';
    $phpcmds[] = '$_GET["key"]="' . $key . '";';
    $phpcmds[] = '$_GET["startfile"]="' . $startfile . '";';
    $phpcmds[] = 'chdir("' . __DIR__ . '");';
    $phpcmds[] = '// include_once "processjob.php";';
    $cmdline = implode("\n", $phpcmds);
    fwrite($fp, "<?php $cmdline ?>");

    fclose($fp);

    $nextconfig = "output/nextjob.conf";	// generated config file
    if (file_exists($nextconfig)) {
        copy($nextconfig, "nextjob.conf.bkp");
    }
}

/*
 *  Erase any old beans that will conflict with this job restart
 */
function cleanupDB($startid) {
    $metadata_beans = R::find('metadata', " idx >= :startid ",
        array(':startid' => $startid));
    foreach ($metadata_beans as $bean) {
        R::trash($bean);
    }
    $error_beans = R::find('errors', " idx >= :startid ",
        array(':startid' => $startid));
    foreach ($error_beans as $bean) {
        R::trash($bean);
    }
}

/*
 *  Prevent HTML mail readers from converting the URL text into a link
 *
 *  So far the most reliable way to deal with this is to remove the
 *  protocol (http://) and try to conceal the domain (by wrapping it
 *  in <span> tags)
 *
 *  Unfortunately the display tends to wrap on dashes, and even setting
 *  the style to nowrap does not solve the problem, e.g.
 *  <span style="white-space: nowrap;"></span>
 */
function unlinkify($url) {
    $url = preg_replace("/^http:\/\//", "", $url, 1);
    return preg_replace("/\.com/", "<span>.com</span>", $url, 1);
}

/*
 *  MAIN
 */
function main() {
    global $JOBID;
    global $JOB_DIR;
    global $POSTVARS;
    global $LOGFILE;
    global $RESTART;
    global $NEEDS_RESTART;

    global $ARCHIVE_BUFFER;		// Extract N documents before zipping
    global $MAX_FILES_PER_ARCHIVE;	// Max docs allowed per zipfile

    global $ARCHIVE_URLBASE;

    global $DATAMART;

    if (! isset($_GET['jobid'])) {
        quit("No job ID!  Can't continue");
    }
    $JOBID = $_GET['jobid'];

    // Change current dir to working dir
    $workingdir = "$JOB_DIR/$JOBID";
    $dir = opendir($workingdir);
    if (!$dir) {
        quit("Job directory for job $JOBID not found!  Can't continue");
    }
    closedir($dir);
    chdir($workingdir);

    if (! isset($_GET['startfile'])) {
        quit("No startfile index!  Can't continue");
    }
    $startfile = $_GET['startfile'];

    $requestdata = "$workingdir/requestdata.php";
    $jobdata = "$workingdir/job.data";
    $RESTART = "$workingdir/restart.php";
    $jobdbfile = "$workingdir/job.db";

    $LOGFILE = "$workingdir/log.txt";
    $NEEDS_RESTART = "$workingdir/NEEDS_RESTART";

    logINFO("Starting job");

    unlink($NEEDS_RESTART);	// Clear restart flag, if any

    $POSTVARS = unserialize(file_get_contents($requestdata));

    // Only permit this script to be called from localhost
    if (! isset($_GET['key'])) {
        quit("No key!  Can't continue");
    }
    $key = $_GET['key'];
    if ($POSTVARS['secretKey'] != $key) {
        quit("Script can only be called from localhost!  Can't continue");
    }

    if (!isset($POSTVARS['archiveRoot'])) {
        quit("No archiveRoot defined, can't continue");
    }

    if (isset($POSTVARS['maxFilesPerArchive'])) {
        $MAX_FILES_PER_ARCHIVE = $POSTVARS['maxFilesPerArchive'];
    }
    if ($MAX_FILES_PER_ARCHIVE < $ARCHIVE_BUFFER) {
        $ARCHIVE_BUFFER = $MAX_FILES_PER_ARCHIVE;
    }

    // create/open job.db
    R::setup("sqlite:$jobdbfile",'user','password');

    // cleanup any DB records from previous runs
    cleanupDB($startfile);

    // get projectid
    $projectid = $POSTVARS['projectID'];

    // initialize job (detect restarts)
    if ($startfile == 0) {
        logINFO("Expanding albums");
        // expand any albums in the job data file
        $newjobdata = "$workingdir/job.data.new";
        $origfp = fopen($jobdata, "r");
        if ($origfp == null) {
            quit("Could not open job data file $jobdata");
        }
        $newfp = fopen($newjobdata, "w");
        if ($newfp == null) {
            quit("Could not open temp job file $newjobdata");
        }

        while (($line = fgets($origfp)) !== false) {
            $attrs = explode(",", $line);
            $origpath = $attrs[0];
            $foldername = $attrs[1];
            if (urldecode($attrs[2]) == "ALBUM") {
                $containerid = trim(urldecode($attrs[3]));
                try {
                    $expanded_files = $DATAMART->datamart_expandAlbum($projectid, $containerid);
                } catch (Exception $e) {
                    quit("Error expanding albums: " . $e->getMessage());
                }
                foreach ($expanded_files as $file) {
                    $path = $origpath . urlencode($file['path']);
                    $filename = urlencode($file['filename']);
                    $docid = urlencode($file['docid']);
                    $newline = "$path,$filename,$docid\n";
                    fwrite($newfp, $newline);
                }
            } else {
                fwrite($newfp, $line);
            }
        }

        // swap old jobfile for new one
        fclose($origfp);
        fclose($newfp);
        $tempjobdata = "$workingdir/job.data.temp";
        rename($jobdata, $tempjobdata);
        rename($newjobdata, $jobdata);
        unlink($tempjobdata);
        logINFO("Done expanding albums");

        // Also, make sure to remove any stamper config files
        $nextconfig = "output/nextjob.conf";	// generated config file
        if (file_exists($nextconfig)) {
            logINFO("Removing stale stamper config file");
            unlink($nextconfig);
        }
    } else {
        // if we're not restarting from the beginning,
        // prime the stamper config with the last saved config
        $nextconfig = "output/nextjob.conf";
        if (file_exists("nextjob.conf.bkp") && file_exists($nextconfig)) {
            copy("nextjob.conf.bkp", $nextconfig);
        }
    }

    // Start stamping
    $totalfiles = $POSTVARS['TotalFiles'];

    $folderstrategy = "nested";
    if (isset($POSTVARS['FolderStrategy'])) {
        if ($POSTVARS['FolderStrategy'] == "flat") {
            $folderstrategy = "flat";
        }
    }

    $jobdatafp = fopen($jobdata, "r");
    if ($jobdatafp == null) {
        quit("Could not open job file $jobdata");
    }
    $lineno = 0;
    $jobline = false;
    while (($jobline = fgets($jobdatafp)) !== false) {
        if ($lineno >= $startfile) { break; }	// Skip files before startfile
        $lineno++;
    }
    // Download and stamp files, and zip them up
    // once every ARCHIVE_BUFFER (=200) files
    $archive_root = $POSTVARS['archiveRoot'];
    $buffer_size_limit = 1024 * 1024 * 1000; // 1 gb
    do {
        if ($jobline !== false) {
            $fileno = $lineno + 1;
            logINFO("Processing file $fileno of $totalfiles");
            processFilespec(trim($jobline, "\n"), $lineno, $folderstrategy);
            $lineno++;
            // Every 200 files, add files to zip archive
            if ($lineno % $ARCHIVE_BUFFER == 0) {
                updateZipArchive();
                updateRestarter($JOBID, $_GET['key'], $lineno);
            } else {
                // If we have > 1 Gb of data, add files to zip archive
                if (totalFileSizes($archive_root) > $buffer_size_limit) {
                    updateZipArchive();
                    updateRestarter($JOBID, $_GET['key'], $lineno);
                }
            }
        }
    } while (($jobline = fgets($jobdatafp)) !== false);

    updateZipArchive();
    updateRestarter($JOBID, $_GET['key'], $lineno);

    $archive_name = $POSTVARS['archiveName'];
    $column_spec = $POSTVARS['columnSpec'];

    // Always include page count as final column
    $column_spec = $column_spec . ",z";

    logINFO("Building archive manifest");
    buildManifest($archive_name, $column_spec);

    // Upload all archives into edge server, compute URLs
    $archivebeans = getArchives();

    foreach ($archivebeans as $b) {
        $zipfile = $b->archivename . ".zip";
        $suffix = "_" . $b->archiveidx;
        if ($suffix == "_1") { $suffix = ""; }

        $filesize_b = filesize($zipfile);
        $filesize_k = strval(intval($filesize_b / 1024));
        $archive_zipfile_name = "${archive_name}${suffix}.zip";
        $archive_zipfile_name = str_replace(" ", "_", "$archive_zipfile_name");

        logINFO("Uploading ${filesize_k}k $zipfile to file server");
        try {
            aws_putfile($archive_zipfile_name, $zipfile);
        } catch (Exception $e) {
            quit("Error uploading file to AWS: " . $e->getMessage());
        }

        $link = "$ARCHIVE_URLBASE/$archive_zipfile_name";
        $b->link = "$link";
        $b->bytes = $filesize_b;
        $b->kilobytes = $filesize_k;
        R::store($b);
        logINFO("Uploaded archive $archive_zipfile_name ${filesize_k}k, location: $link");
    }

    $filecount = countMetadata();	// Count stamps that didn't fail

    $finalmsg = "";

    $archivecount = countArchives();

    if ($archivecount == 1) {

        $finalmsg = "<p>Your archive contains $filecount files.</p><p>You may retrieve your zipfile from the following link within the next 30 days:</p>\n";

    } else {
        $finalmsg = "<p>Your archive contains $filecount files in $archivecount zipfiles.</p>\n";
        $finalmsg .= "<p>You may retrieve your zipfiles from the following links within the next 30 days:</p>\n";
    }
    $finalmsg .= "<table border='1' style='border-spacing:0;border-width:1px'>\n";
    $finalmsg .= "<tr><td>&nbsp;<b>Download&nbsp;Link</b><sup>*</sup>&nbsp;</td><td><center><b>Size</b></center></td><td><center><b>URL</b><sup>*</sup></center></td></tr>\n";
    $archivebeans = getArchives();
    foreach ($archivebeans as $b) {
        $link = $b->link;
        $size = $b->kilobytes;
        $idx  = $b->archiveidx;
        $finalmsg .= "<tr>";
        $finalmsg .= "<td><center><a href='$link'>Zipfile $idx</a></center></td>";
        $finalmsg .= "<td>&nbsp;$size&nbsp;k&nbsp;</td>";
        $finalmsg .= "<td>&nbsp;" . unlinkify($link) . "&nbsp;</td>";
        $finalmsg .= "</tr>\n";
    }
    $finalmsg .= "</tr></table>\n";
    $finalmsg .= "<p><sup>*</sup><i>The <b>Download Link</b> may not be accessible outside of the USDA intranet.</br>\n";
    $finalmsg .= "If you want to download a zipfile from outside the USDA, please copy and paste the <b>URL</b> address into your browser's navigation bar.</i></p>\n";

    $sensitivecount = countSensitive();
    if ($sensitivecount > 0) {
        $finalmsg .= "<p>Your archive includes the following sensitive files:</p>\n";
        $finalmsg .= "<table border='1' style='border-spacing:0;border-width:1px'>\n";
        $finalmsg .= "<tr><td><center><b>File name</b></center></td></tr>\n";
        $sbeans = getSensitive();
        foreach ($sbeans as $b) {
            $filename = htmlentities($b->filename);
            $finalmsg .= "<tr>";
            $finalmsg .= "<td><center>$filename</center></td>";
            $finalmsg .= "</tr>\n";
        }
        $finalmsg .= "</table>\n";
    }

    $email = $POSTVARS['userEmail'];
    sendConfirmationEmail($email, $finalmsg);

    logINFO("Stamping complete!  Processed $totalfiles files");
    logINFO("Ending job");
}

/*
 *  Kick off all processing here
 */

proc_nice(19);
main();
?>
