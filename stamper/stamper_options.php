<script language="javascript">
$(function() {
    // Initialize options
    $("#StampLocation").val("topleft");
    $("#AddMargin").val("true");	// always scale page to add whitespace
    $("#DateFormat").val("1");

    $("input:radio[name=StampFormat]").val(["page"]);	// actual format is StampFormatCode
    $("input:radio[name=FolderStrategy]").val(["nested"]);
    $("#TotalFilesFormat").val("%d");
    $("#TotalFilePagesFormat").val("%d");
    $("#FileSeqFormat").val("%d");
    $('#PageInFileFormat').val("%d");
    $("#StaticTime").val(new Date().getTime());	// use fixed current timestamp
<?php
/*
 *  The "counter" means different things in different stamp formats
 */
?>
    $("#StartNumber").val("1");	// initial value for counter
    $("#NumberWidth").val("4");	// minimum field width for counter
    $("#Prefix").val("");
    $("#Suffix").val("");
    $("#StampFontSize").val("12");

    // Automatically erase UI warnings and errors
    $('select').change(function() {
        $(this).removeClass('warning');
        $(this).parent().find("div.error").empty();
        validateStamperForm();
        updateStampPreview();
    }); 
    $('input:radio').click(function() {
        $(this).removeClass('warning');
        $(this).parent().find("div.error").empty();
        validateStamperForm();
        updateStampPreview();
    });
    $('input').keyup(function() {
        $(this).removeClass('warning');
        $(this).parent().find("div.error").empty();
        validateStamperForm();
        updateStampPreview();
    });
});

function updateStampPreview() {
    if ($(".warning").length > 0) {
        $("#Preview").parent().find('div.error').html('Please fix invalid option(s)');
        $("#Preview").val("-- FORMAT ERROR --");
    } else {
        var dateformat = getDateFormat($("#DateFormat").val());
        var now = new Date(parseInt($("#StaticTime").val(), 10));
        var datepreview = $.format.date(now, dateformat);
        var prefix = $("#Prefix").val();
        var suffix = $("#Suffix").val();
        var stampformat = $('input:radio[name=StampFormat]:checked').val();
        var fileseq = 1;
        var totalfilepages = 10;
        var pageinfile = 1;
        var pageincollection = 1;
        var startnumber = $("#StartNumber").val();
        var numberwidth = $("#NumberWidth").val();
        var stampfontsize = $("#StampFontSize").val();

        $("#Preview").css('font-size', parseInt(stampfontsize, 10));
        switch(stampformat) {
        case "page":
            $("#StartNumberInfo").text("(page number across all files)");
            if (startnumber) {
                // StartNumber is a page increment
                pageincollection = startnumber;
            }
            pageincollection = padInteger(pageincollection, numberwidth);
            $("#Preview").val(datepreview + prefix + pageincollection + suffix);
            break;
        case "file":
            $("#StartNumberInfo").text("(file number across all files)");
            if (startnumber) {
                // StartNumber is a file increment
                fileseq = startnumber;
            }
            fileseq = padInteger(fileseq, numberwidth);
            $("#Preview").val(datepreview + prefix + fileseq + suffix + " Page " + pageinfile + " of " + totalfilepages);
            break;
        case "none":
            $("#StartNumberInfo").text("(ignored)");
            $("#Preview").val(datepreview + prefix + suffix);
            break;
        default:
            $("#Preview").val("Unrecognized format: " + stampformat);
        }
    }
}

/*
 *  Convert date option to SimpleDateFormat spec
 */
function getDateFormat(optval) {
    switch (optval) {
        case "1":
            // No date
            return "";
        case "2":
            // MM/DD/YYYY
            return "MM/dd/yyyy ";
        case "3":
            // Month D, YYYY
            return "MMMM d, yyyy ";
        case "4":
            // Month YYYY
            return "MMMM yyyy ";
        case "5":
            // YYYY
            return "yyyy ";
        default:
            return "'Undefined DateFormat value " + optval + "' ";
    }
}

function padInteger(n, len) {
    var str = '' + n;
    while (str.length < len) {
        str = '0' + str;
    }
    return str;
}

function isInteger(n) {
  return !isNaN(parseInt(n)) && isFinite(n);
}

function validateStamperForm() {
    // Clear old warnings
    $('#stamperControls input').each(function(i, e)
    {
        $(e).removeClass('warning');
    });
    $('#stamperControls select').each(function(i, e)
    {
        $(e).removeClass('warning');
    });
    $('#stamperControls .error').empty();

    // Validate fields
    $('#stamperControls input.required').each(function(i, e)
    {
        if( !$(e).val() ) {
            $(e).addClass('warning');
            $(e).parent().find('div.error').html('please enter a value');
        }
    });
    $('#stamperControls select.required').each(function(i, e)
    {
        if( $(e).val() == "0" ) {
            $(e).addClass('warning');
            $(e).parent().find('div.error').html('please make a selection');
        }
    });

    var stampformat = $('input:radio[name=StampFormat]:checked').val();
    switch(stampformat) {
    case "page":
        $("#StampFormatCode").val("[date] [prefix][pageincollection][suffix]");
        // Make sure StartNumber and NumberWidth are active
        $('#StartNumber').removeAttr('disabled');
        $('#NumberWidth').removeAttr('disabled');
        break;
    case "file":
        $("#StampFormatCode").val("[date] [prefix][fileseq][suffix] Page [pageinfile] of [totalfilepages]");
        // TEST $("#StampFormatCode").val("[date] [fileseq] of [totalfiles], Page [pageinfile] of [totalfilepages]");
        // Make sure StartNumber and NumberWidth are active
        $('#StartNumber').removeAttr('disabled');
        $('#NumberWidth').removeAttr('disabled');
        break;
    case "none":
        $("#StampFormatCode").val("[date] [prefix][suffix]");
        // Make sure StartNumber and NumberWidth are cleared and inactive
        $('#StartNumber').attr('disabled', true);
        $('#NumberWidth').attr('disabled', true);
        break;
    default:
        $("#StampFormatCode").val("");
        alert("Error: StampFormat has bad value " + stampformat);
    }

    var startnumber = $.trim($('#StartNumber').val());
    var numberwidth = $("#NumberWidth").val();
    if (startnumber == "") {
        // Empty string, pass
    } else if (!isInteger(startnumber)) {
        $('#StartNumber').addClass('warning');
        $('#StartNumber').parent().find('div.error').html('Start Number must be a positive integer');
    } else if (parseInt(startnumber,10) <= 0) {
        $('#StartNumber').addClass('warning');
        $('#StartNumber').parent().find('div.error').html('Start Number must be a positive integer');
    } else {
        startnumber = parseInt(startnumber,10);
        startnumber = padInteger(startnumber, numberwidth);
        $('#StartNumber').val(startnumber);
    }

    // Assign number format string for page counter
    $('#PageInCollectionFormat').val("%d");
    $('#FileSeqFormat').val("%d");
    $('#StartPageInCollection').val("1");
    $('#StartFileSeq').val("1");
    var numberformat = "%0" + numberwidth + "d";
    switch(stampformat) {
    case "page":
        $('#PageInCollectionFormat').val(numberformat);
        $('#StartPageInCollection').val(startnumber);
        break;
    case "file":
        $('#FileSeqFormat').val(numberformat);
        $('#StartFileSeq').val(startnumber);
        break;
    }

    MAX_PREFIX_LENGTH = 10;
    var prefix = $('#Prefix').val();
    if (prefix.length > MAX_PREFIX_LENGTH) {
        $('#Prefix').addClass('warning');
        $('#Prefix').parent().find('div.error').html('Prefix too long, please reduce to ' + MAX_PREFIX_LENGTH + ' characters');
    }

    MAX_SUFFIX_LENGTH = 10;
    var suffix = $('#Suffix').val();
    if (suffix.length > MAX_SUFFIX_LENGTH) {
        $('#Suffix').addClass('warning');
        $('#Suffix').parent().find('div.error').html('Suffix too long, please reduce to ' + MAX_SUFFIX_LENGTH + ' characters');
    }

    var dateformat = getDateFormat($("#DateFormat").val());
    $('#DateFormatCode').val(dateformat);

    $("#StaticTime").val(new Date().getTime());

    fields = $('#StampLocation')
          .add('#StampFormat')
          .add('#DateFormat')
          .add('#StartNumber')
          .add('#NumberWidth')
          .add('#Prefix')
          .add('#Suffix')
          .add('#StampFontSize');
<?php
// Note jQuery returns wrapped set in the order
// that elements appear on the web page
?>
    return fields;
}
</script>

<form id="stamperForm" method="POST" action="stamper/buildjob.php">

<div class="sectionTitle nofloat">Zipfile Options</div>

<div class="indent">
<div class="optionSection">

<div>
  <span class="instruction">* indicates a required field</span>
</div>

<div>
  <span class="star">*</span>
  <span class="label required">Folder Structure</span>
  <div class="radioDiv">

    <div>
      <input type="radio" name="FolderStrategy" value="nested"/>
      <div class="label">
        <span class="name">Preserve folder structure</span>
      </div>
    </div>

    <div>
      <input type="radio" name="FolderStrategy" value="flat"/>
      <div class="label">
        <span class="name">Store all files in one folder</span>
      </div>
    </div>

  </div>
</div>

</div>
</div>

<div class="sectionTitle nofloat">Bates Stamp Options</div>
<div class="indent">
<div class="optionSection">

<input id="projectID" name="projectID" style="display:none"/>
<input id="projectName" name="projectName" style="display:none"/>
<input id="userID" name="userID" style="display:none"/>
<input id="userEmail" name="userEmail" style="display:none"/>
<input id="archiveName" name="archiveName" style="display:none"/>
<input id="archiveRoot" name="archiveRoot" style="display:none"/>
<input id="columnSpec" name="columnSpec" style="display:none"/>
<input id="selectedFiles" name="selectedFiles" style="display:none"/>

<input id="StartFileSeq" name="StartFileSeq" style="display:none"/>
<input id="StartPageInCollection" name="StartPageInCollection" style="display:none"/>
<input id="TotalFiles" name="TotalFiles" style="display:none"/>

<input id="AddMargin" name="AddMargin" style="display:none"/>
<input id="StampFormatCode" name="StampFormatCode" style="display:none"/>
<input id="TotalFilesFormat" name="TotalFilesFormat" style="display:none"/>
<input id="TotalFilePagesFormat" name="TotalFilePagesFormat" style="display:none"/>
<input id="FileSeqFormat" name="FileSeqFormat" style="display:none"/>
<input id="PageInFileFormat" name="PageInFileFormat" style="display:none"/>
<input id="PageInCollectionFormat" name="PageInCollectionFormat" style="display:none"/>
<input id="DateFormatCode" name="DateFormatCode" style="display:none"/>
<input id="StaticTime" name="StaticTime" style="display:none"/>

<div>
  <span class="star">*</span>
  <span class="label required">Stamp Location</span>
  <div>
    <select id="StampLocation" name="StampLocation" class="choices required">
        <option value="topleft" selected="selected">Upper Left</option>
        <option value="topright">Upper Right</option>
        <option value="bottomleft">Bottom Left</option>
        <option value="bottomright">Bottom Right</option>
    </select>
    <div class="error"></div>
  </div>
</div>


<div>
  <span class="star hidden">*</span>
  <span class="label">Date Format</span>
  <div>
    <select id="DateFormat" name="DateFormat" class="choices required">
        <option value="1" selected="selected">No date</option>
        <option value="2">MM/DD/YYYY</option>
        <option value="3">Month D, YYYY</option>
        <option value="4">Month YYYY</option>
        <option value="5">YYYY</option>
    </select>
    <div class="error"></div>
  </div>
</div>

<div>
  <span class="star">*</span>
  <span class="label required">Stamp Format</span>
  <div class="radioDiv">

    <div>
      <input type="radio" name="StampFormat" value="page"/>
      <div class="label">
        <span class="name">Unique Page Stamp</span><br/>
        <span class="details">[date] [prefix][page number][suffix]</span>
      </div>
    </div>

    <div>
      <input type="radio" name="StampFormat" value="file"/>
      <div class="label">
        <span class="name">Document Page Stamp</span><br/>
        <span class="details">[date] [prefix][file number][suffix] Page [page] of [total pages in file]</span>
      </div>
    </div>

    <div>
      <input type="radio" name="StampFormat" value="none"/>
      <div class="label">
        <span class="name">Do Not Number Pages</span><br/>
        <span class="details">[date] [prefix][suffix]</span>
      </div>
    </div>

    <div class="error"></div>
  </div>
</div>

<div>
  <span class="star hidden">*</span>
  <span class="label">Start Number</span>
  <div>
    <input id="StartNumber" name="StartNumber" type="text" value=""/>
    <select id="NumberWidth" name="NumberWidth" class="choices required">
        <option value="1">1 digit</option>
        <option value="2">2 digits</option>
        <option value="3">3 digits</option>
        <option value="4" selected="selected">4 digits</option>
        <option value="5">5 digits</option>
        <option value="6">6 digits</option>
        <option value="7">7 digits</option>
        <option value="8">8 digits</option>
        <option value="9">9 digits</option>
        <option value="10">10 digits</option>
    </select>
    <span id="StartNumberInfo" class="pinfo"></span>
    <div class="error"></div>
  </div>
</div>

<div>
  <span class="star hidden">*</span>
  <span class="label">Prefix</span>
  <div>
    <input id="Prefix" name="Prefix" type="text" value=""/>
    <div class="error"></div>
  </div>
</div>

<div>
  <span class="star hidden">*</span>
  <span class="label">Suffix</span>
  <div>
    <input id="Suffix" name="Suffix" type="text" value=""/>
    <div class="error"></div>
  </div>
</div>

<div>
  <span class="star">*</span>
  <span class="label">Font Size</span>
  <div>
    <select id="StampFontSize" name="StampFontSize" class="required"/>
        <option value="10">10 pt</option>
        <option value="11">11 pt</option>
        <option value="12" selected="selected">12 pt</option>
        <option value="13">13 pt</option>
        <option value="14">14 pt</option>
    </select>
    <div class="error"></div>
  </div>
</div>

<div>
  <span class="star hidden">*</span>
  <span class="label">Stamp Preview</span>
  <div>
    <input id="Preview" name="Preview" type="text" class="plabel"/>
    <div class="error"></div>
  </div>
</div>

<br/>
<br/>

</div>
</div>

</form>
