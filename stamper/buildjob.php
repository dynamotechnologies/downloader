<?php

require_once "../config.php";

/*
 *  Build the job description files (requestdata.php, job.data),
 *  the job control files (restart.php),
 *  and begin processing (call restart.php)
 *
 *  - Store requestdata.php (copy of original request parameters)
 *  - Store job.data (list of doc IDs and names)
 *  - Create restart.php (script to start or restart job processing)
 */

/*
 *  Store original request parameters
 *  (contents of $_POST)
 */
function storeRequest($path_to_file) {
    $fp = fopen($path_to_file, "w");
    $postvars = serialize($_POST);
    fwrite($fp, $postvars);
    fclose($fp);
}

/*
 *  Store job data
 *  (list of files to process, may contain unexpanded albums)
 */
function storeJobData($path_to_file) {
    $fp = fopen($path_to_file, "w");
    $jobspec = $_POST['selectedFiles'];
    // split $jobspec on vbars
    $filelist = explode("|", $jobspec);
    // write fields to file, 1 per line
    foreach ($filelist as $line) {
        fwrite($fp, "$line\n");
    }
    fclose($fp);
}

/*
 *	MAIN
 */
try {
    if (! $JOB_DIR) {
        throw new Exception("JOB_DIR not provided, cannot continue!");
    }
    if (! $PHP_CLI) {
        throw new Exception("PHP_CLI not provided, cannot continue!");
    }
    // Get job ID
    $mypath = $_SERVER['SERVER_NAME'] . dirname($_SERVER['SCRIPT_NAME']);
    $jsonstr = file_get_contents("https://$mypath/../svc_takeanumber.php");
    $jobdata = json_decode($jsonstr, True);
    if (! isset($jobdata['jobid'])) {
        $msg = "Couldn't get new job ID, cannot continue!  Reason: " .
            $jobdata['errormsg'];
        throw new Exception($msg);
    }
    $jobid = strval($jobdata['jobid']);
    $projectid = $_POST['projectID'];

    // Create working directories
    $workingdir = "$JOB_DIR/$jobid";
    if (! is_dir("$workingdir")) {
        throw new Exception("Job dir $jobid not created, cannot continue!");
    }
    touch("$workingdir/STAMP_JOB");
    $jobdata = "$workingdir/job.data";
    $requestdata = "$workingdir/requestdata.php";
    $restart = "$workingdir/restart.php";

    // Generate secret key to prevent external users from running job
    $key = rand();
    $_POST['secretKey'] = $key;

    // Save job.data
    storeJobData($jobdata);

    // Save original POST request data (requestdata.php)
    storeRequest($requestdata);

    $startnumber = $_POST['StartNumber'];	// starting number (string)

    // Create the restart script
    $fp = fopen($restart, "w");
    $processjoburl = __DIR__ . "/processjob.php";

    $phpcmds = array();
    $phpcmds[] = '$_GET["jobid"]="' . $jobid . '";';
    $phpcmds[] = '$_GET["key"]="' . $key . '";';
    $phpcmds[] = '$_GET["startfile"]="0";';
    $phpcmds[] = 'chdir("' . __DIR__ . '");';
    $phpcmds[] = 'include_once "' . $processjoburl . '";';
    $cmdline = implode("\n", $phpcmds);
    fwrite($fp, "<?php $cmdline ?>");
    fclose($fp);
    chmod($restart, 0755);

    $email = "";
    if (isset($_POST['userEmail'])) {
        $email = $_POST['userEmail'];
    } else {
        throw new Exception("User record has no email address!  Can't continue");
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Bates Stamp Job Submitted for Project <?php echo $projectid; ?></title>
<link type="text/css" href="../jquery/css/custom-theme/jquery-ui-1.8.13.custom.css" rel="Stylesheet" />
<link rel="stylesheet" type="text/css" href="../efile.css">
</head>
<body>
<div id="content">
<p>Your job has been submitted.</p>
<p>A confirmation email will be sent to <?php echo $email; ?> when your job is complete.</p>
<p>You may now close this window.</p>
</div>
</body>
</html>
<?php
    // Submit job
    system("$PHP_CLI $restart >/dev/null 2>&1 &");

} catch (Exception $e) {

    echo "<PRE>";
    echo "Exception: " . $e->getMessage();
    // print_r($_POST);	// DEBUG
    echo "</PRE>";

}

/*
echo "<PRE>";
print_r($_SERVER);
print_r($_GET);
print_r($_POST);
print_r($_PUT);
echo "</PRE>";
*/
?>
