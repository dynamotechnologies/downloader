<?php
include("web_config.php");
/* Make sure Downloader page was invoked with required parameters */
$projectid = htmlentities($_GET["projectid"]);
if (!$projectid) {
  echo "<H1>No project ID given, can't continue</H1>";
  exit(0);
}

$author = htmlentities($_GET["author"]);
if (!$author) {
  echo "<H1>No author name given, can't continue</H1>";
  exit(0);
}

if(isset($_GET["viewid"]) && $_GET["viewid"] != null && $_GET["viewid"] != ""){
$viewid = htmlentities($_GET["viewid"]);
} else{
  $viewid = null;
}




$columnspec = "a,b,c,d,e,f,g,h,i,j";
//$columnspec = "a,d,f,h,j";
//if (isset($_GET['columnspec'])) {
//    $columnspec = htmlentities($_GET["columnspec"]);
//}
//if (strlen(trim($columnspec)) == 0) {
//    $columnspec = "a,d,f,h,j";
//}
/* Validate columnspec */
$columnspec = trim($columnspec);
// Codes currently range from a-j
if (!preg_match("/^([a-j],)*[a-j]$/", $columnspec)) {
  echo "<H1>Bad format for columnspec, can't continue</H1>";
  exit(0);
}

/* Specify origonly=1 to only download original documents */
$origonly = "";
if (isset($_GET["origonly"])) {
  $origonly = htmlentities($_GET["origonly"]);
}

/* Create archive title */
$today = date('Y-m-d');
$project_title = "$projectid Document Download $today";

if (!file_exists('config.php')) {
  echo "<H1>System error, check config file</H1>";
  exit(0);
}
require_once('config.php');
require_once('datamart_utils.php');
require_once('dmd_utils.php');

$DATAMART = new datamart_utils();

/* Stop IE8 from modifying the page for XSS */
header('X-XSS-Protection: 0');

/* Make sure required definitions were set in the config file */
if (!isset($STELLENT_IPADDR)) {
  echo "<H1>System error, check configs</H1>";
  exit(0);
}

if (!isset($DATAMART_PREFIX)) {
  echo "<H1>System error, check configs</H1>";
  exit(0);
}

if (!isset($DATAMART_AUTH)) {
  echo "<H1>System error, check configs</H1>";
  exit(0);
}

/* Make sure systems are available */
if ($DATAMART->datamart_status() != 200) {
  echo "<H1>Error, system not available</H1>";
  echo "<div>Please try again later</div>";
  echo "<div>{$DATAMART->datamart_status}</div>";
  exit(0);
}

if (dmd_status() == False) {
  echo "<H1>Error, document repository server not responding</H1>";
  echo "<div>Please try again later</div>";
  exit(0);
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Project <?php echo $project_title; ?></title>

<link type="text/css" href="jquery/css/custom-theme/jquery-ui-1.8.13.custom.css" rel="Stylesheet" />
<link rel='stylesheet' type='text/css' href='dynatree/src/skin/ui.dynatree.css'>
<link rel="stylesheet" type="text/css" href="downloader.css">
<?php
/*
 * After combining, replace the above with
<link rel="stylesheet" type="text/css" href="combined-min.css">
 */
?>

<script type="text/javascript">

  var _gaq = _gaq || [];

<?php if (isset($GA_TRACKER_ID)) { ?>

  _gaq.push(['_setAccount', '<?php echo $GA_TRACKER_ID; ?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

<?php } ?>

</script>

</head>
<body>
  <div id="titleBanner">
    <img id="clipboardImg" src="assets/clipboard.gif"/>
    <div id="titleStripe">
      <div id="title">Project <?php echo $project_title; ?></div>
    </div>
  </div>
  <br/>
  <div id="content">
    <div id="selectAllBtn" class="btn select-button selectall">Select All</div>
    <div id="expandAllBtn" class="btn expand-button expandall">Expand All</div>
    <div id="submitBtn" class="btn submit-button">Download</div>
    <div id="submitIndexBtn" class="btn submit-button">Download Index</div>
    <div id="stamperBtn" class="btn stamper-button">Bates Stamp</div>
    <br/>
    <br/>
    <?php
    /* Load user data */
    try {
      $USERDATA = $DATAMART->datamart_getUserData($author);
    } catch (Exception $e) {
      echo "<H1>Error loading data for user ${author}</H1>";
      echo "<H2>" . $e->getMessage() . "</H2>";
      exit(0);
    }

    /* Load project data */
    try {
      $PROJECTDATA = $DATAMART->datamart_getProjectData($projectid);
    } catch (Exception $e) {
      echo "<H1>Error loading data for project ${projectid}</H1>";
      echo "<H2>" . $e->getMessage() . "</H2>";
      exit(0);
    }

    /* Make sure user is allowed to access the given project */
    $adminunitid = $PROJECTDATA['unitcode'];
    $authorized_roles = array(1, 3, 4, 5, 6);
    $authorized = False;
    foreach ($USERDATA['roles'] as $roleline) {
      $roledata = explode("|", $roleline);
      $unit = $roledata[0];
      $role = $roledata[1];
      $unit = preg_replace("/(00)*$/", "", $unit);
      if (preg_match("/^$unit/", $adminunitid)) {
        if (in_array($role, $authorized_roles)) {
          $authorized = True;
          break;
        }
      }
    }
    if ($authorized == False) {
      echo "<H1>User $author not authorized for project ${projectid}</H1>";
      echo "<H2>" . $e->getMessage() . "</H2>";
      exit(0);
    }

    /* Load project containers in Dynatree format */
    $DATAMART->datamart_setModeDownloader($adminunitid, $USERDATA);
    try {
      // FIXME - no firm requirement on which downloader folders should be
      // classified as all-or-nothing containers
      $dynatree = $DATAMART->datamart_getContainersAsDynatree1($projectid,"","/Original Letters and Submissions/i",$viewid);
    } catch (Exception $e) {
      echo "<H1>Error loading containers for project ${projectid}</H1>";
      echo "<H2>" . $e->getMessage() . "</H2>";
      exit(0);
    }
    ?>
    <br/>
    <div id="totalSelected"></div>
    <div id="fileSelector"></div>
  </div>

  <div class="optionSection">
    <div>
      <span class="label required">Folder Structure</span>
      <div class="radioDiv">

        <div>
          <input type="radio" name="FolderStrategy" value="nested"/>
          <div class="label">
            <span class="name">Preserve folder structure</span>
          </div>
        </div>
        <div>
          <input type="radio" name="FolderStrategy" value="flat"/>
          <div class="label">
            <span class="name">Store all files in one folder</span>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Include the required JavaScript libraries: -->
  <script src='jquery/js/jquery-1.5.1.min.js' type="text/javascript"></script>
  <script src='jquery/js/jquery-ui-1.8.13.custom.min.js' type="text/javascript"></script>
  <script src='dynatree/jquery/jquery.cookie.js' type="text/javascript"></script>
  <script src='dynatree/src/jquery.dynatree.js' type="text/javascript"></script>

  <script type="text/javascript">

    var STATUS_WAITING = "WAITING";
    var STATUS_WORKING = "WORKING";
    var STATUS_SUCCESS = "SUCCESS";
    var STATUS_ERROR = "ERROR";

    var selected_filecnt = 0;
    var selected_filesize = 0;
    var project_filecnt = 0;

    $(function () {

      // Discourage caching
      $.ajaxSetup({ cache: false });

      // Init button UIs
      $(".btn").button();

      <?php
        if ($authorized) {
      ?>
        $('#stamperBtn').button("enable");
      <?php } else { ?>
        $('#stamperBtn').button("disable");
      <?php
      }
      ?>

      // Set default folder strategy
      $("input:radio[name=FolderStrategy]").val(["nested"]);
      $("#FolderStrategy").val("nested");
      $("#FolderStrategyIndex").val("nested");

      // Get dynatree data
      json_data = $.parseJSON('<?php echo $dynatree; ?>');
      project_filecnt = '<?php echo $DATAMART->G_DOC_COUNT; ?>';
      projectid = '<?php echo $projectid; ?>';
      project_title = '<?php echo $project_title; ?>';

      $("input:radio[name=FolderStrategy]").click(function() {
        $('#FolderStrategy').val($('input:radio[name=FolderStrategy]:checked').val());
        $('#FolderStrategyIndex').val($('input:radio[name=FolderStrategy]:checked').val());
      });

      // Define download button action (create archive)
      $("#submitBtn").click(
        function() {
          var checkedlist = [];

          // Get list of doc IDs in "checkedlist"
          selectedlist = $("#fileSelector").dynatree("getSelectedNodes");
          $.each(selectedlist,
            function() {
              node = this;
              if (node.data.isFolder == false) {
                checkedlist.push(node.data.key);
              }
            }
          );

          // Show confirmation dialog,
          // call publishFiles() if user approves
          var msg = "You selected " + selected_filecnt + " of " +
                    project_filecnt + " files" + "<br/>";
          msg += "Do you want to download these items?";
          $("<div><br/></div>")
          .find("br").after(msg).end()
          .css('text-align', 'center')
          .dialog({
            resizable: false,
            modal: true,
            buttons: {
              "Download": function() {
                $( this ).dialog( "destroy" );
                disableUI();
                publishFiles();
              },
              "Cancel": function() {
                $( this ).dialog( "destroy" );
              }
            }
          })
          .dialog('widget')
          .addClass('modalDialog');       // hide titlebar via css

          // adjust button placement
          $('.modalDialog .ui-dialog-buttonset').position({
            my: 'center bottom',
            at: 'center bottom',
            of: $('.modalDialog')
          });
        }
      );

      $("#submitIndexBtn").click(
        function() {
          var checkedlist = [];

          // Get list of doc IDs in "checkedlist"
          selectedlist = $("#fileSelector").dynatree("getSelectedNodes");
          $.each(selectedlist,
            function() {
              node = this;
              if (node.data.isFolder == false) {
                checkedlist.push(node.data.key);
              }
            }
          );

          // Show confirmation dialog,
          // call publishManifestFile() if user approves
          var msg = "You selected " + selected_filecnt + " of " +
                    project_filecnt + " files" + "<br/>";
          msg += "Do you want to download the manifest file?";
          $("<div><br/></div>")
          .find("br").after(msg).end()
          .css('text-align', 'center')
          .dialog({
            resizable: false,
            modal: true,
            buttons: {
              "Download": function() {
                $( this ).dialog( "destroy" );
                disableUI();
                publishManifestFile();
              },
              "Cancel": function() {
                $( this ).dialog( "destroy" );
              }
            }
          })
          .dialog('widget')
          .addClass('modalDialog');       // hide titlebar via css

          // adjust button placement
          $('.modalDialog .ui-dialog-buttonset').position({
            my: 'center bottom',
            at: 'center bottom',
            of: $('.modalDialog')
          });
        }
      );

      $("#stamperBtn").click(
        function() {
          <?php
          $stamperurl = "stamper.php?projectid=${projectid}&author=${author}&columnspec=${columnspec}";
          ?>
          var url="<?php echo $stamperurl; ?>";
          window.location.replace(url);
        }
      );

      // Define other button actions
      // Select/deselect all documents
      $("#selectAllBtn").click(
        function() {
          if ($(this).hasClass("selectall")) {
            // Select all nodes
            $("#fileSelector").dynatree('getRoot').visit(function(node) {
              node.select(true);
            });
            // Change button text
            $(this).button("option", "label", "Deselect All");
          } else {
            // Deselect all nodes
            $("#fileSelector").dynatree('getRoot').visit(function(node) {
              node.select(false);
            });
            // Change button text
            $(this).button("option", "label", "Select All");
          }
          $(this).toggleClass("selectall");
          return false;
        }
      );



      // Expand all folders
      $("#expandAllBtn").click(
        function() {
          if ($(this).hasClass("expandall")) {
            // Expand all nodes
            $("#fileSelector").dynatree("getRoot").visit(function(node){
              node.expand(true);
            });
            // Change button text
            $(this).button("option", "label", "Collapse All");
          } else {
            // Collapse all nodes
            $("#fileSelector").dynatree("getRoot").visit(function(node){
              node.expand(false);
            });
            // Change button text
            $(this).button("option", "label", "Expand All");
          }
          $(this).toggleClass("expandall");
          return false;
        }
      );



      // Display running total of selected files and filesize (from globals)
      // See countSelectedFiles()
      function displayTotal() {
        $("#totalSelected").html("Selected " + selected_filecnt + " of " + project_filecnt + " files, estimated file size " + selected_filesize + "kb");
      }



      // Disable all buttons and tree controls on main page
      function disableUI() {
        $(".btn").button("disable");
        $("#fileSelector").dynatree("disable");
      }



      // Given a Dynatree node (representing a document),
      // create a "file spec" string in the list of documents to be archived
      // A file spec consists of the comma-separated fields:
      //     1) path
      //     2) filename
      //     3) docid
      // All fields are URI-encoded
      //
      // Note that directory names are URI-encoded in the path, then separated
      // with slash separators, and then the *entire* path is URI-encoded again
      function addPubFile(publist, node) {
        var path="";
        node.visitParents(function(node) {
          path = encodeURIComponent(node.data.filename) + "/" + path;
        }, false);	// don't include self
        var filename = node.data.filename;
        var docid = node.data.key;
        publist.push(encodeURIComponent(path) +','+ encodeURIComponent(filename) +','+ encodeURIComponent(docid));
      }

      // Given a Dynatree node (representing a folder),
      // create a "file spec" string in the list of documents to be archived
      // A file spec consists of the comma-separated fields:
      //     1) path
      //     2) album name (the category name)
      //     3) the reserved word ALBUM (instead of a docid)
      //     4) the PALS unique container ID
      // All fields are URI-encoded
      function addPubAlbum(publist, node) {
        var path="";
        node.visitParents(function(node) {
          path = encodeURIComponent(node.data.filename) + "/" + path;
        }, false);
        var filename = node.data.filename;
        var docid = "ALBUM";
        var contid = node.data.contid;
        var phaseid = node.data.phaseid;
        if(phaseid === undefined || phaseid === null){
          publist.push(encodeURIComponent(path) +','+ encodeURIComponent(filename) +','+ encodeURIComponent(docid) + ',' + encodeURIComponent(contid));
        }else{
          publist.push(encodeURIComponent(path) +','+ encodeURIComponent(filename) +','+ encodeURIComponent(docid) + ',' + encodeURIComponent(contid)+','+ encodeURIComponent(phaseid));
        }
      }

      // Recompute count/size of selected files
      // See displayTotal()
      function countSelectedFiles() {
        selected_filecnt = 0;
        selected_filesize = 0;
        selectedlist = $("#fileSelector").dynatree("getSelectedNodes");
        $.each(selectedlist,
          function() {
            var node = this;
            if (node.data.isFolder == false) {
              if (node.data.isAlbum == true) {
                selected_filecnt += node.data.filecnt;
                selected_filesize += node.data.size;
              } else {
                selected_filecnt += 1;
                selected_filesize += node.data.size;
              }
            }
          }
        );
      }



      // Publish the selected files to an archive
      // (for consistency, the UI refers to this as the "download")
      //
      // Collect list of selected files
      // Reserve a job ID for the archive job (call svc_takeanumber.php)
      // Create the job status window
      // Start the archive job (call svc_buildarchive.php)
      // Start a timer to periodically update the job status window
      function publishFiles() {

        $('#projectID').val(projectid);
        $('#projectName').val(<?php echo json_encode($PROJECTDATA['name']); ?>);
        $('#userID').val('<?php echo $author; ?>');
        $('#userEmail').val('<?php echo $USERDATA['email']; ?>');
        $('#columnSpec').val(<?php echo json_encode($columnspec); ?>);

        // Get list of documents to publish in "publist"
        publist = new Array();
        selectedlist = $("#fileSelector").dynatree("getSelectedNodes");
        $.each(selectedlist,
          function() {
            var node = this;
            if (node.data.isAlbum == true) {
              addPubAlbum(publist, node);
            } else if (node.data.isFolder == false) {
              addPubFile(publist, node);
            }
          }
        );

        // Assemble list of documents into a job request in "downloadspec"
        <?php
        /*
        * downloadspec: vbar-separated fields
        * Field 1: requested archive file title
        * Field 2: custom metadata column spec
        * Field 3: project ID
        * Remaining fields describe files to be submitted
        * (see addPubFile and addPubAlbum())
        * All fields are URI-encoded
        *
        * downloadspec is also stored in form field $("#downloadSpec") for
        * batch-style job processing
        */
        ?>
        downloadspec = publist.join("|");
        <?php $archive_name = json_encode("$project_title $author"); ?>
        downloadspec = encodeURIComponent(<?php echo $archive_name; ?>)
                     + "|" + encodeURIComponent("<?php echo $columnspec; ?>")
                     + "|" + projectid
                     + "|" + downloadspec;

        $('#archiveName').val(<?php echo $archive_name; ?>);
        $('#archiveRoot').val($("#fileSelector").dynatree('getRoot').data.filename);
        <?php
        /*
        *  If there are too many documents to process interactively,
        *  change to batch mode.  The batch target will pick a job ID.
        *
        *  publishFiles() is the last action performed by the submit button,
        *  so returning from this function will bypass the process monitor code
        */
        if ($MAX_FILES_PER_ARCHIVE < $INTERACTIVE_FILE_LIMIT) {
          $INTERACTIVE_FILE_LIMIT = $MAX_FILES_PER_ARCHIVE;
        }
        ?>
        if (selected_filecnt > <?php echo $INTERACTIVE_FILE_LIMIT; ?>) {
          $('#downloadSpec').val(downloadspec);
          $('#totalFiles').val(selected_filecnt);
          $('#origonly_opt').val('<?php echo "$origonly"; ?>');
          $('#downloaderForm').submit();
          return false;	<?php // exit here ?>
        }

        var jobid = undefined;
        var statusurl = "";
        var cancelurl = "";
        var jobstatus = STATUS_WAITING;
        var message = "Waiting to start";
        var updatetimer = undefined;

        // Get a job ID
        // (also returns URLs for job status and job cancellation
        //  and creates working directory on server)
        $.ajax({
          type: "POST",
          url: "svc_takeanumber.php",
          async: false,
          data: "",
          success: function(msg){
            var response;
            try {
              response = $.parseJSON(msg);
              if ("jobid" in response) {
                jobid = response['jobid'];
                statusurl = response['statusurl'];
                cancelurl = response['cancelurl'];
                jobstatus = STATUS_WORKING;
                message = "Starting job";
              } else {
                jobstatus = STATUS_ERROR;
                message = response['errormsg'];
              }
            } catch (err) {
              jobstatus = STATUS_ERROR;
              message = "Bad response getting job ID: " + err
            }
          }
        });

        // Create status dialog window
        $("<div><div id='statustitle'>Building archive</div><p id='procstatus'></p></div>")
        .dialog({
          resizable: false,
          modal: true,
          buttons: {
            "Cancel": function() {
              $( this ).dialog( "destroy" );
              // Clear update timer, if available
              if (updatetimer != undefined) {
                clearInterval(updatetimer);
                updatetimer = undefined;
              }
              // Call cancel URL, if available
              if (cancelurl != "") {
                $.post(cancelurl);
              }
            }
          }
        })
        .dialog('widget')
        .addClass('modalDialog');       // hide titlebar via css

        // center the close button
        $('.modalDialog .ui-button').position({
          my: 'bottom center',
          at: 'bottom center',
          of: $('.modalDialog .ui-dialog-buttonpane')
        });

        // Function to update job status window
        function setStatus(htmlmsg) {
          $('#procstatus').html(htmlmsg);
        }

        // Initialize status message
        setStatus(message);
        if (jobstatus == STATUS_ERROR) { return; }

        origonly_opt = "<?php if ($origonly === "1") { echo "&origonly=1"; } ?>";
        folderstrategy_opt = '&folderstrategy=' + $('#FolderStrategy').val();

        // Start async job to build download package
        $.ajax({
          type: "POST",
          url: "svc_buildarchive.php?jobid=" + jobid + origonly_opt + folderstrategy_opt,
          data: downloadspec,
          success: function(msg){
            // Track event, log error messages
            // ['_trackEvent', 'category', 'action', 'opt_label', opt_val (int)]
            if (msg.match("^Archive complete!")) {
              _gaq.push(['_trackEvent', 'build_archive', 'success', 'success', 0]);
            } else {
              _gaq.push(['_trackEvent', 'build_archive', 'error', msg, 0]);
            }
            // Clean up after archive job completes,
            // either successfully or unsuccessfully
            if (updatetimer != undefined) {
              clearInterval(updatetimer);
              updatetimer = undefined;
            }
            setStatus(msg);
            // Change button text from "Cancel" to "Close"
            $('.modalDialog .ui-button').button('option', 'label', 'Close');
            cancelurl = ""; // Stop the cancel button from calling a service
          }
        });

      // Start periodic function to update status window every second
        updatetimer = setInterval(function() {
          $.get(statusurl.replace("localhost", "<?php echo $DNS;?>"), "", function(msg) { setStatus(msg); }, 'html');
        }, 8000);
      } // End of function publishFiles()


      // Publish the selected files to an manifest
      // (for consistency, the UI refers to this as the "download Index")
      //
      // Collect list of selected files
      // Reserve a job ID for the archive job (call svc_takeanumber.php)
      // Create the job status window
      // Start the manifest job (call svc_buildmanifest.php)
      // Start a timer to periodically update the job status window
      function publishManifestFile() {

        $('#projectIDIndex').val(projectid);
        $('#projectNameIndex').val(<?php echo json_encode($PROJECTDATA['name']); ?>);
        $('#userIDIndex').val('<?php echo $author; ?>');
        $('#userEmailIndex').val('<?php echo $USERDATA['email']; ?>');
        $('#columnSpecIndex').val(<?php echo json_encode($columnspec); ?>);

        // Get list of documents to publish in "publist"
        publist = new Array();
        selectedlist = $("#fileSelector").dynatree("getSelectedNodes");
        $.each(selectedlist,
          function() {
            var node = this;
            if (node.data.isAlbum == true) {
              addPubAlbum(publist, node);
            } else if (node.data.isFolder == false) {
              addPubFile(publist, node);
            }
          }
        );

        // Assemble list of documents into a job request in "downloadspec"
        <?php
        /*
        * downloadspec: vbar-separated fields
        * Field 1: requested archive file title
        * Field 2: custom metadata column spec
        * Field 3: project ID
        * Remaining fields describe files to be submitted
        * (see addPubFile and addPubAlbum())
        * All fields are URI-encoded
        *
        * downloadspec is also stored in form field $("#downloadSpec") for
        * batch-style job processing
        */
        ?>
        downloadspec = publist.join("|");
        <?php $archive_name = json_encode("$project_title $author"); ?>
        downloadspec = encodeURIComponent(<?php echo $archive_name; ?>)
                     + "|" + encodeURIComponent("<?php echo $columnspec; ?>")
                     + "|" + projectid
                     + "|" + downloadspec;
        $('#archiveNameIndex').val(<?php echo $archive_name; ?>);
        $('#archiveRootIndex').val($("#fileSelector").dynatree('getRoot').data.filename);
        <?php
        /*
        *  If there are too many documents to process interactively,
        *  change to batch mode.  The batch target will pick a job ID.
        *
        *  publishManifestFile() is the last action performed by the submit button,
        *  so returning from this function will bypass the process monitor code
        */
        if ($MAX_FILES_PER_ARCHIVE < $INTERACTIVE_FILE_LIMIT) {
          $INTERACTIVE_FILE_LIMIT = $MAX_FILES_PER_ARCHIVE;
        }
        ?>
        //// This check for max files is not needed for Manifest file building
        if (selected_filecnt > <?php echo $INTERACTIVE_FILE_LIMIT; ?>) {
          $('#downloadSpecIndex').val(downloadspec);
          $('#totalFilesIndex').val(selected_filecnt);
          $('#origonly_optIndex').val('<?php echo "$origonly"; ?>');
          $('#downloaderIndexForm').submit();
          return false; <?php // exit here ?>
        }

        var jobid = undefined;
        var statusurl = "";
        var cancelurl = "";
        var jobstatus = STATUS_WAITING;
        var message = "Waiting to start";
        var updatetimer = undefined;

        // Get a job ID
        // (also returns URLs for job status and job cancellation
        //  and creates working directory on server)
        $.ajax({
          type: "POST",
          url: "svc_takeanumber.php",
          async: false,
          data: "",
          success: function(msg){
            var response;
            try {
              response = $.parseJSON(msg);
              if ("jobid" in response) {
                jobid = response['jobid'];
                statusurl = response['statusurl'];
                cancelurl = response['cancelurl'];
                jobstatus = STATUS_WORKING;
                message = "Starting job";
              } else {
                jobstatus = STATUS_ERROR;
                message = response['errormsg'];
              }
            } catch (err) {
              jobstatus = STATUS_ERROR;
              message = "Bad response getting job ID: " + err
            }
          }
        });

        // Create status dialog window
        $("<div><div id='statustitle'>Building manifest</div><p id='procstatus'></p></div>")
        .dialog({
          resizable: false,
          modal: true,
          buttons: {
            "Cancel": function() {
              $( this ).dialog( "destroy" );
              // Clear update timer, if available
              if (updatetimer != undefined) {
                clearInterval(updatetimer);
                updatetimer = undefined;
              }
              // Call cancel URL, if available
              if (cancelurl != "") {
                $.post(cancelurl);
              }
            }
          }
        })
        .dialog('widget')
        .addClass('modalDialog');       // hide titlebar via css

        // center the close button
        $('.modalDialog .ui-button').position({
          my: 'bottom center',
          at: 'bottom center',
          of: $('.modalDialog .ui-dialog-buttonpane')
        });

        // Function to update job status window
        function setStatus(htmlmsg) {
          $('#procstatus').html(htmlmsg);
        }

        // Initialize status message
        setStatus(message);
        if (jobstatus == STATUS_ERROR) { return; }

        origonly_opt = "<?php if ($origonly === "1") { echo "&origonly=1"; } ?>";
        folderstrategy_opt = '&folderstrategy=' + $('#FolderStrategy').val();

        // Start async job to build download package
        $.ajax({
          type: "POST",
          url: "svc_buildmanifest.php?jobid=" + jobid + origonly_opt + folderstrategy_opt,
          data: downloadspec,
          success: function(msg){
            // Track event, log error messages
            // ['_trackEvent', 'category', 'action', 'opt_label', opt_val (int)]
            if (msg.match("^Manifest complete!")) {
              _gaq.push(['_trackEvent', 'build_archive', 'success', 'success', 0]);
            } else {
              _gaq.push(['_trackEvent', 'build_archive', 'error', msg, 0]);
            }
            // Clean up after archive job completes,
            // either successfully or unsuccessfully
            if (updatetimer != undefined) {
              clearInterval(updatetimer);
              updatetimer = undefined;
            }
            setStatus(msg);
            // Change button text from "Cancel" to "Close"
            $('.modalDialog .ui-button').button('option', 'label', 'Close');
            cancelurl = ""; // Stop the cancel button from calling a service
          }
        });

        // Start periodic function to update status window every second
        updatetimer = setInterval(function() {
          $.get(statusurl.replace("localhost", "<?php echo $DNS;?>"), "", function(msg) { setStatus(msg); }, 'html');
        }, 8000);
      } // End of function publishManifestFile()
      // Create dynatree
      // Attach the dynatree widget to an existing <div> element
      // and pass the tree options as an argument to the dynatree() function:
      $("#fileSelector").dynatree({
        cookieId: "downloader",
        checkbox: true,
        selectMode: 3,
        persist: true,
        children: json_data,
        onSelect: function(flag, node) {
          <?php
          /*
          * This seems terribly inefficient but appears to work well in practice
          */
          ?>
          countSelectedFiles();
          displayTotal();
        },
        cookie: {
          expires: 90
        }
      });

      // Set root title
      $("#fileSelector").dynatree('getRoot').data.title = project_title;
      // Set name of root for stamped files in archive
      $("#fileSelector").dynatree('getRoot').data.filename = "files";

      // Count selected files
      countSelectedFiles();
      displayTotal();

    });
  </script>

  <form id="downloaderForm" method="POST" action="downloader/buildjob.php">
    <input id="downloadSpec" name="downloadSpec" style="display:none"/>

    <input id="totalFiles" name="totalFiles" style="display:none"/>
    <input id="origonly_opt" name="origonly_opt" style="display:none"/>
    <input id="userID" name="userID" style="display:none"/>
    <input id="userEmail" name="userEmail" style="display:none"/>
    <input id="archiveName" name="archiveName" style="display:none"/>
    <input id="archiveRoot" name="archiveRoot" style="display:none"/>
    <input id="columnSpec" name="columnSpec" style="display:none"/>
    <input id="projectName" name="projectName" style="display:none"/>
    <input id="projectID" name="projectID" style="display:none"/>
    <input id="FolderStrategy" name="FolderStrategy" style="display:none"/>
  </form>

  <form id="downloaderIndexForm" method="POST" action="downloader/buildindexjob.php">
    <input id="downloadSpecIndex" name="downloadSpecIndex" style="display:none"/>
    <input id="totalFilesIndex" name="totalFilesIndex" style="display:none"/>
    <input id="origonly_optIndex" name="origonly_optIndex" style="display:none"/>
    <input id="userIDIndex" name="userIDIndex" style="display:none"/>
    <input id="userEmailIndex" name="userEmailIndex" style="display:none"/>
    <input id="archiveNameIndex" name="archiveNameIndex" style="display:none"/>
    <input id="archiveRootIndex" name="archiveRootIndex" style="display:none"/>
    <input id="columnSpecIndex" name="columnSpecIndex" style="display:none"/>
    <input id="projectNameIndex" name="projectNameIndex" style="display:none"/>
    <input id="projectIDIndex" name="projectIDIndex" style="display:none"/>
    <input id="FolderStrategyIndex" name="FolderStrategyIndex" style="display:none"/>
  </form>
</body>
</html>
