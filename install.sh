#!/bin/bash

function die {
  echo $1
  echo "Please fix and retry"
  exit 1
}

#
# Make sure directory exists and is writeable
#
function chkdir {
  olddir=$(pwd)
  dir=$1
  mkdir -p $dir 2>/dev/null
  chmod 777 $dir 2>/dev/null

  cd $dir && touch foo
  if [ $? -ne 0 ]; then
    die "Cannot access $dir!"
  fi
  rm foo
  cd "$olddir"
}

#
# Attempt to retrieve the value of a PHP config
# usage: extract_php_conf filename config
#
function extract_php_conf {
  file=$1;
  conf=$2;
  line=$(grep "$conf" "$file" | grep -v '^//' | head -1)
  val=$(echo $line | sed -e 's/^.*"\([^"]*\)".*$/\1/')
  echo $val
}

# Installation checklist

myid=$(whoami)

if [ "$myid" != "apache" ]; then
    echo "Please rerun with 'sudo -u apache'"
    exit 1
fi

echo "Running installation and sanity checker for downloader app"
echo "=========================================================="

CONFIG_FILE="config.php"

# make sure config.php exists
if [ ! -f $CONFIG_FILE ]; then
  die "Config file $CONFIG_FILE not found, please check release package"
fi

# check contents of config.php
list_of_configs="
JOB_URLBASE
ARCHIVE_URLBASE
EFILE_ARCHIVE_DIR
DATAMART_PREFIX
DATAMART_AUTH
AWS_BUCKET
STELLENT_IPADDR
GA_TRACKER_ID
EMAIL_ALERT
EFILE_EMAIL_NOTIFY
"
missing_configs=""
for i in $list_of_configs; do
  config=$(grep "$i" "$CONFIG_FILE" | grep -v '^//')
  if [ -z "$config" ]; then
    missing_configs="$missing_configs $i"
  fi
done
if [ ! -z "$missing_configs" ]; then
  die "missing (or unset) configs $missing_configs"
fi

INCLUDE_FILE="includes.php"

# make sure includes.php exists
if [ ! -f "$INCLUDE_FILE" ]; then
  die "Includes file $INCLUDE_FILE not found, please check release package"
fi

# make sure jobs directory is created and is writeable
TMPDIR='/var/www/html/tmp/downloader'
chkdir "$TMPDIR"
chkdir "$TMPDIR/jobs"

EFILE_ARCHIVE_DIR=$(extract_php_conf "$CONFIG_FILE" "EFILE_ARCHIVE_DIR")
chkdir "$EFILE_ARCHIVE_DIR"

# check for old config file
OLDCONF="/var/www/config/downloader-conf.php"
if [ -f "$OLDCONF" ]; then
  echo "Old config file $OLDCONF is still present, please rename or remove"
fi

# make sure pagecounter app is installed (in common)
PAGECOUNTER="/var/www/common/pagecounter/PageCounter.jar"
if [ ! -f "$PAGECOUNTER" ]; then
  die "$PAGECOUNTER not installed!  May need to update /var/www/common"
fi

# make sure stamper app is installed (in common)
STAMPER="/var/www/common/stamper/Stamper.jar"
if [ ! -f "$STAMPER" ]; then
  die "$STAMPER not installed!  May need to update /var/www/common"
fi

# make sure efile app is installed (for EPA-eFile)
EFILEDIR="/var/www/efile"
if [ ! -d $EFILEDIR ]; then
  die "$EFILEDIR not installed!"
fi

if [ ! -f "$EFILEDIR/EPA-eFile.jar" ]; then
  die "$EFILEDIR/EPA-eFile.jar not installed!"
fi

PROP_FILE="$EFILEDIR/EPA-eFile.properties"
if [ ! -f "$PROP_FILE" ]; then
  die "$PROP_FILE not installed!"
fi

# check contents of EPA-eFile.properties
list_of_configs="
endpoint
userid
password
"
missing_configs=""
for i in $list_of_configs; do
  config=$(grep "^$i" "$PROP_FILE")
  if [ -z "$config" ]; then
    missing_configs="$missing_configs $i"
  fi
done
if [ ! -z "$missing_configs" ]; then
  die "missing (or unset) eFile configs $missing_configs"
fi

PHP_CLI=$(extract_php_conf "$INCLUDE_FILE" "PHP_CLI")
$PHP_CLI -v 2>&1 >/dev/null
if [ "$?" -ne 0 ]; then
  die "Can't execute PHP_CLI, check includes.php"
fi

echo "Done!"

