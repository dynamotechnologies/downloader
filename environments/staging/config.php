<?php

// Include common defs
require_once("includes.php");

/*
 *	Temporary config file for install-specific properties
 *
 *	Remainder of this file cannot contain define() directives
 *	All configs must be in PHP globals because they will be
 *	migrated to and read from a DB table
 */

/*
 *      Set PHPMailer settings
 */

$SMTP_USERNAME="AKIAIIOS5R4J2M57TAKQ";
$SMTP_PASS="AnqyAWYpPLLfq7z5iTjjOofIaHrIGSY2RW5H2PSq1UT8";
$SMTP_HOST="email-smtp.us-west-2.amazonaws.com";
$USE_SMTP_AUTH=true;
$SMTP_AUTH_METHOD='tls';
$SMTP_PORT=587;
$SMTP_IS_HTML=true;
$SMTP_FROM="noreply@ecosystem-management.org";

/*
 *	Set physical location of job working directory
 */
$JOB_DIR="/var/www/html/tmp/downloader/jobs";

/*
 *	Set web location of job working directory
 */
// $JOB_URLBASE="http://localhost/tmp/downloader/jobs"; # DEV
$JOB_URLBASE="http://localhost/tmp/downloader/jobs"; # CORETST
//$JOB_URLBASE="http://data.ecosystem-management.org/tmp/downloader/jobs"; # PILOT
// $JOB_URLBASE="http://coretst2.ecosystem-management.org/tmp/downloader/jobs";

/*
 *      Set location of efile log archive
 */
$EFILE_ARCHIVE_DIR="/var/www/efile/logs";

/*
 *	Set target AWS bucket for final archive file
 */
// $AWS_BUCKET = "nepadocsdev";     // DEV
$AWS_BUCKET = "nepadocstest";    // CORETST
//$AWS_BUCKET = "nepadocs";        // PILOT

/*
 *	Set web location of final archive file
 */
// $ARCHIVE_URLBASE="http://nepadocsdev.s3.amazonaws.com";	# DEV
$ARCHIVE_URLBASE= "http://nepadocstest.s3-us-gov-west-1.amazonaws.com";
#"http://nepadocstest.s3.amazonaws.com";	# CORETST
//$ARCHIVE_URLBASE="http://nepadocs.s3.amazonaws.com";		# PILOT

/*
 *	Set location of Stellent server
 */
// $STELLENT_IPADDR="10.0.14.11";		# CORETST
// $STELLENT_IPADDR="10.0.15.12";		# PILOT
// $STELLENT_IPADDR="oracletest2";
$STELLENT_IPADDR="oracleprod2";

/*
 *	Doc ID for repository test file (to test repo availability)
 */
$STELLENT_TESTFILE = "FSTST_000001";

/*
 *	Set auth for Stellent access
 */
$STELLENT_AUTH = 'sysadmin:p1cgorcl';

/*
 *	Set command for PDF page counter utility
 *	$PAGECOUNTER <filename>
 */
$PAGECOUNTER = 'java -jar /var/www/common/pagecounter/PageCounter.jar';

/*
 *	Set command for EPA eFile submit utility
 *	$EFILE_SUBMIT <efile.data>
 *	(efile.data contains pointers to submit.csv, files.csv, and various
 *	downloaded PDF files)
 */
$EFILE_SUBMIT='java -jar /var/www/efile/EPA-eFile.jar -submit';

/*
 *	Set prefix for Datamart access
 */
// $DATAMART_PREFIX = 'http://localhost:7001/api/1_0';	# DEV
// $DATAMART_PREFIX = 'http://localhost:7001/api/1_0';	# CORETST
$DATAMART_PREFIX = 'https://dmd-tst.ecosystem-management.org/api/1_0';	# PILOT
// $DATAMART_PREFIX = 'http://localhost:7001/api/1_0';

/*
 *	Set auth for Datamart access
 */
// $DATAMART_AUTH = 'admin:p1cgorcl';	# DEV
// $DATAMART_AUTH = 'admin:p1cgorcl';	# CORETST
$DATAMART_AUTH = 'admin:D0ntP4niC';	# PILOT

/*
 *	Set ID for Google Analytics tracking
 *	(leave undefined for development)
 */
$GA_TRACKER_ID = 'UA-2172917-9';	# CORETST
//$GA_TRACKER_ID = 'UA-2172917-10';	# PILOT

/*
 *	Set email addresses for alerts
 */
#$EMAIL_ALERT = 'fs-drm-alerts@phaseonecg.com';
$EMAIL_ALERT='jeremy.lam@dynamotechnologies.com,derek.schaefer@dynamotechnologies.com,nicholas.vercruysse@dynamotechnologies.com';

// $EFILE_EMAIL_NOTIFY = 'fs-efiletest@phaseonecg.com';	# CORETST
//$EFILE_EMAIL_NOTIFY = 'fs-efile@phaseonecg.com';	# PILOT
$EFILE_EMAIL_NOTIFY = 'jeremy.lam@dynamotechnologies.com,derek.schaefer@dynamotechnologies.com,nicholas.vercruysse@dynamotechnologies.com';

?>
