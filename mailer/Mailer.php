<?php
  require_once 'PHPMailerAutoload.php';
  require_once '/var/www/html/downloader/config.php';
  //needs to be absolute, called from status_daemon, php-cli

  class Mailer {
    private $mail;

    function __construct() {
      global $SMTP_FROM;
      global $SMTP_USERNAME;
      global $SMTP_PASS;
      global $SMTP_HOST;
      global $USE_SMTP_AUTH;
      global $SMTP_AUTH_METHOD;
      global $SMTP_PORT;
      global $SMTP_IS_HTML;

      $mail = new PHPMailer();
      $mail->isSMTP();
      $mail->setFrom($SMTP_FROM, 'eFile Manager');
      $mail->Username = $SMTP_USERNAME;
      $mail->Password = $SMTP_PASS;
      $mail->Host = $SMTP_HOST;
      $mail->SMTPAuth = $USE_SMTP_AUTH;
      if($mail->SMTPAuth) {
        $mail->SMTPSecure = $SMTP_AUTH_METHOD;
      }
      $mail->Port = $SMTP_PORT;
      $mail->isHTML($SMTP_IS_HTML);
      $this->mail = $mail;
    }

    function sendEmail($subject, $body, $to) {
      $this->mail->ClearAllRecipients();
      $this->mail->addAddress($to);
      $this->mail->Subject = $subject;
      $this->mail->Body = $body;
      $success = $this->mail->send();
      return $success;
    }

  }
?>
