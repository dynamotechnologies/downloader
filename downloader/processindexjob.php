<?php
/*
 *  Manifest-building service for downloader
 *
 *  Request consists of a GET with the following URL parameters:
 *      jobid - used as the name of the working directory
 *      key - a randomized integer used to prevent this script from
 *            arbitrary execution
 *      startfile - index of next file in job.data to be processed
 *
 *  startfile can be changed to resume processing at any document
 *
 *  In addition, the job requires several job-description files in the
 *  working directory, /var/www/html/tmp/downloader/jobs/[jobid]/:
 *      jobdata.php - a list of files to be archived
 *      request.data - a serialized array of job parameters
 *
 *  request.data consists of a list of files to process, possibly
 *  including albums (folders that must be recursively expanded)
 *  Entries consist of comma-separated urlencoded properties in the order:
 *      path,filename,docid
 *
 *  The filename is the document's title.  Actual document filenames are
 *  generally not very descriptive so the title is used as the new filename
 *  in the archive.
 *
 *  Filenames need to be unique within a single category, so if multiple
 *  documents have the same title, the filename will be disambiguated with
 *  a two-digit extension.  If more than 99 documents have the same title
 *  in the same category, the archive will fail.
 *
 *  The title does not normally indicate the type of the file, and in any
 *  case there is some logic to determine which rendition of the file will
 *  be included in the archive.  If the filetype (based on the original
 *  filename) is of a type that we expect to be able to convert to PDF, we
 *  include the PDF version.  Otherwise the original file is included in
 *  the archive.  The list of convertible filetypes is (case insensitive):
 *  doc,bmp,docx,dot,gif,htm,html,jpeg,jpg,png,ppt,rtf,txt,wpd,pdf
 *
 *  jobdata.php includes the following parameters:
 *      totalFiles	- count of files (including expanded albums)
 *      origonly_opt	- ==1 to download original files instead of PDFs
 *      userID		- user ID
 *      userEmail	- email for final job notice
 *      secretKey	- randomized integer, must match "key", above
 *      archiveName	- name for archive file
 *      columnSpec	- coded list of columns to include in final output
 *      projectName	- project name
 *      projectID	- project ID
 *
 *  During the archiving process, files are written to a working directory
 *  at /var/www/html/tmp/downloader/jobs/[JOBID].
 *
 *  For the most part, we don't expect the project file set to change and/or
 *  to be rearranged while the user is working on it, although this is
 *  possible.  Rearranging files is probably benign, but removing files may
 *  cause the archive process to fail.
 *
 *  Possible errors:
 *  - Error getting document (may have been deleted while user was editing
 *    the package?)
 *  - Error creating hierarchy (bad file/dir name?)
 *  - Out of space?
 *  - Permissions?
 *  - Error uploading to edge server?
 */
require_once('../rb.php');
require_once('../aws_utils.php');
require_once('../dmd_utils.php');
require_once('../datamart_utils.php');
require_once('../manifest_utils.php');
require_once('../config.php');
require_once('PHPMailerAutoload.php');

$DATAMART = new datamart_utils();

$mail = new PHPMailer();
$mail->isSMTP();
$mail->setFrom($SMTP_FROM, 'Project File Downloader');
$mail->Username = $SMTP_USERNAME;
$mail->Password = $SMTP_PASS;
$mail->Host = $SMTP_HOST;
$mail->SMTPAuth = $USE_SMTP_AUTH;
if ($mail->SMTPAuth) {
  $mail->SMTPSecure = $SMTP_AUTH_METHOD;
}
$mail->Port = $SMTP_PORT;
$mail->isHTML($SMTP_IS_HTML);
// Logfile tags
define("L_DEBUG", "DEBUG");
define("L_INFO", "INFO");	// Generally data to be sent back to user
define("L_ERROR", "ERROR");

error_reporting(0);

/*
 *  Define globals
 */
$JOBID = $_GET['jobid'];
if (!isset($JOBID)) { echo "No jobid specified"; exit(1); }

/*
 *	Replace nonprintable characters and reserved Windows metachars
 *	\/:*?"<>|
 */
function sanitizename($str) {
  // Remove nonprinting chars
  $str = preg_replace('/[[:cntrl:]]+/', '', $str);

  // Remove illegal filename chars
  $search = array('\\', '/', ':', '*', '?', '"', '<', '>', '|');
  $replace = "_";
  $str = str_replace($search, $replace, $str);

  // Remove non-ASCII chars
  $str = preg_replace('/[^(\x20-\x7F)]+/','_', $str);

  return $str;
}

function getFile($docid, $filename, $pdf_flag) {
  $fp = fopen($filename, "w");
  if ($fp == null) {
    quit("Could not open document $filename");
  }
  try {
    if ($pdf_flag) {
      fwrite($fp, dmd_getPDF($docid));
    } else {
      fwrite($fp, dmd_getFile($docid));
    }
  } catch (Exception $e) {
    quit("Error retrieving file: " . $e->getMessage());
  }
  fclose($fp);
}

/*
 *  Mark error status and quit
 */
function quit($msg) {
    global $NEEDS_RESTART;
    global $EMAIL_ALERT;

    echo $msg;
    logERROR($msg);
    if (isset($NEEDS_RESTART)) {
        touch($NEEDS_RESTART);	// signal for help
    }
    sendErrorEmail($EMAIL_ALERT);
   // exit(1);
}

/*
 *	Build and send HTML confirmation email
 */
function sendConfirmationEmail($email, $htmlmsg) {
    global $POSTVARS;
    global $mail;
    $shortname = htmlentities($POSTVARS['userIDIndex']);
    $archive_name = htmlentities($POSTVARS['archiveNameIndex']);
    $projname = htmlentities($POSTVARS['projectNameIndex']);
    $projid = htmlentities($POSTVARS['projectIDIndex']);
    $date = htmlentities(strftime('%F'));
    $time = htmlentities(strftime('%T'));

    $title = "$projname ($projid)";

    $msg = "<html><body>";
    $msg .= "<p>Dear $shortname,</p>";
    $msg .= "<p>Your request for a set of documents from the $projname ($projid) project has been processed successfully.</p>";
    $msg .= $htmlmsg;
    $msg .= "<p>Thank you for using PALS!</p>";

    /*$headers = "From: Project File Downloader<noreply@fs.fed.us>\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    $to = $email;
    */
    $hostname = gethostname();
    $subject = "$hostname: Project File Downloader job completion - $title";

    $mail->addAddress($email, '');
    $mail->Subject = $subject;
    $mail->Body = $msg;
    $mail->send();
    //mail($to, $subject, $msg . "</body></html>", $headers);
}

/*
 *	Build and send error report
 */
function sendErrorEmail($email) {
    global $LOGFILE;
    global $JOBID;
    global $RESTART;
    global $POSTVARS;
    global $mail;
    $msg = "<html><body>";

    $date = htmlentities(strftime('%F'));
    $time = htmlentities(strftime('%T'));
    $title = "";

    if (isset($JOBID)) {
        $msg .= "<p>Error encountered while processing job $JOBID on $date at $time.</p>";
    }
    if (isset($POSTVARS)) {
        $projname = htmlentities($POSTVARS['projectNameIndex']);
        $projid = htmlentities($POSTVARS['projectIDIndex']);
        $title = "$projname ($projid)";
        $msg .= "<p>Project $projname ($projid)</p>";
    }
    if (isset($RESTART)) {
        $msg .= "<p>Job restart script at: <PRE>$RESTART</PRE>.</p>";
    } else {
        $msg .= "<p>No job restart script defined</p>";
    }

    /*$headers = "From: Project File Downloader<noreply@fs.fed.us>\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    */
    $hostname = gethostname();
    $subject = "$hostname: ERROR: Download for - $title";

    $msg .= "\n";
    $msg .= "<br/>\n";
    $msg .= "<pre>\n";
    $msg .= "=================\n";
    $msg .= "Logfile: $LOGFILE\n";
    $msg .= "=================\n";
    $msg .= file_get_contents($LOGFILE);
    $msg .= "</pre>\n";
    $mail->addAddress($email, '');
    $mail->Subject = $subject;
    $mail->Body = $msg;
    $mail->send();
    //mail($email, $subject, $msg . "</body></html>", $headers);
}


/*
 *  Prepend a timestamp to a log message
 */
function timestamp($msg) {
    $tstamp = strftime('%F %T');
    return "$tstamp $msg";
}

function logDEBUG($msg) { writeLogMsg(L_DEBUG, timestamp($msg)); }
function logINFO($msg) { writeLogMsg(L_INFO, timestamp($msg)); }
function logERROR($msg) { writeLogMsg(L_ERROR, timestamp($msg)); }

function writeLogMsg($prefix, $data) {
    global $LOGFILE;

    if (!isset($LOGFILE)) {
      //  exit(1);        // log file undefined
    }
    try {
        $fp = fopen($LOGFILE, "a");
        if ($fp == null) {
            quit("Could not open log file $LOGFILE");
        }
        fwrite($fp, "$prefix: $data\n");
        fclose($fp);
    } catch (Exception $e) {
        // Fatal condition, quit immediately
        echo "Could not open log file $LOGFILE: " . $e->getMessage();
        echo $data;
       // exit(1);
    }
}

/*
 *	Archive a single document, keep metadata in list for manifest
 *	$filespec is a comma-separated list of urlencoded fields:
 *
 *	field 1: Path to file (using / as directory seperators, and
 *		each directory name is urlencoded separately
 *	field 2: File name
 *	field 3: Docid
 *
 *	$idx is the 0-based index of the document in the list, which
 *	eventually provides a sort order for the metadata in the manifest
 *
 *	NOTE: Any albums in the job spec need to be expanded into a list of
 *	individual filespec entries prior to calling this function.
 *
 *	$folderstrategy can be either "nested" (default) or "flat"
 *	If $folderstrategy == "flat" then store all documents in the same
 *	folder.  This will increase the possibility of name collisions, but
 *	the final document paths will be shorter.
 */


 function processFilespec($filespec, $idx, $folderstrategy, &$errorProcessFilespec) {

    global $MAX_MEMBER_LENGTH, $MAX_PATH_LENGTH;
    global $PDF_CONVERSION_TYPES;

    // split filespec into properties
    $props = explode(",", $filespec);
    $path = urldecode($props[0]);
    $filename = urldecode($props[1]);
    $docid = urldecode($props[2]);

    // urldecode and sanitize path elements
    // Note incoming path ends with a slash
    // mkdir -p path (in workingdir)
    if (substr($path, -1) == "/") {
        $path = substr($path, 0, -1);	// remove trailing slash
    }
    $pathparts = explode("/", $path);
    $spath = "";

    if ($folderstrategy == "flat") {
        // Store all files in same folder (named after root of file tree)
        $pathelem = sanitizename(urldecode($pathparts[0]));
        if (strlen($pathelem) > $MAX_MEMBER_LENGTH) {
            $pathelem = substr(0, $MAX_MEMBER_LENGTH, $pathelem);
        }
        $spath = $pathelem;
    } else {
        // Default folderstrategy is "nested"
        foreach ($pathparts as $part) {
            $pathelem = sanitizename(urldecode($part));
            if (strlen($pathelem) > $MAX_MEMBER_LENGTH) {
                $pathelem = substr(0, $MAX_MEMBER_LENGTH, $pathelem);
            }
            $spath = "$spath/$pathelem";
        }
        $spath = substr($spath, 1);	// remove leading slash
    }

    if (is_dir("$spath") == False) {
        $rc = mkdir("$spath", 0777, True);
        if (!$rc) {
            quit("Could not create temp dir $spath");
        }
    }

    // Get unique target filename
    $sfilename = sanitizename($filename);
    $targetfname = "${spath}/${sfilename}";

    // Maximum path length for windows is 260, including null terminator,
    // drive letter, and colon.  There is a Unicode API for NTFS that permits
    // longer paths but we are probably not allowed to assume them, for backward
    // compatibility.

    // Check the length of the full path and filename.  MAX_PATH_LENGTH is
    // currently set at 250 characters.
    // This check is made without knowing the file extension type but we do
    // not allow extensions of longer than 4 characters.

    // If the dotted extension is longer than 4 characters long, assume it is
    // just part of the file name.

    // Windows file names are case-insensitive, so we need to make all of the
    // uniqueness checks case-insensitive too.

    if (strlen($targetfname) > $MAX_PATH_LENGTH) {
        if (strlen("${spath}/") > $MAX_PATH_LENGTH) {
            quit("Path too long, $targetfname &gt; $MAX_PATH_LENGTH");
        } else {
            // Desperation move: truncate filename
            $targetfname = substr(0, $MAX_PATH_LENGTH, $targetfname);
        }
    }

    // Make targetfname unique with a numeric suffix, if needed
    $origtargetfname = $targetfname;
    if (nameExists($targetfname)) {
        $targetfname = "${targetfname}_$idx";
        if (nameExists($targetfname)) {
            quit("Unable to find unique name for $origtargetfname");
        }
    }

    $sfilename = basename($targetfname);

    // Get original file metadata
    $metadata = array();
    try {
        $metadata = dmd_getMetadata($docid);
    } catch (Exception $e) {
       // quit("Error retrieving metadata: " . $e->getMessage());
       $errorProcessFilespec .= "Error retrieving metadata: " . $e->getMessage() ."\n";
    }

    $metadata['sfilename'] = $sfilename;	// Sanitized filename

    // Set $metadata['sensitiveflag'] to true/false
    $metadata['sensitiveflag'] = dmd_issensitive($metadata);

    $orig_fname = trim($metadata['filename']);
    $orig_ftype = "";
    $dot_ext = "";

    // Get original file type
    if (preg_match('/\.[[:alnum:]+$\-]{1,10}$/', $orig_fname) > 0) {
        $orig_ftype = preg_replace('/^.*\.([[:alnum:]+$\-]{1,10})$/', '${1}', $orig_fname);
        $orig_ftype = strtolower($orig_ftype);
        $dot_ext = '.' . $orig_ftype;
    }

/*    // Determine file rendition to download
    if (in_array($orig_ftype, $PDF_CONVERSION_TYPES)) {
        $dot_ext = '.pdf';
        getFile($docid, "${targetfname}${dot_ext}", True);	// get PDF
    } else {
        getFile($docid, "${targetfname}${dot_ext}", False);	// get original
    }
*/
    $metadata['path'] = "${targetfname}${dot_ext}";	// Path to file

    $metadata['docid'] = $docid;

    // store metadata in persistent db
    // use path+fname as unique key

    $b = R::dispense('metadata');
    $b->key = strtolower($targetfname);
    $b->metadata = serialize($metadata);
    $b->idx = $idx;
    $b->filename = $sfilename;
    $b->sensitiveflag= $metadata['sensitiveflag'];
    R::store($b);

    // Downloaded files will be periodically archived by updateZipArchive()
}

/* Count # of files under a directory */
function countFiles($dirname) {
    $dirname = rtrim($dirname, "/");
    $total = 0;
    $dirlist = scandir($dirname);
    foreach ($dirlist as $entry) {
        if ($entry === ".") { continue; }
        if ($entry === "..") { continue; }
        if (is_dir("$dirname/$entry")) {
            $total += countFiles("$dirname/$entry");
        } else {
            $total += 1;
        }
    }
    return $total;
}

/* Sum sizes of files under a directory */
function totalFileSizes($dirname) {
    $dirname = rtrim($dirname, "/");
    $total = 0;
    $dirlist = scandir($dirname);
    foreach ($dirlist as $entry) {
        if ($entry === ".") { continue; }
        if ($entry === "..") { continue; }
        if (is_dir("$dirname/$entry")) {
            $total += totalFileSizes("$dirname/$entry");
        } else {
            $total += filesize("$dirname/$entry");
        }
    }
    return $total;
}

/*
 *  Take all files from $POSTVARS['archiveRoot'] and move to zipfile
 *
 *  The original zipfile format has a limit of 64k files per archive
 *  We limit the number of files to 50k per zipfile, but there is still
 *  only one manifest spreadsheet, which is attached to the first zipfile.
 *
 *  If a zipfile is over 2 Gb, start a new zipfile
 *
 *  The first zipfile is archive.zip
 *  The second zipfile is archive_2.zip
 *  The third zipfile is archive_3.zip, etc.
 *
 */

 //commenting this function, not being used
/* function updateZipArchive() {
    global $POSTVARS;
    global $MAX_FILES_PER_ARCHIVE;

    $archive_root = $POSTVARS['archiveRootIndex'];
    $filecount = countFiles($archive_root);

    $archive_idx = 1;		// counter for multiple zipfiles
    $archive_filecount = 0;	// number of files in zipfile
    $archivename = "archive";	// name of zipfile (without extension)

    // Get info for current archive file
    $latest_archive = getLatestArchive();
    if ($latest_archive != null) {
        $archive_idx = $latest_archive->archiveidx;
        $archive_filecount = $latest_archive->filecount;
        $archivename = $latest_archive->archivename;
    }

    $zipfile_size_limit_mb = 2000;
    $archivesize_mb = 0;
    if (file_exists("$archivename.zip")) {
        $archivesize_mb = intval(filesize("$archivename.zip") / (1024 * 1024));
    }
    logDEBUG("File $archivename.zip is $archivesize_mb mb");

    // if # records in archive + # new files > max files, start new archive
    // OR if size of archive > 2 Gb, start new archive
    if (($archive_filecount + $filecount > $MAX_FILES_PER_ARCHIVE) ||
        ($archivesize_mb > $zipfile_size_limit_mb)) {
        $archive_idx += 1;
        $archive_filecount = 0;
        $archivename = "archive_${archive_idx}";
        logINFO("Starting new archive $archivename");
    }

    // zip zipfile file-to-archive
    $rc = 0;
    $output = array();
    $zipcmd = "zip -gmr $archivename \"${archive_root}\"";
    exec($zipcmd, $output, $rc);
    if ($rc == 12) {
        return;	// nothing to do
    } else if ($rc !== 0) {
        quit("Zip utility quit with error [$rc]");
    }

    // Update db record for zipfile stats
    $b = findArchive($archive_idx);
    if ($b == null) {
        $b = R::dispense('archives');
        $b->archiveidx = $archive_idx;
        $b->filecount = 0;
        $b->archivename = $archivename;
    }
    $b->filecount = $b->filecount + $filecount;
    R::store($b);
} */

/*
 *  Build Excel manifest, attach to archive.zip
 */
function buildManifest($archive_name, $column_spec) {

    // Build manifest
    // Format provided by column_spec
    // (Old default: Category, Doc Name, Link, Date, Author, Description)
    $manifest_data = array();

    $metadata_beans = getMetadata();
    foreach ($metadata_beans as $b) {
        $metadata = unserialize($b->metadata);

        $category = trim($metadata['appdoctype']);
        $doctitle = $metadata['sfilename'];
        $link = $metadata['path'];
        $docdate = trim($metadata['moreMetadata']['docdate']);
        if((isset($metadata['moreMetadata']['level3id']) && !is_null($metadata['moreMetadata']['level3id']) && $metadata['moreMetadata']['level3id'] == 57) || (isset($metadata['moreMetadata']['caraid']) && !is_null($metadata['moreMetadata']['caraid'])) || (isset($metadata['moreMetadata']['caraletterid']) && !is_null($metadata['moreMetadata']['caraletterid'])) || (isset($metadata['moreMetadata']['caraphaseid']) && !is_null($metadata['moreMetadata']['caraphaseid']))) {
        $author = trim($metadata['author']);
         } else {
        $author = trim($metadata['moreMetadata']['docauthor']);
        }
        $description = trim($metadata['moreMetadata']['docsummary']);

        $group = trim($metadata['moreMetadata']['level2name']);
        $subgroup = trim($metadata['moreMetadata']['level3name']);
        $filename = trim($metadata['filename']);
        $pubdate = trim($metadata['pubdate']);
        $recipient = trim($metadata['moreMetadata']['addressee']);
        $sensitiveflag = $metadata['sensitiveflag'];

        $row = array();
        $row[FL_CATEGORY] = $category;
        $row[FL_DOCTITLE] = $doctitle;
        $row[FL_DOCDATE] = $docdate;
        $row[FL_AUTHOR] = $author;
        $row[FL_DESCRIPTION] = $description;

        $row[FL_LINK] = $link;

        $row[FL_GROUP] = $group;
        $row[FL_SUBGROUP] = $subgroup;
        $row[FL_FILENAME] = $filename;
        $row[FL_PUBDATE] = $pubdate;
        $row[FL_RECIPIENT] = $recipient;

        $row[FL_SENSITIVEFLAG] = $sensitiveflag;

        $manifest_data[] = $row;
    }

    try {
/*
        // Build CSV manifest
        $manifest_fname="${archive_name}.csv";
        manifest_build_csv($archive_name, $column_spec, $manifest_data, $manifest_fname);
*/

        // Build Excel manifest
        $manifest_fname="${archive_name}.xlsx";	// Build Excel2007 file
        manifest_build_xls($archive_name, $column_spec, $manifest_data, $manifest_fname);
    } catch (Exception $e) {
        quit("Error building manifest: " . $e->getMessage());
    }

	// Do not need to create archive file for manifest building
/*
    $rc = 0;
    $output = array();
    $zipcmd = "zip -r archive \"$manifest_fname\"";
    exec($zipcmd, $output, $rc);
    if ($rc !== 0) {
        quit("Zip utility quit with error [$rc]");
    }
*/
}

/*
 *  Return true if filename already exists in the archive
 *
 *  Compare the lowercased name against the 'key' in the metadata table
 */
function nameExists($name) {
    $found = R::findOne('metadata', ' key = :name ',
                         array(strtolower($name)));
    return ($found != null);
}

/*
 *  Return list of all metadata
 */
function getMetadata() {
    return R::findAll('metadata', 'ORDER BY idx');
}

/*
 *  Count archived documents
 */
function countMetadata() {
    return R::count('metadata');
}

/*
 *  Return list of sensitive documents
 */
function getSensitive() {
    return R::find('metadata', 'sensitiveflag = 1 ORDER BY idx');
}

/*
 *  Count sensitive documents
 */
function countSensitive() {
    return R::count('metadata', 'sensitiveflag = 1');
}

/*
 *  Find archive by index
 */
function findArchive($archive_idx) {
    return R::findOne('archives', 'archiveidx = ?', array($archive_idx));
}

/*
 *  Return list of all archives
 */
function getArchives() {
    return R::findAll('archives', 'ORDER BY archiveidx');
}

/*
 *  Count archives
 */
function countArchives() {
    return R::count('archives');
}

/*
 *  Get latest archive index
 *  Returns null if no archive files have been created
 */
function getLatestArchive() {
    return R::findOne('archives', ' 1 ORDER BY archiveidx DESC LIMIT 1 ');
}

/*
 *  Update restart.php to point to the next file to process
 */
function updateRestarter($jobid, $key, $startfile) {
    global $RESTART;

    $fp = fopen($RESTART, "w");
    $phpcmds = array();
    $phpcmds[] = '$_GET["jobid"]="' . $jobid . '";';
    $phpcmds[] = '$_GET["key"]="' . $key . '";';
    $phpcmds[] = '$_GET["startfile"]="' . $startfile . '";';
    $phpcmds[] = 'chdir("' . __DIR__ . '");';
    $phpcmds[] = '// include_once "processindexjob.php";';
    $cmdline = implode("\n", $phpcmds);
    fwrite($fp, "<?php $cmdline ?>");

    fclose($fp);
}

/*
 *  Erase any old beans that will conflict with this job restart
 */
function cleanupDB($startid) {
    $metadata_beans = R::find('metadata', " idx >= :startid ",
        array(':startid' => $startid));
    foreach ($metadata_beans as $bean) {
        R::trash($bean);
    }
}

/*
 *  Prevent HTML mail readers from converting the URL text into a link
 *
 *  So far the most reliable way to deal with this is to remove the
 *  protocol (http://) and try to conceal the domain (by wrapping it
 *  in <span> tags)
 *
 *  Unfortunately the display tends to wrap on dashes, and even setting
 *  the style to nowrap does not solve the problem, e.g.
 *  <span style="white-space: nowrap;"></span>
 */
function unlinkify($url) {
    $url = preg_replace("/^http:\/\//", "", $url, 1);
    return preg_replace("/\.com/", "<span>.com</span>", $url, 1);
}

/*
 *	Process job request
 */
function main() {
    global $JOBID;
    global $JOB_DIR;
    global $POSTVARS;
    global $LOGFILE;
    global $RESTART;
    global $NEEDS_RESTART;

    global $ARCHIVE_BUFFER;		// Extract N documents before zipping
    global $MAX_FILES_PER_ARCHIVE;	// Max docs allowed per zipfile

    global $ARCHIVE_URLBASE;
    global $PDF_CONVERSION_TYPES;

    global $DATAMART;

    if (! isset($_GET['jobid'])) {
        quit("No job ID!  Can't continue");
    }
    $JOBID = $_GET['jobid'];

    // Change current dir to working dir
    $workingdir = "$JOB_DIR/$JOBID";
    $dir = opendir($workingdir);
    if (!$dir) {
        quit("Job directory for job $JOBID not found!  Can't continue");
    }
    closedir($dir);
    chdir($workingdir);

    if (! isset($_GET['startfile'])) {
        quit("No startfile index!  Can't continue");
    }
    $startfile = $_GET['startfile'];

    $requestdata = "$workingdir/requestdata.php";
    $jobdata = "$workingdir/job.data";
    $RESTART = "$workingdir/restart.php";
    $jobdbfile = "$workingdir/job.db";

    $LOGFILE = "$workingdir/log.txt";
    $NEEDS_RESTART = "$workingdir/NEEDS_RESTART";

    logINFO("Starting job");

    unlink($NEEDS_RESTART);     // Clear restart flag, if any

    $POSTVARS = unserialize(file_get_contents($requestdata));

    // Only permit this script to be called from localhost
    if (! isset($_GET['key'])) {
        quit("No key!  Can't continue");
    }
    $key = $_GET['key'];
    if ($POSTVARS['secretKeyIndex'] != $key) {
        quit("Script can only be called from localhost!  Can't continue");
    }

    if (! isset($POSTVARS['archiveRootIndex'])) {
        quit("No archiveRoot defined, can't continue");
    }

    if (isset($POSTVARS['maxFilesPerArchive'])) {
        $MAX_FILES_PER_ARCHIVE = $POSTVARS['maxFilesPerArchive'];
    }
    if ($MAX_FILES_PER_ARCHIVE < $ARCHIVE_BUFFER) {
        $ARCHIVE_BUFFER = $MAX_FILES_PER_ARCHIVE;
    }

    // create/open job.db
    R::setup("sqlite:$jobdbfile",'user','password');

    // cleanup any DB records from previous runs
    cleanupDB($startfile);

    $projectid = $POSTVARS['projectIDIndex'];

    if ($startfile == 0) {
        logINFO("Expanding albums");
        // expand any albums in the job data file
        $newjobdata = "$workingdir/job.data.new";
        $origfp = fopen($jobdata, "r");
        if ($origfp == null) {
            quit("Could not open job data file $jobdata");
        }
        $newfp = fopen($newjobdata, "w");
        if ($newfp == null) {
            quit("Could not open temp job file $newjobdata");
        }

        while (($line = fgets($origfp)) !== false) {
            $attrs = explode(",", $line);
            $origpath = $attrs[0];
            $foldername = $attrs[1];
            if (urldecode($attrs[2]) == "ALBUM") {
                $containerid = trim(urldecode($attrs[3]));
                $phaseid = trim(urldecode($attrs[4]));
                try {
                    $expanded_files = $DATAMART->datamart_expandAlbum($projectid, $containerid, $phaseid);
                } catch (Exception $e) {
                    quit("Error expanding albums: " . $e->getMessage());
                }
                foreach ($expanded_files as $file) {
                    $path = $origpath . urlencode($file['path']);
                    $filename = urlencode($file['filename']);
                    $docid = urlencode($file['docid']);
                    $newline = "$path,$filename,$docid\n";
                    fwrite($newfp, $newline);
                }
            } else {
                fwrite($newfp, $line);
            }
        }

        // swap old jobfile for new one
        fclose($origfp);
        fclose($newfp);
        $tempjobdata = "$workingdir/job.data.temp";
        rename($jobdata, $tempjobdata);
        rename($newjobdata, $jobdata);
        unlink($tempjobdata);
        logINFO("Done expanding albums");

    }

    // Start downloads
    if ($POSTVARS['origonly_optIndex'] === "1") {
        $PDF_CONVERSION_TYPES = array();	// Only download originals
    }

    $totalfiles = $POSTVARS['totalFilesIndex'];

    $folderstrategy = "nested";
    if (isset($POSTVARS['FolderStrategyIndex'])) {
        if ($POSTVARS['FolderStrategyIndex'] == "flat") {
            $folderstrategy = "flat";
        }
    }

    $jobdatafp = fopen($jobdata, "r");
    if ($jobdatafp == null) {
        quit("Could not open job file $jobdata");
    }
    $lineno = 0;
    $jobline = false;
    while (($jobline = fgets($jobdatafp)) !== false) {
        if ($lineno >= $startfile) { break; }   // Skip files before startfile
        $lineno++;
    }
    // Download files to archive directory, and zip them up
    // once every ARCHIVE_BUFFER (=200) files
    $archive_root = $POSTVARS['archiveRootIndex'];

    $buffer_size_limit = 1024 * 1024 * 1000; // 1 gb
    // adding new variable to hold error messages in processFilespec function
    $errorProcessFilespec = "";
    do {
        if ($jobline !== false) {
            $fileno = $lineno + 1;
            logINFO("Processing file $fileno of $totalfiles");
            processFilespec(trim($jobline, "\n"), $lineno, $folderstrategy, $errorProcessFilespec);
            $lineno++;

          //comment creating archive file
          /*  // Every 200 files, add files to zip archive
            if ($lineno % $ARCHIVE_BUFFER == 0) {
                updateZipArchive();
                updateRestarter($JOBID, $_GET['key'], $lineno);
            } else {
                // If we have > 1 Gb of data, add files to zip archive
                if (totalFileSizes($archive_root) > $buffer_size_limit) {
                    updateZipArchive();
                    updateRestarter($JOBID, $_GET['key'], $lineno);
                }
            } */
        }
    } while (($jobline = fgets($jobdatafp)) !== false);

   //send email if we have error in processFilespec function
      if (strlen($errorProcessFilespec) > 0) {
	  quit($errorProcessFilespec);
	}
   // updateZipArchive();
   // updateRestarter($JOBID, $_GET['key'], $lineno);


    $archive_name = $POSTVARS['archiveNameIndex'];
    $column_spec = $POSTVARS['columnSpecIndex'];

    logINFO("Building manifest");
    buildManifest($archive_name, $column_spec);

	// Added new code to upload manifest on aws
	// Upload manifest into edge server, compute URL
    $manifest_fname="${archive_name}.xlsx";
    $filesize_b = filesize($manifest_fname);
    $filesize_k = strval(intval($filesize_b / 1024));
    ////$archive_zipfile_name = "${archive_name}.zip";

    logINFO("Uploading ${filesize_k}k manifest to file server");
    try {
        aws_putfile($manifest_fname, $manifest_fname);
    } catch (Exception $e) {
        quit("Error uploading file to AWS: " . $e->getMessage());
    }

    $manifest_link="$ARCHIVE_URLBASE/$manifest_fname";


	//commenting below code avoid uploading archieve files on aws
	/*
    // Upload all archives into edge server, compute URLs
    $archivebeans = getArchives();

    foreach ($archivebeans as $b) {
        $zipfile = $b->archivename . ".zip";
        $suffix = "_" . $b->archiveidx;
        if ($suffix == "_1") { $suffix = ""; }

        $filesize_b = filesize($zipfile);
        $filesize_k = strval(intval($filesize_b / 1024));
        $archive_zipfile_name = "${archive_name}${suffix}.zip";
        $archive_zipfile_name = str_replace(" ", "_", $archive_zipfile_name);

        logINFO("Uploading ${filesize_k}k $zipfile to file server");
        try {
            aws_putfile($archive_zipfile_name, $zipfile);
        } catch (Exception $e) {
            quit("Error uploading file to AWS: " . $e->getMessage());
        }

        $link = "$ARCHIVE_URLBASE/$archive_zipfile_name";
        $b->link = "$link";
        $b->bytes = $filesize_b;
        $b->kilobytes = $filesize_k;
        R::store($b);
        logINFO("Uploaded archive $archive_zipfile_name ${filesize_k}k, location: $link");
    }
	*/

	// Creating finalmsg

	$finalmsg = "";

	$finalmsg = "<p>You may retrieve your manifest file from the following link: <a href='$manifest_link'>Manifest File</a></p>\n";
	$finalmsg .= "<p><sup>*</sup><i>The <b>Download Link</b> may not be accessible outside of the USDA intranet.</br>\n";
    $finalmsg .= "If you want to download the manifest file from outside the USDA, please copy and paste the <b>URL</b> address into your browser's navigation bar.</i></p>\n";



	// commenting the below code
	/*
    $filecount = countMetadata();       // Count stamps that didn't fail

    $finalmsg = "";

    $archivecount = countArchives();

    if ($archivecount == 1) {

        $finalmsg = "<p>Your archive contains $filecount files.</p><p>You may retrieve your zipfile from the following link:</p>\n";

    } else {
        $finalmsg = "<p>Your archive contains $filecount files in $archivecount zipfiles.</p>\n";
        $finalmsg .= "<p>You may retrieve your zipfiles from the following links:</p>\n";
    }
    $finalmsg .= "<table border='1' style='border-spacing:0;border-width:1px'>\n";
    $finalmsg .= "<tr><td>&nbsp;<b>Download&nbsp;Link</b><sup>*</sup>&nbsp;</td><td><center><b>Size</b></center></td><td><center><b>URL</b><sup>*</sup></center></td></tr>\n";
    $archivebeans = getArchives();
    foreach ($archivebeans as $b) {
        $link = $b->link;
        $size = $b->kilobytes;
        $idx  = $b->archiveidx;
        $finalmsg .= "<tr>";
        $finalmsg .= "<td><center><a href='$link'>Zipfile $idx</a></center></td>";
        $finalmsg .= "<td>&nbsp;$size&nbsp;k&nbsp;</td>";
        $finalmsg .= "<td>&nbsp;" . unlinkify($link) . "&nbsp;</td>";
        $finalmsg .= "</tr>\n";
    }
    $finalmsg .= "</tr></table>\n";
    $finalmsg .= "<p><sup>*</sup><i>The <b>Download Link</b> may not be accessible outside of the USDA intranet.</br>\n";
    $finalmsg .= "If you want to download a zipfile from outside the USDA, please copy and paste the <b>URL</b> address into your browser's navigation bar.</i></p>\n";

    $sensitivecount = countSensitive();
    if ($sensitivecount > 0) {
        $finalmsg .= "<p>Your archive includes the following sensitive files:</p>\n";
        $finalmsg .= "<table border='1' style='border-spacing:0;border-width:1px'>\n";
        $finalmsg .= "<tr><td><center><b>File name</b></center></td></tr>\n";
        $sbeans = getSensitive();
        foreach ($sbeans as $b) {
            $filename = htmlentities($b->filename);
            $finalmsg .= "<tr>";
            $finalmsg .= "<td><center>$filename</center></td>";
            $finalmsg .= "</tr>\n";
        }
        $finalmsg .= "</table>\n";
    }
	*/

    $email = $POSTVARS['userEmailIndex'];
    sendConfirmationEmail($email, $finalmsg);

    logINFO("Manifest complete!");
    logINFO("Ending job");
}

/*
 *  Kick off all processing here
 */

proc_nice(19);
main();
?>
