<?php

require_once('dmd_utils.php');

if ((!isset($DATAMART_PREFIX)) ||
    (!isset($DATAMART_AUTH))) {
	echo "<H1>Error loading utils, check configs</H1>";
        exit(0);
}

class datamart_utils {

/*****************
 *	PROPERTIES
 *****************/
public $G_HTTPCODE = null;
public $G_DOCLIST = array();	// Global cached list of project documents
public $G_DOC_COUNT = 0;	// Number of (non-sensitive) documents in tree
public $G_PRUNE_REGEXES = array();	// Patterns of folder names to prune
public $G_ALBUM_REGEXES = array();	// Patterns of folder names to albumize
public $G_JOBSPEC = array();	// List of published files (used by expandAlbum)

public $G_USERDATA = null;	// Record for active user
public $G_SELECT_RULE = "sensitive_rejectAll";	// Callback rule for handling
				// sensitive material, allowed values are:
				//   sensitive_rejectAll
				//   sensitive_allowAll
				//   sensitive_allowOwned

public $FOR_DISPLAY = false;
public $FOR_PHASEID = false;
public $VIEWID = null;
/*****************
 *	FUNCTIONS
 *****************/

/*
 *	datamart_status()
 *
 *	Test connection to Datamart with a GET
 *
 *	Returns HTTP response code
 */
function datamart_status() {
  global $DATAMART_PREFIX;
  $url = "$DATAMART_PREFIX/ref/documentsensitivityrationales/";
  $resp = $this->datamart_get($url);
  return $this->G_HTTPCODE;
}

/*
 *	validateTemplate($projectid)
 *
 *	Check the template for the specified project for errors.
 *	In particular, make sure that container ID values are unique throughout
 *	the template.
 */
function validateTemplate($projectid) {
    $template_dom = $this->getTemplateDOM($projectid);
    $template_dom->registerXPathNamespace("x", "http://www.fs.fed.us/nepa/schema");

    $containerids = array();
try{
    foreach ($template_dom->xpath('//x:container') as $container) {
        $contid = strval($container['contid']);
        if (isset($containerids[$contid])) {
            throw new Exception("Container tree contains duplicate IDs ($contid)");
            break;
        } else {
            $containerids[$contid] = True;
        }
    }
 }catch(Exception $e){
		foreach ($template_dom->xpath('//x:container') as $container) {
        $phaseid = strval($container['phaseid']);
        $containerids[$phaseid] = True;
        }
  }
}

/*
 *	datamart_getContainersAsDynatree($projectid, $prune_pat, $album_pat)
 *
 *	Return a Datamart Container tree in the form of a Dynatree
 *
 *	If $prune_pat is provided, it is a regex string or an array of
 *	regex strings indicating folders to be pruned from the container tree
 *
 *	If $album_pat is provided, it is a regex string or an array of
 *	regex strings indicating folders from the container tree to be made
 *	into albums (all-or-nothing selections)
 *
 *	The Dynatree is a tree of arrays with keys defined by the Dynatree spec
 *	The Dynatree is returned as a JSON string
 */
function datamart_getContainersAsDynatree($projectid, $prune_pat = "", $album_pat = "") {

  $this->validateTemplate($projectid);

  if (is_array($prune_pat)) {
    $this->G_PRUNE_REGEXES = $prune_pat;
  } else {
    // If non-empty pattern, add to array of pruning regexes
    if (strlen($prune_pat) > 0) {
      $this->G_PRUNE_REGEXES[] = $prune_pat;
    }
  }

  if (is_array($album_pat)) {
    $this->G_ALBUM_REGEXES = $album_pat;
  } else {
    // If non-empty pattern, add to array of album regexes
    if (strlen($album_pat) > 0) {
      $this->G_ALBUM_REGEXES[] = $album_pat;
    }
  }

  $this->preloadDocList($projectid);	// Optimization: cache project documents

  // Get dynatree as tree of PHP arrays
  $this->FOR_DISPLAY = false;
  $dynatree = $this->buildDynatree($projectid);
  $this->FOR_DISPLAY = false;

  // Encode as JSON
  return json_encode($dynatree, JSON_HEX_APOS|JSON_HEX_QUOT);
}


/*Created a duplicate function to avoid preloading documents*/
function datamart_getContainersAsDynatree1($projectid, $prune_pat = "", $album_pat = "",$viewid) {
  $this->VIEWID = $viewid;
  $this->FOR_DISPLAY = true;
  $this->validateTemplate($projectid);
  if (is_array($prune_pat)) {
    $this->G_PRUNE_REGEXES = $prune_pat;
  } else {
    // If non-empty pattern, add to array of pruning regexes
    if (strlen($prune_pat) > 0) {
      $this->G_PRUNE_REGEXES[] = $prune_pat;
    }
  }
  if (is_array($album_pat)) {
    $this->G_ALBUM_REGEXES = $album_pat;
  } else {
    // If non-empty pattern, add to array of album regexes
    if (strlen($album_pat) > 0) {
      $this->G_ALBUM_REGEXES[] = $album_pat;
    }
  }
//  $this->preloadDocList($projectid);  // Optimization: cache project documents
// Get dynatree as tree of PHP arrays
  $dynatree = $this->buildDynatree($projectid);
  $this->FOR_DISPLAY = false;
  // Encode as JSON
  return json_encode($dynatree, JSON_HEX_APOS|JSON_HEX_QUOT);
}
function datamart_getUserData($userid) {
  $userDOM = $this->getUserDOM($userid);
  return $this->userFromDOM($userDOM);
}

function datamart_getProjectData($projectid) {
  $projectDOM = $this->getProjectDOM($projectid);
  $project = $this->projectFromDOM($projectDOM);
  $projectLocationsDOM = $this->getProjectLocationsDOM($projectid);
  $locations = $this->projectLocationsFromDOM($projectLocationsDOM);
  return array_merge($project, $locations);
  return $project;
}

/*
 *	preloadDocList($projectid)
 *
 *	Load first 200000 project documents into a cache.
 *	These will be used to build the Dynatree nodes.
 *	If the project has more than this, we will eventually fetch
 *	most of them on demand.
 *
 *	11/1/2012 - At present our largest project has 111k documents
 *	(mostly comments) and most projects have far fewer.  Future
 *	projections include projects with millions of documents.  In
 *	order to handle document sets at that scale, we would probably
 *	want to build support into the Datamart for various styles of
 *	paging through documents at different category levels.
 */
function preloadDocList($projectid, $start = 0, $rows = 200000) {

  $last = $start + $rows;
  $docs_read = 0;
  $pagesize = $rows;
  if ($pagesize > 5000) { $pagesize = 5000; }
  do {
    // Get page of project documents as DOM
    $pdoc_dom = $this->getProjectDocListDOM($projectid, $start, $pagesize);

    // Convert DOM documents to flat list
    $doclist = $this->projectDocsDOMToList($pdoc_dom);
    $docs_read = count($doclist);
    if ($docs_read == 0) { break; }
    $start += $docs_read;

    // Add to global list
    $this->G_DOCLIST = array_merge($this->G_DOCLIST, $doclist);

    if ($docs_read != $pagesize) { break; }

  } while ($start < $last);
}

function buildDynatree($projectid) {
  // Fetch the container tree template
  $template_dom = $this->getTemplateDOM($projectid);

  // Attach documents to tree template (this allows us to handle pruned
  // folders and albums differently than plain folders)
  $dynatree = $this->templateDOMToDynatree($projectid, $template_dom);

  return $dynatree;
}

/*
 *  Given a project and the container ID of a folder (an "album"),
 *  generate all of the job spec data that would have resulted from
 *  expanding the folder and subfolders, and selecting all documents.
 *
 *  Output consists of an array of file specs, consisting of:
 *    path
 *    filename
 *    docid
 *
 *  The path consists of the folder path starting at the album.  The calling
 *  program can easily prepend the path from the root to the album as needed.
 *
 *  Path components should be urlencoded before concatenating with slashes
 */
function datamart_expandAlbum($projectid, $album_contid, $phaseid) {
  // Erase any album patterns
  $this->G_PRUNE_REGEXES = array();
  $this->G_ALBUM_REGEXES = array();
  $this->G_JOBSPEC = array();
  $album_folder = $this->datamart_buildAlbumDynatree($projectid, $album_contid, $phaseid);
  $this->publishFilesDepthFirst($album_folder, "");
  return $this->G_JOBSPEC;
}
function test() { return "hi there"; }

/*
 *  Visit all tree nodes, print data for anything that looks like a document
 *
 *  Path components should be urlencoded before concatenating with slashes
 */
function publishFilesDepthFirst($treenode, $currpath) {

    if ($treenode['isFolder']) {
        $childAr = $treenode['children'];
        if ($currpath == "") {
            $currpath = urlencode($treenode['filename']);
        } else {
            $currpath = $currpath . "/" . urlencode($treenode['filename']);
        }
        foreach ($childAr as $child) {
            $this->publishFilesdepthFirst($child, $currpath);
        }
    } else {
        // Make sure it isn't an album
        if ($treenode['isAlbum']) {
            echo "ERROR: found album node\n";
        } else {
            // Node is a document, add to job spec
            $doc = array();
            $doc['path'] = $currpath;
            $doc['filename'] = $treenode['filename'];
            $doc['docid'] = $treenode['key'];
            $this->G_JOBSPEC[] = $doc;
        }
    }
}

/*
 *	Build a Dynatree (in PHP structs) for the specified container ID
 */
function datamart_buildAlbumDynatree($projectid, $album_contid, $phaseid) {
  // get full container tree at specified node
  if (isset($phaseid)){
	  $this->FOR_PHASEID = true;
  }
  $template_dom = $this->getTemplateDOM($projectid);
  $this->FOR_PHASEID = false;
  $template_dom->registerXPathNamespace("x", "http://www.fs.fed.us/nepa/schema");
  $album_dom = null;
  foreach ($template_dom->xpath('//x:container') as $container) {
    if (intval($container['contid']) == $album_contid) {
		if (is_null($phaseid) || intval($container['phaseid']) == $phaseid) {
			$album_dom = $container;
			break;
		}
    }
  }
  if ($album_dom === null) {
    throw new Exception("Unable to find container for contid=$album_contid");
  }

  $album = $this->makeFolder($album_dom);
  $album['children'] = $this->templateDOMToDynatree($projectid, $album_dom, $phaseid);
  return $album;
}

/*
 *	Utility function to get a Datamart URL
 */
function datamart_get($url) {
  global $DATAMART_AUTH;
  $this->G_HTTPCODE = null;

  $ch=curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_USERPWD, $DATAMART_AUTH);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  $xml = curl_exec($ch);
  $info = curl_getinfo($ch);
  $this->G_HTTPCODE = $info["http_code"];
  curl_close($ch);
  return $xml;
}

/*
 *	getProjectDocDOM($projectid, $docid)
 *
 *	Return a single ProjectDocument as XML DOM
 */
function getProjectDocDOM($projectid, $docid) {
  global $DATAMART_PREFIX;
  if(!is_null($this->VIEWID)){
  $url = "$DATAMART_PREFIX/projects/nepa/$projectid/docs/$docid?viewId=".$this->VIEWID;
  }
  else{
  $url = "$DATAMART_PREFIX/projects/nepa/$projectid/docs/$docid";
  }
  $xml = $this->datamart_get($url);

  if (is_null($xml)) {
    throw new Exception("No response for project $projectid document $docid");
  }
  $dom = new SimpleXMLElement($xml);
  return $dom;
}

/*
 *  Fetch (and cache) a document record from the Datamart
 *
 *  Always use this function to retrieve projectdoc metadata so that
 *  the array gets populated on demand
 */
function getProjectDocMetadata($projectid, $docid) {

  if (isset($this->G_DOCLIST[$docid])) {
      return $this->G_DOCLIST[$docid];
  } else {
      $dom = $this->getProjectDocDOM($projectid, $docid);
      $metadata = $this->docMetadataFromDOM($dom);
      $this->G_DOCLIST[$docid] = $metadata;
      return $metadata;
  }
}

/*
 *	getTemplateDOM($projectid)
 *
 *	Return Container Template as XML DOM
 *	Remove any containers that match the regexes in $G_PRUNE_REGEXES
 */
function getTemplateDOM($projectid) {
  global $DATAMART_PREFIX;
  if($this->FOR_DISPLAY == true || $this->FOR_PHASEID == true){
	if(!is_null($this->VIEWID)){
		$url = "$DATAMART_PREFIX/projects/nepa/${projectid}/containers/downloader/template?viewid=".$this->VIEWID;
	} else {
	$url = "$DATAMART_PREFIX/projects/nepa/${projectid}/containers/downloader/template";
	}
  } else{
    $url = "$DATAMART_PREFIX/projects/nepa/${projectid}/containers/template";
  }
  $xml = $this->datamart_get($url);

  if (is_null($xml)) {
    throw new Exception("No response for project ${projectid} template");
  }

  // Use DOMDocument to delete pruned containers
  $dom = new DOMDocument();
  $dom->loadxml($xml);
  $xpath = new DOMXpath($dom);
  $xpath->registerNamespace('x', 'http://www.fs.fed.us/nepa/schema');

  foreach ($xpath->query('//x:container') as $container) {
    $label = $container->getAttribute('label');
    foreach ($this->G_PRUNE_REGEXES as $regex) {
      if (preg_match("$regex", $label) > 0) {
        $container->parentNode->removeChild($container);
        break;
      }
    }
  }

  $dom = new SimpleXMLElement($dom->savexml());
  return $dom;
}

/*
 *	getContainersDOM($projectid)
 *
 *	Return Containers as XML DOM
 */
function getContainersDOM($projectid) {
  global $DATAMART_PREFIX;
  if(!is_null($this->VIEWID)){
  $url = "$DATAMART_PREFIX/projects/nepa/${projectid}/containers/".$this->VIEWID;
  } else {
  $url = "$DATAMART_PREFIX/projects/nepa/${projectid}/containers/";
  }
  $xml = $this->datamart_get($url);

  if (is_null($xml)) {
    throw new Exception("No response for project ${projectid} categories");
  }
  $dom = new SimpleXMLElement($xml);
  $this->removeRootDocuments($dom);
  return $dom;
}

/*
 *	getProjectDocListDOM($projectid, $start, $rows)
 *
 *	Return list of project documents as XML DOM
 */
function getProjectDocListDOM($projectid, $start, $rows) {
  global $DATAMART_PREFIX;
  if(!is_null($this->VIEWID)){
  $url = "$DATAMART_PREFIX/projects/nepa/${projectid}/docs?start=$start&rows=$rows&viewId=".$this->VIEWID;
  } else {
  $url = "$DATAMART_PREFIX/projects/nepa/${projectid}/docs?start=$start&rows=$rows";
  }
  $xml = $this->datamart_get($url);

  if (is_null($xml)) {
    throw new Exception("No response for project ${projectid} documents");
  }
  $dom = new SimpleXMLElement($xml);
  return $dom;
}

/*
 *	Convert ProjectDocuments DOM to an array of metadata
 *
 */
function projectDocsDOMToList($dom) {
  $list = array();
  foreach ($dom->projectdocument as $projdocdom) {
    $doc = $this->docMetadataFromDOM($projdocdom);
    $list[$doc['docid']] = $doc;
  }
  return $list;
}

/*
 *	getContainerDocsDOM($projectid, $start, $rows)
 *
 *	Return list of project documents as XML DOM
 */
function getContainerDocsDOM($projectid, $contid, $start, $rows, $phaseid) {
  global $DATAMART_PREFIX;
  if(!is_null($this->VIEWID)) {

    $url = "$DATAMART_PREFIX/projects/nepa/${projectid}/containers/containerdocs?contid=$contid&start=$start&rows=$rows&viewId=".$this->VIEWID;
  } else if(isset($phaseid) && !is_null($phaseid) && ($phaseid !== "")){

  $url = "$DATAMART_PREFIX/projects/nepa/${projectid}/containers/containerdocs?contid=$contid&start=$start&rows=$rows&phaseid=$phaseid";

  } else {
	  //(is_null($phaseid) || ($phaseid == "") && is_null($viewid) || ($viewid == "")){
	  $url = "$DATAMART_PREFIX/projects/nepa/${projectid}/containers/containerdocs?contid=$contid&start=$start&rows=$rows";
  }

	$xml = $this->datamart_get($url);
if (is_null($xml)) {
    throw new Exception("No response for project ${projectid} container $contid documents");
  }
  $dom = new SimpleXMLElement($xml);
  return $dom;
}

function getUserDOM($userid) {
  global $DATAMART_PREFIX;
  $url = "$DATAMART_PREFIX/users/${userid}";
  $xml = $this->datamart_get($url);
  if ((is_null($xml)) || ($xml == "")) {
    throw new Exception("No response for user ${userid}");
  }
  $dom = new SimpleXMLElement($xml);
  return $dom;
}

function getProjectDOM($projectid) {
  global $DATAMART_PREFIX;
  $url = "$DATAMART_PREFIX/projects/nepa/${projectid}";
  $xml = $this->datamart_get($url);
  if ((is_null($xml)) || ($xml == "")) {
    throw new Exception("No response for project ${projectid}");
  }
  $dom = new SimpleXMLElement($xml);
  return $dom;
}

function getProjectLocationsDOM($projectid) {
  global $DATAMART_PREFIX;
  $url = "$DATAMART_PREFIX/projects/nepa/${projectid}/locations";
  $xml = $this->datamart_get($url);
  if ((is_null($xml)) || ($xml == "")) {
    throw new Exception("No response for project ${projectid} locations");
  }
  $dom = new SimpleXMLElement($xml);
  return $dom;
}

/*
 *  Convert the DOM into a set of PHP arrays
 *
 *  Note that the user can have multiple roles for the same unit,
 *  so we need to preserve all of them.  This is done by saving strings
 *  that look like "unit|role"
 */
function userFromDOM($userDOM) {
    $user = array();
    $user['shortname'] = strval($userDOM->shortname);
    $user['firstname'] = strval($userDOM->firstname);
    $user['lastname'] = strval($userDOM->lastname);
    $user['email'] = strval($userDOM->email);
    $user['phone'] = strval($userDOM->phone);
    $user['title'] = strval($userDOM->title);

    $roles = array();
    try {
        foreach ($userDOM->unitroles->unitrole as $unitrole) {
            $unitid = trim(strval($unitrole->unit));
            $roleid = trim(strval($unitrole->role));
            $roles[] = "$unitid|$roleid";
        }
    } catch (Exception $e) { /* No roles? */ }

    $user['roles'] = $roles;
    return $user;
}

function projectFromDOM($projectDOM) {
  $project = array();
  $project['name'] = str_replace('"','_',str_replace("'","_",strval($projectDOM->name)));
  $project['unitcode'] = strval($projectDOM->unitcode);
  $project['wwwpub'] = strval($projectDOM->wwwpub);
  $project['wwwlink'] = strval($projectDOM->wwwlink);
  $project['contactname'] = strval($projectDOM->nepainfo->contactname);
  $project['contactphone'] = strval($projectDOM->nepainfo->contactphone);
  $project['contactemail'] = strval($projectDOM->nepainfo->contactemail);

  // multiple purposeids
  $purposeids = array();
  foreach ($projectDOM->nepainfo->purposeids->purposeid as $purposeid) {
    $purposeids[] = strval($purposeid);
  }
  $project['purposeids'] = $purposeids;
  return $project;
}

function projectLocationsFromDOM($projectLocationsDOM) {
  $locations = array();
  $stateids = array();
  foreach ($projectLocationsDOM->states->stateid as $stateid) {
    $stateids[] = strval($stateid);
  }
  $locations['stateids'] = $stateids;
  return $locations;
}

/*
 *	Convert ProjectDocument to PHP array of metadata
 *
 *	This constructs "displayname" and "size" properties
 *
 *	"size" is based on PDF file size, and may not even be available
 *	if the document has not been converted to PDF.
 *
 *	Datamart "sensitiveflag" is stored as an integer value of 1/0
 */
function docMetadataFromDOM($projdoc) {
    $docid = strval($projdoc->docid);
    $doc = array(
      'docid' => $docid,
      'projectid' => strval($projdoc->projectid),
      'projecttype' => strval($projdoc->projecttype),
      'pubflag' => strval($projdoc->pubflag),
      'wwwlink' => strval($projdoc->wwwlink),
      'docname' => trim(strval($projdoc->docname)),
      'description' => trim(strval($projdoc->description)),
      'pdffilesize' => strval($projdoc->pdffilesize),
      'sensitiveflag' => strval($projdoc->sensitiveflag)	// 1/0
    );

    // Sanitize docname and description fields
    $search = array('"','`');
    $replace = "_";
    $doc['docname'] = str_replace($search, $replace, $doc['docname']);
    $doc['description'] = str_replace($search, $replace, $doc['description']);

    if (strlen($doc['docname']) == 0) { $doc['docname'] = "Untitled"; }

    $displaysize = $doc['pdffilesize'];
    if (strlen($displaysize) == 0) {
      $displaysize = "Unknown";
      $doc['size'] = 0;
    } else {
      if (preg_match("/^\d+kb$/", $displaysize) > 0) {
        $size = intval(preg_replace('/^(\d+)kb$/', '${1}', $displaysize));
        $doc['size'] = $size;
      } else if (preg_match("/^\.pdf\((\d+)kb\)$/", $displaysize) > 0) {
        // This is a weird size value formatted as ".pdf(NNNkb)",
        // reformat the display size string as well
        $size = intval(preg_replace('/^\.pdf\((\d+)kb\)$/', '${1}', $displaysize));
        $displaysize = "${size}kb";
        $doc['size'] = $size;
      } else {
        $doc['size'] = 0;
      }
    }

    $doc['displayname'] = $doc['docname'] . " (Uncompressed file size: $displaysize)";

    return $doc;
}

/*
 *	templateDOMToDynatree($projectid, $containers_template)
 *
 *	Convert XML container tree to the tree format used by the jsTree
 *	json_data plugin format.  Output is a PHP array that can be
 *	serialized as JSON
 *
 *	Basic format is:
 *	{ "title" : document title or category name,
 *	  "key" : docid, or autogenerated when omitted (for folder)
 *        "isFolder" : true/false,
 *        "order" : custom_value,
 *        "isAlbum" : true/false
 *        "sensitiveflag" : true/false,
 *	  "children" : []	// only valid for non-leaf nodes
 *	}
 *
 *	children can contain either nodes or strings, not clear what you
 *	would do with just strings though
 *
 *	* NOTE If the node is a folder whose name matches a regex in the
 *            $G_PRUNE_REGEXES array, we eliminate the folder and all contained
 *            nodes
 *
 *	* NOTE If the node is a folder whose name matches a regex in the
 *	      $G_ALBUM_REGEXES array, the node is an album (an all-or-nothing
 *            folder) and the following rules apply:
 *            - isFolder = false (The node will be treated as a document)
 *            - isAlbum  = true
 *            - The node will have a custom icon "album.gif"
 *            - The node size is the accumulated size of all contained docs
 */
function templateDOMToDynatree($projectid, $template, $phaseid) {
  $dynatree = array();
  // Recursively add documents to tree root node
  $this->populateDynatree($dynatree, $projectid, $template, $phaseid);

  return $dynatree;
}

/*
 * Given a container node (from a template), fetch the docs that belong
 * in the container and recursively build a dynatree
 *
 * There needs to be some kind of limit on the number of documents
 * per container or this function will hang
 *
 * In general, folders with a lot of documents need to be designated as
 * "albums" prior executing this function.  When we find an album, we handle
 * it differently from a normal folder, and the documents are counted without
 * creating UI document nodes for them.  This allows us to display the project
 * files (mostly) without crashing the UI.
 */
function populateDynatree(&$array, $projectid, $containernode, $phaseid1) {
  global $MAX_DOCS_PER_FOLDER;

  // Fetch and add all documents for container node
  if ($containernode->getName() == "container") {
    $contid = strval($containernode['contid']);
    $label = strval($containernode['label']);
    if(is_null($phaseid1)){
		$phaseid = strval($containernode['phaseid']);
    } else{
		$phaseid = $phaseid1;
    }
    if ((is_null($contid)) || ($contid == "")) {
      throw new Exception("Data error, project $projectid container $label has no contid");
    } else {
      // Read/process container docs in pages
      $start = 0;
      $pagesize = 10000;	// FIXME may need to tweak this
      $docs_read = 0;
      do {
        // Get page of container documents as DOM
        $cdoc_dom = $this->getContainerDocsDOM($projectid, $contid, $start, $pagesize, $phaseid);

        // Convert DOM documents to dynatree docs
        $docs_read = 0;
        foreach ($cdoc_dom->containerdoc as $docnode) {
          $doc = $this->makeDocument($projectid, $docnode);
          $array[] = $doc;
          $this->G_DOC_COUNT += 1;
          $docs_read += 1;
        }

        $start += $docs_read;
        if ($docs_read != $pagesize) { break; }

        /*
         * NOTE We can't handle more than several thousand documents in a
         * single folder, so we should halt the UI if this is detected.
         * In general, this should not be the case at the container root
         * level (although it is possible before documents are ingressed
         * and categorized), and folders at lower levels should be designated
         *  as all-or-nothing "albums" by filename pattern.
         */

      } while ($docs_read > 0);
    }
  }

  // Recursively populate all subfolders
  foreach ($containernode->container as $foldernode) {
    if ($this->isAlbum($foldernode)) {
		if ($this->FOR_DISPLAY == true) {
			$album = $this->makeAlbum1($projectid, $foldernode);
		} else {
			$album = $this->makeAlbum($projectid, $foldernode);
		}
		$this->G_DOC_COUNT += $album['filecnt'];
		$array[] = $album;
    } else {
      $folder = $this->makeFolder($foldernode);
      $this->populateDynatree($folder['children'], $projectid, $foldernode);
      $array[] = $folder;
    }
  }

  usort($array, array("datamart_utils", "cmpOrder"));
}

/*
 * Given a container node (from a template), tabulate statistics for the
 * contents of the node and its descendants
 *
 * Returns a pseudodocument with properties that are a summarization of its
 * contents.
 *
 * doccount - number of contained documents
 * totalsize - cumulative size of all contained documents (kb)
 * sensitiveflag - true if container has any sensitive documents
 * unselectable - true if container has any unselectable documents
 *
 * This is primarily for use with albums, which are too large to display
 * on the UI but can still be submitted for processing
 */
function tabulateDynatree($projectid, $containernode) {
  $doccount = 0;
  $totalsize = 0;
  $sensitiveflag = False;
  $unselectable = False;

  // Count all documents for container node
  if ($containernode->getName() == "container") {
    $contid = strval($containernode['contid']);
    $label = strval($containernode['label']);
	$phaseid = strval($containernode['phaseid']);
    if ((is_null($contid)) || ($contid == "")) {
      throw new Exception("Data error, project $projectid container $label has no contid");
    } else {
      // Read/process container docs in pages
      $start = 0;
      $pagesize = 10000;	// FIXME may need to tweak this
      $docs_read = 0;
      do {
        // Get page of container documents as DOM
        $cdoc_dom = $this->getContainerDocsDOM($projectid, $contid, $start, $pagesize, $phaseid);

        // Tabulate DOM documents
        $docs_read = 0;
        foreach ($cdoc_dom->containerdoc as $docnode) {
          $doc = $this->makeDocument($projectid, $docnode);
          $totalsize += $doc['size'];
          $docs_read += 1;
          if ($doc['sensitiveflag']) {
              $sensitiveflag = True;
          }
          if (isset($doc['unselectable']) && $doc['unselectable']) {
              $unselectable = True;
          }
        }

        $start += $docs_read;
        $doccount += $docs_read;
        if ($docs_read != $pagesize) { break; }
      } while ($docs_read > 0);
    }
  }

  // Recursively visit all subfolders
  foreach ($containernode->container as $foldernode) {
      $subfolder = $this->tabulateDynatree($projectid, $foldernode);
      $totalsize += $subfolder['totalsize'];
      $doccount += $subfolder['doccount'];
      if ($subfolder['sensitiveflag']) { $sensitiveflag = True; }
      if ($subfolder['unselectable']) { $unselectable = True; }
  }

  return array(
      'totalsize' => $totalsize,
      'doccount' => $doccount,
      'sensitiveflag' => $sensitiveflag,
      'unselectable' => $unselectable
  );
}

/*
 *	Define a sort order function for documents and categories
 */
static function cmpOrder($a, $b) {
    if ($a['order'] == $b['order']) {
        if ($a['title'] == $b['title']) {
            return 0;
        }
        return ($a['title'] < $b['title']) ? -1 : 1;
    }
    return ($a['order'] < $b['order']) ? -1 : 1;
}

/*
 *	Should this container node be pruned from the container tree?
 */
function isPrunable($node) {
  $label = strval($node['label']);
  foreach ($this->G_PRUNE_REGEXES as $regex) {
    if (preg_match("$regex", $label) > 0) { return True; }
  }
  return False;
}

/*
 *	Is this container node an "album" (all-or-nothing folder)?
 */
function isAlbum($node) {
  $label = strval($node['label']);
  foreach ($this->G_ALBUM_REGEXES as $regex) {
    if (preg_match("$regex", $label) > 0) { return True; }
  }
  return False;
}

/*
 *	Remove any (uncategorized) documents from DOM root
 */
function removeRootDocuments(&$dom) {
  $doc_cnt = count($dom->containerdoc);
  for ($doc_cnt--; $doc_cnt > 0; $doc_cnt--) {
    unset($dom->containerdoc[$doc_cnt]);
  }
}

/*
 * Make a Dynatree document node from a Containerdoc node
 *
 * Dynatree UI attributes:
 *   title		// display name (including annotations)
 *   key		// unique ID (docid)
 *   isFolder		// true/false (false for documents)
 *   addClass		// new CSS class name (used to style sensitive docs)
 *   hideCheckbox	// true/false (true for sensitive docs)
 *   unselectable	// true/false (UI)
 *
 * Custom attributes:
 *   filename		// original document name
 *   order		// sort key (numeric)
 *   docid
 *   sensitiveflag	// true / false
 *   size		// PDF size in kb
 */
function makeDocument($projectid, $node) {

  $order = strval($node['order']);
  $docid = strval($node['docid']);

  $metadata = $this->getProjectDocMetadata($projectid, $docid);

  $doc = array(
    'title' => $metadata['displayname'],
    'filename' => $metadata['docname'],
    'key' => "$docid",
    'isFolder' => false,
    'isAlbum' => false,
    'order' => $order,
    'sensitiveflag' => $metadata['sensitiveflag'],
    'size' => $metadata['size']	// PDF size in kb
  );

  if ($doc['sensitiveflag'] == "1") {
    $doc['addClass'] = 'sensitiveDoc';
    $doc['title'] = "[SENSITIVE] " . $metadata['docname'];
  }

  if ($this->{$this->G_SELECT_RULE}($projectid, $docid) == False) {
    $doc['unselectable'] = true;
    $doc['hideCheckbox'] = true;
  }

  // Note: PDF size is standing in for estimated document size because the
  // original filetype and original docsize are not part of the ProjectDocument
  // record.  At any rate, this is just an estimate of the size of the selected
  // uncompressed data.

  return $doc;
}

/*
 * Make a custom album node from a Container node
 * The source is a folder node, but we're making it into a document node
 *
 * Dynatree UI attributes:
 *   title		// display name (including annotations)
 *   key		// unique ID (leave null, autogenerated)
 *   isFolder		// true/false (false for albums)
 *   icon		// custom icon (album.gif)
 *
 * Custom attributes:
 *   isAlbum		// true for albums, false on all other nodes
 *   contid		// PALS unique container ID
 *   filename		// original folder name
 *   order		// sort key (numeric)
 *   sensitiveflag	// true/false (true if album contains a sensitive doc)
 *   unselectable	// true/false (true if album contains an unselectable doc)
 *   size		// total size of all contained docs in kb
 *   filecnt		// total count of all contained docs
 */
function makeAlbum($projectid, $node) {
  $order = strval($node['order']);
  $label = strval($node['label']);
  $contid = strval($node['contid']);
  $phaseid = strval($node['phaseid']);
  $album_stats = $this->tabulateDynatree($projectid, $node);
  $is_sensitive = $album_stats['sensitiveflag']; // True if any sensitive files in album
  $is_unselectable = $album_stats['unselectable']; // True if any unselectable files in album
  $total_size = $album_stats['totalsize'];	// Size of all files, in kb
  $total_count = $album_stats['doccount'];	// Count of all files

  $album = array(
    'title' => "$label ($total_count documents)",
    'filename' => "$label",
    // 'key' => "",	# Autogenerate key
    'isFolder' => false,
    'isAlbum' => true,
    'contid' => $contid,
    'phaseid' => $phaseid,
    'order' => $order,
    'sensitiveflag' => $is_sensitive,
    'size' => $total_size,
    'filecnt' => $total_count,
    'icon' => 'album.gif'
  );

  // Mark album as sensitive if it contains any sensitive documents
  // Mark album as unselectable if it contains any unselectable documents
  if ($is_sensitive) {
    $album['addClass'] = 'sensitiveDoc';
    $album['title'] = "[SENSITIVE] " . $album['title'];
  }

  if ($is_unselectable) {
    $album['unselectable'] = true;
    $album['hideCheckbox'] = true;
  }

  return $album;
}



//Created duplicate function for displaying original letters & submissions without preloading
function makeAlbum1($projectid, $node) {
	$order = strval($node['order']);
	$label = strval($node['label']);
	$contid = strval($node['contid']);
	$phaseid = strval($node['phaseid']);
	$total_count = (int)strval($node['doccount']);
	$total_size = (int)strval($node['filesizetotal']);
	$is_unselectable = strval($node['selectable']);
	$is_sensitive = strval($node['sensitive']);

	if($is_sensitive == 'true'){
	 $is_sensitive = 1 ;
	} else{
	 $is_sensitive = 0;
	}
	if($is_unselectable == 'true'){
	 $is_unselectable = 0 ;
	} else{
	 $is_unselectable = 1;
	}
	$album = array(
		'title' => "$label ($total_count documents)",
		'filename' => "$label",
		// 'key' => "",	# Autogenerate key
		'isFolder' => false,
		'isAlbum' => true,
		'contid' => $contid,
		'phaseid' => $phaseid,
		'order' => $order,
		'sensitiveflag' => $is_sensitive,
		'size' => $total_size,
		'filecnt' => $total_count,
		'icon' => 'album.gif'
	);
	// Mark album as sensitive if it contains any sensitive documents
	// Mark album as unselectable if it contains any unselectable documents
	if ($is_sensitive) {
		$album['addClass'] = 'sensitiveDoc';
		$album['title'] = "[SENSITIVE] " . $album['title'];
	}
	if ($is_unselectable) {
		$album['unselectable'] = true;
		$album['hideCheckbox'] = true;
	}
	return $album;
}
/*
 * Make a Dynatree folder node from a Container node
 *
 * Dynatree UI attributes:
 *   title		// display name (including annotations)
 *   key		// unique ID (leave null, autogenerated)
 *   isFolder		// true/false (true for folders)
 *   children		// array of contained document and folder nodes
 *
 * Custom attributes:
 *   contid		// PALS unique container ID
 *   filename		// original folder name
 *   order		// sort key (numeric)
 */
function makeFolder($node) {
  $order = strval($node['order']);
  $label = strval($node['label']);
  $contid = strval($node['contid']);
  $phaseid = strval($node['phaseid']);
  $folder = array(
    'title' => "$label",
    'contid' => $contid,
    'phaseid' => $phaseid,
    'filename' => "$label",
    'order' => $order,
    'isFolder' => true,
    'isAlbum' => false,
    'children' => array()
  );
  return $folder;
}

/*
 *  Set business rules for eFile utility
 */
function datamart_setModeEfile() {
    $this->G_SELECT_RULE = "sensitive_rejectAll";
}

/*
 *  Set business rules for Downloader utility
 */
function datamart_setModeDownloader($adminunitid, $userdata) {
    $this->G_SELECT_RULE = "sensitive_allowOwned";
    $this->G_USERDATA = $userdata;

    /*
     *  Check for the following roles for this admin unit:
     *  1 - PALS Admin
     *  2 - PALS Manager
     *  3 - SOPA Manager
     */
    $authorized_roles = array(1, 2, 3);
    foreach ($userdata['roles'] as $roleline) {
        $roledata = explode("|", $roleline);
        $unit = $roledata[0];
        $role = $roledata[1];
        $unit = preg_replace("/(00)*$/", "", $unit);
        if (preg_match("/^$unit/", $adminunitid)) {
            if (in_array($role, $authorized_roles)) {
                $this->G_SELECT_RULE = "sensitive_allowAll";
                break;
            }
        }
    }
}

/*
 *  Set business rules for Bates stamper utility
 */
function datamart_setModeBates($adminunitid, $userdata) {
    $this->datamart_setModeDownloader($adminunitid, $userdata);
}

/*
 *  Rule for handling sensitive documents for EPA eFile (reject all)
 */
function sensitive_rejectAll($projectid, $docid) {
    $metadata = $this->getProjectDocMetadata($projectid, $docid);
    if ($metadata['sensitiveflag']) {
        return False;
    } else {
        return True;
    }
}

/*
 *  Rule for handling sensitive documents for Downloader and Bates stamper
 *
 *  If user has a role of SOPA_MANAGER, PALS_MANAGER, or PALS_ADMIN (for a
 *  unitid that prefixes the project unitid) then user is authorized to
 *  handle all sensitive files.  This is evaluated by the function
 *  datamart_setModeDownloader().
 */
function sensitive_allowAll($projectid, $docid) {
    return True;
}

/*
 *  Rule for handling sensitive documents for Downloader and Bates stamper
 *
 *  If user is the same as the document author, then user is authorized to
 *  handle the sensitive file
 */
function sensitive_allowOwned($projectid, $docid) {
    $metadata = $this->getProjectDocMetadata($projectid, $docid);
    if ($metadata['sensitiveflag']) {
        if (!isset($metadata['author'])) {
            // Fetch document author
            $dmd_metadata = dmd_getMetadata($docid);
            $metadata['author'] = trim($dmd_metadata['author']);
        }
        $user = trim($this->G_USERDATA['shortname']);
        $author = $metadata['author'];
        if ($user == $author) {
            return True;	// user owns sensitive document
        } else {
            return False;	// user does not own sensitive document
        }
    } else {
        return True;	// not sensitive
    }
}

}
?>
